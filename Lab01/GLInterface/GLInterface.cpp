#include "GLInterface.h"


CGLInterface::CGLInterface(void)
{
	
}

CGLInterface::~CGLInterface(void)
{

}

void CGLInterface::SetProjectView(int cx, int cy)
{
	// cx와 cy는 클라이언트 영역의 x축, y축 크기
	glViewport(0, 0, cx, cy);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, 500, 0, 500);
	glMatrixMode(GL_MODELVIEW);
}


void CGLInterface::Display()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1.0f, 0.0f, 0.0f);

	glBegin(GL_POLYGON);

	glVertex2f(0, 0);
	glVertex2f(0, 400);
	glVertex2f(400, 400);
	glVertex2f(400, 0);
	
	glEnd();

	// force execution of OpenGL functions in finite time
	glFlush();
}

