#ifndef GL_INTERFACE_H
#define GL_INTERFACE_H

#ifdef WIN32
#include <Windows.h>
#endif

#ifndef _APPLE_OSX
#include <gl/GL.h>
#include <gl/GLU.h>
#else
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#endif

class CGLInterface
{
public:
	CGLInterface();
	virtual ~CGLInterface();

	void SetProjectView(int cx, int cy);
	void Display();

private:
};

#endif
