#include "cgglwidget.h"

#ifdef __APPLE__
#include <glu.h>
#elif _WIN32
#include <gl/glu.h>
#endif

#include <QMouseEvent>
#include <qpushbutton.h>
#include <QHBoxLayout>

CGGLWidget::CGGLWidget(QWidget *parent) : QOpenGLWidget(parent)
{

}

CGGLWidget::~CGGLWidget(void)
{

}

void CGGLWidget::initializeGL()
{
	
}

void CGGLWidget::paintGL()
{
	miGL.Display();
}

void CGGLWidget::resizeGL(int w, int h)
{
	miGL.SetProjectView(w, h);
}

