#ifndef QT_MAINWINDOW_H
#define QT_MAINWINDOW_H

#include <qmainwindow.h>
#include "ui_QtMainWindow.h"

class QtMainWindow : public QMainWindow , Ui::QtMainWindow
{
public:
	explicit QtMainWindow(QWidget *parent = 0, Qt::WindowFlags flags = 0);
	virtual ~QtMainWindow();

};
#endif