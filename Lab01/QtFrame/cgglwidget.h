#ifndef OPENGL_PROJECTORSCREEN_VIEWER_H_011
#define OPENGL_PROJECTORSCREEN_VIEWER_H_011

#include <qopenglwidget.h>
#include "GLInterface.h"

class QPushButton;

class CGGLWidget :public QOpenGLWidget
{
public:
	CGGLWidget(QWidget *parent = 0);
	~CGGLWidget();
	
protected:
	void initializeGL();
	void resizeGL(int w, int h);
	void paintGL();

	CGLInterface miGL;
};

#endif
