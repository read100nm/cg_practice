#include "GLInterface.h"


CGLInterface::CGLInterface(void)
{
	m_pos[0].x = 0;
	m_pos[0].y = 0;
	m_pos[1].x = 0;
	m_pos[1].y = 0;
	m_fScaleFactor[0] = 0.0f;
	m_fScaleFactor[1] = 0.0f;
	mIsFillRect = false;
}

CGLInterface::~CGLInterface(void)
{

}

void CGLInterface::SetProjectView(int cx, int cy)
{
	// cx와 cy는 클라이언트 영역의 x축, y축 크기
	glViewport(0, 0, cx, cy);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, 500, 0, 500);
	m_fScaleFactor[0] = 500.0f / cx;
	m_fScaleFactor[1] = 500.0f / cy;
	glMatrixMode(GL_MODELVIEW);
}


void CGLInterface::Display()
{
	// Viewport 정보 받아오기
	float fViewport[4];
	glGetFloatv(GL_VIEWPORT, fViewport);

	// Window좌표 값을 OpenGL 좌표값으로 변환
	float pos[2][2];
	pos[0][0] = (float)m_pos[0].x * m_fScaleFactor[0];
	pos[0][1] = (fViewport[3] - (float)m_pos[0].y) * m_fScaleFactor[1];
	pos[1][0] = (float)m_pos[1].x * m_fScaleFactor[0];
	pos[1][1] = (fViewport[3] - (float)m_pos[1].y) * m_fScaleFactor[1];

	// clear color
	glClear(GL_COLOR_BUFFER_BIT);

	if (mIsFillRect)
		SetStyle(1.0f, 0.0f, 0.0f, 1.0f, true);
	else
		SetStyle(1.0f, 1.0f, 0.0f, 0.0f);

	// draw quad
	glBegin(GL_QUADS);
	glVertex2f(pos[0][0], pos[0][1]);
	glVertex2f(pos[0][0], pos[1][1]);
	glVertex2f(pos[1][0], pos[1][1]);
	glVertex2f(pos[1][0], pos[0][1]);
	glEnd();

	// force execution of OpenGL functions in finite time
	glFlush();
}


void CGLInterface::SetPos(int mx, int my, bool bIsStartPos)
{
	m_pos[!bIsStartPos].x = mx;
	m_pos[!bIsStartPos].y = my;
}


void CGLInterface::SetStyle(float fThickness, float rf, float gf, float bf, bool bIsFilled)
{
	glLineWidth(fThickness);
	glColor3f(rf, gf, bf);

	if (bIsFilled)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glDisable(GL_LINE_STIPPLE);

	}
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glEnable(GL_LINE_STIPPLE);
		glLineStipple(1, 0x0f0f);
	}
}
