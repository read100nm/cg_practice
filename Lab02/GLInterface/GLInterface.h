#ifndef GL_INTERFACE_H
#define GL_INTERFACE_H

#ifdef WIN32
#include <Windows.h>
#endif

#ifndef _APPLE_OSX
#include <gl/GL.h>
#include <gl/GLU.h>
#else
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#endif

struct MPoint
{
	int x;
	int y;
};

class CGLInterface
{
public:
	CGLInterface();
	virtual ~CGLInterface();

	void SetProjectView(int cx, int cy);
	void Display();

	void SetPos(int mx, int my, bool bIsStartPos = false);
	void SetStyle(float fThickness, float rf, float gf, float bf, bool bIsFilled = false);
	void SetFillRect(bool bIsFilled){
		mIsFillRect = bIsFilled;
	}
private:
	MPoint m_pos[2];  // {x ,y} , { x, y}
	float m_fScaleFactor[2];
	bool mIsFillRect;
};

#endif
