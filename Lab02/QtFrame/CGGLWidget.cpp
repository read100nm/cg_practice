#include "cgglwidget.h"

#ifdef __APPLE__
#include <glu.h>
#elif _WIN32
#include <gl/glu.h>
#endif

#include <QMouseEvent>
#include <qpushbutton.h>
#include <QHBoxLayout>

CGGLWidget::CGGLWidget(QWidget *parent) : QOpenGLWidget(parent)
{
	setMouseTracking(true);
}

CGGLWidget::~CGGLWidget(void)
{

}

void CGGLWidget::initializeGL()
{
	
}

void CGGLWidget::paintGL()
{
	miGL.Display();
}

void CGGLWidget::resizeGL(int w, int h)
{
	miGL.SetProjectView(w, h);
}

void CGGLWidget::mouseMoveEvent(QMouseEvent *mevent)
{
	if (mevent->buttons() &  Qt::LeftButton)
	{
		miGL.SetPos(mevent->x(), mevent->y());
		update();
	}
}

void CGGLWidget::mousePressEvent(QMouseEvent *mevent)
{
	if (mevent->button() == Qt::MouseButton::LeftButton)
	{
		miGL.SetFillRect(false);
		miGL.SetPos(mevent->x(), mevent->y());
		miGL.SetPos(mevent->x(), mevent->y(),true);
	}

}

void CGGLWidget::mouseReleaseEvent(QMouseEvent *mevent)
{
	if (mevent->button() == Qt::MouseButton::LeftButton)
	{
		miGL.SetFillRect(true);
		miGL.SetPos(mevent->x(), mevent->y());
		update();
	}

}
