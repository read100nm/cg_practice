#include "GLInterface.h"


CGLInterface::CGLInterface(void)
{
	m_pos[0].x = m_pos[0].y = m_pos[1].x = m_pos[1].y = 0;
	start = 0;
	shape = LINE;
	SetColor(COLOR[BLACK]);
	SetLineWidth(2.0);
	attrib.fillType = GL_LINE_LOOP;
}

CGLInterface::~CGLInterface(void)
{

}

void CGLInterface::SetProjectView(int cx, int cy)
{
	// cx와 cy는 클라이언트 영역의 x축, y축 크기
	// cx and cy are the size of the x axis and the y axis of the client's region.
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, cx, 0, cy);
	glViewport(0, 0, cx, cy);
	curVPSizeX = cx;
	curVPSizeY = cy;
}

void CGLInterface::OnMouseEvent(int event, GLfloat mx, GLfloat my)
{
	//?? MouseEvent 함수 작성
	//?? Implement this function

	switch (shape)
	{
	case LINE:
		InputLine(event, mx, my);
		break;
	case QUAD:
		InputQuad(event, mx, my);
		break;
	}
}

void CGLInterface::Display()
{
	//?? display 함수 작성 
	//?? Implement this function
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);  // clear buffer with specified color

	ApplyAttribute();

	switch (shape)
	{
	case LINE:
		DisplayLine();
		break;
	case QUAD:
		DisplayQuad();
		break;
	}
	glFlush();	// forces execution of OpenGL functions in finite time.

}

void CGLInterface::DisplayQuad()
{
	//?? 사각형 그리기 함수 작성
	//?? Implement this function for draw a rectangle
	glBegin(attrib.fillType);
	glVertex2f(m_pos[0].x, m_pos[0].y);
	glVertex2f(m_pos[0].x, m_pos[1].y);
	glVertex2f(m_pos[1].x, m_pos[1].y);
	glVertex2f(m_pos[1].x, m_pos[0].y);
	glEnd();

}

void CGLInterface::InputQuad(int event, GLfloat x, GLfloat y)
{
	//?? 마우스를 이용한 사각형 그리기 함수 작성 
	//?? Implement this function that draw a rectangle using mouse event
	y = curVPSizeY - y;	// 화면 좌표계로 변환
	switch (event)
	{
	case LBUTTON_DOWN:
		if (!start) 	// 시작점과 끝점을 같게 초기화한다. 
		{
			m_pos[0].x = m_pos[1].x = x;	m_pos[0].y = m_pos[1].y = y;
			SetDrawStart();
		}
		else	// 그리기가 시작된 경우 끝점을 update하고 정상 종료
		{
			m_pos[1].x = x; 	m_pos[1].y = y;
			NormalDrawEnd();
		}
		break;
	case MOVE:	// 마우스 버튼을 떼고 옮기는 경우
		if (start)	// 그리기가 시작된 경우 끝점을 update
		{
			m_pos[1].x = x; 	m_pos[1].y = y;
		}
		break;
	}

}

void CGLInterface::DisplayLine()
{
	glBegin(GL_LINES);
	//	glBegin(attrib.fillType);	// draw quadrilaterals
	glVertex2f(m_pos[0].x, m_pos[0].y);
	glVertex2f(m_pos[1].x, m_pos[1].y);
	glEnd();
}

void CGLInterface::InputLine(int event, GLfloat x, GLfloat y)
{
	y = curVPSizeY - y;
	switch (event)
	{
	case LBUTTON_DOWN:
		if (!start)
		{
			// 시작점과 끝점을 같게 초기화한다. 
			// Initialize the start point and the end point to same.
			m_pos[0].x = m_pos[1].x = x;
			m_pos[0].y = m_pos[1].y = y;
			SetDrawStart();
		}
		else
		{
			m_pos[1].x = x; 	m_pos[1].y = y;
			NormalDrawEnd();
		}
		break;
	case MOVE:
		if (start)
		{
			m_pos[1].x = x; 	m_pos[1].y = y;
		}
		break;
	}
}

void  CGLInterface::ApplyAttribute()
{
	glColor3fv(attrib.color);
	glLineWidth(attrib.lineWidth);
	glPointSize(attrib.pointSize);
}