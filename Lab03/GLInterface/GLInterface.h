#ifndef GL_INTERFACE_H
#define GL_INTERFACE_H

#ifdef WIN32
#include <Windows.h>
#endif

#ifndef _APPLE_OSX
#include <gl/GL.h>
#include <gl/GLU.h>
#else
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#endif

typedef GLfloat ColorType[3]; // color 자료형을 정의 

enum ShapeID{ LINE = 0, QUAD, CIRCLE, CIRCLE_PASSIVE, POLYGON, FILLSEL, NOSHAPE };
enum MouseEvent { MOVE, DRAG, LBUTTON_DOWN, LBUTTON_UP, RBUTTON_DOWN, RBUTTON_UP };

// 자주 사용하는 color를 정의 
enum ColorID { BLACK = 0, RED, GREEN, BLUE, CYAN, MAGENTA, YELLOW, WHITE }; // assign color index
static ColorType COLOR[30] = { { 0.0f, 0.0f, 0.0f, }, // not color
{ 1.0f, 0.0f, 0.0f },  //RED
{ 0.0f, 1.0f, 0.0f },  //GREEN
{ 0.0f, 0.0f, 1.0f },  //BLUE
{ 0.0f, 1.0f, 1.0f },  //CYAN
{ 1.0f, 0.0f, 1.0f },  //MAGENTA
{ 1.0f, 1.0f, 0.0f },  //YELLOW
{ 1.0f, 1.0f, 1.0f }   //WHITE
};

struct MPoint
{
	int x;
	int y;
};

struct AttributeType {  // attribute class
	// this의 내용으로 OpenGL register를 세팅 
	void  SetAttribute(){
		glColor3fv(color); glLineWidth(lineWidth);
		glPointSize(pointSize);
	};
	void RegColor(ColorType inColor) { color[0] = inColor[0]; color[1] = inColor[1]; color[2] = inColor[2]; }; // a를 this에 
	// a를 this에 복사 
	void  CopyAll(AttributeType& a){
		color[0] = a.color[0]; color[1] = a.color[1]; color[2] = a.color[2];
		fillType = a.fillType; pointSize = a.pointSize;	lineWidth = a.lineWidth;
	};

	ColorType color;
	int fillType;
	float pointSize;
	float lineWidth;
};


class CGLInterface
{
public:
	CGLInterface();
	virtual ~CGLInterface();

	/// set projection
	void SetProjectView(int cx, int cy);

	/// draw display by OpenGL functions
	void Display();

	/// mouse Event handler
	void OnMouseEvent(int evenType, GLfloat mx, GLfloat my);

	/// set primitive type
	void SetPrimitiveType(int type) { shape = type; m_pos[0].x = m_pos[1].x = m_pos[0].y = m_pos[1].y = 0.0; };  // set current primitive type

	/// draw quad
	void DisplayQuad();

	/// input quad
	void InputQuad(int event, GLfloat x, GLfloat y);

	/// draw line
	void DisplayLine();

	/// input line
	void InputLine(int event, GLfloat x, GLfloat y);

	/// drawing status
	void  SetDrawStart() { start = 1; };
	void  NormalDrawEnd() { start = 0; };

	// current attribute setting functions
	void  SetColor(ColorType inCol){ attrib.RegColor(inCol);	}; // set the current color
	void  SetLineWidth(float width){ attrib.lineWidth = width;  };	// set current line width
	void  SetPointSize(float size){ attrib.pointSize = size; };		// set current point size
	void  SetFillType(int a){ attrib.fillType = a; };									// set current fill type
	void  ApplyAttribute();

private:

	/// Points to draw
	MPoint m_pos[2];

	/// current shape
	int shape; 

	/// drawing flag. 1 for start, 0 for not start
	int start; 

	/// current point
	int curVPSizeX, curVPSizeY;

	/// drawing attribute
	AttributeType attrib;
};

#endif
