// example3View.cpp : implementation of the CExample3View class
//

#include "stdafx.h"
#include "MFCFrame.h"

#include "MFCFrameDoc.h"
#include "MFCFrameView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CExample3View

IMPLEMENT_DYNCREATE(MFCFrameView, CView)

BEGIN_MESSAGE_MAP(MFCFrameView, CView)
    ON_WM_SIZE()
    ON_WM_ERASEBKGND()
    ON_WM_DESTROY()
    ON_WM_CREATE()
	ON_COMMAND(ID_PRIMITIVES_LINE, &MFCFrameView::OnPrimitivesLine)
	ON_COMMAND(ID_PRIMITIVES_QUADRILATERAL, &MFCFrameView::OnPrimitivesQuadrilateral)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_COMMAND(ID_COLOR_BLUE, &MFCFrameView::OnColorBlue)
	ON_COMMAND(ID_COLOR_GREEN, &MFCFrameView::OnColorGreen)
	ON_COMMAND(ID_COLOR_RED, &MFCFrameView::OnColorRed)
	ON_COMMAND(ID_COLOR_YELLOW, &MFCFrameView::OnColorYellow)
	ON_COMMAND(ID_FILL_FILL, &MFCFrameView::OnFillFill)
	ON_COMMAND(ID_FILL_SOLIDLINE, &MFCFrameView::OnFillSolidline)
	ON_COMMAND(ID_LINEWIDTH_1, &MFCFrameView::OnLinewidth1)
	ON_COMMAND(ID_LINEWIDTH_2, &MFCFrameView::OnLinewidth2)
	ON_COMMAND(ID_LINEWIDTH_3, &MFCFrameView::OnLinewidth3)
	ON_COMMAND(ID_LINEWIDTH_4, &MFCFrameView::OnLinewidth4)
END_MESSAGE_MAP()

// CExample3View construction/destruction

MFCFrameView::MFCFrameView()
{
	// TODO: add construction code here
	m_hRC = NULL;
	m_pDC = NULL;
}

MFCFrameView::~MFCFrameView()
{
}

BOOL MFCFrameView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	cs.style |= WS_CLIPCHILDREN | WS_CLIPSIBLINGS;

	return CView::PreCreateWindow(cs);
}

// CExample3View drawing

void MFCFrameView::OnDraw(CDC* /*pDC*/)
{
	MFCFrameDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: add draw code for native data here
	m_iGL.Display();
}


// CExample3View diagnostics

#ifdef _DEBUG
void MFCFrameView::AssertValid() const
{
	CView::AssertValid();
}

void MFCFrameView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

MFCFrameDoc* MFCFrameView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(MFCFrameDoc)));
	return (MFCFrameDoc*)m_pDocument;
}
#endif //_DEBUG

void MFCFrameView::OnSize(UINT nType, int cx, int cy)
{
    CView::OnSize(nType, cx, cy);

    // TODO: Add your message handler code here
    m_iGL.SetProjectView(cx, cy);
}

BOOL MFCFrameView::OnEraseBkgnd(CDC* pDC)
{
    // TODO: Add your message handler code here and/or call default

    //return CView::OnEraseBkgnd(pDC);
    return false;
}


int MFCFrameView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CView::OnCreate(lpCreateStruct) == -1)
        return -1;

    // TODO:  Add your specialized creation code here
    return Initialize(new CClientDC(this));
}

void MFCFrameView::OnDestroy()
{
	CView::OnDestroy();

	// TODO: Add your message handler code here
	Release();
}

int MFCFrameView::Initialize(CDC *pDC)
{
	if (m_pDC || m_hRC) {
		::AfxMessageBox(_T("Already initialized"));
		return -1;
	}
	if (!(m_pDC = pDC))	{
		::AfxMessageBox(_T("Fail to get device context"));
		return -1;
	}

	if (!SetPixelFormat(NULL)) {	// Pixel format ����
		::AfxMessageBox(_T("SetupPixelFormat failed"));
		return -1;
	}
	if (!(m_hRC = wglCreateContext(m_pDC->GetSafeHdc()))) {	// RC ȹ��
		::AfxMessageBox(_T("wglCreateContext failed"));
		return -1;
	}
	if (!wglMakeCurrent(m_pDC->GetSafeHdc(), m_hRC)) {// Current RC ����
		::AfxMessageBox(_T("wglMakeCurrent failed"));
		return -1;
	}

	return 0;	// -1: Fail | 0: Well done
}

void MFCFrameView::Release(void)
{
	if (!wglMakeCurrent(0, 0))
		::AfxMessageBox(_T("wglMakeCurrent failed"));

	if (m_hRC && !wglDeleteContext(m_hRC))
		::AfxMessageBox(_T("wglDeleteContext filed"));

	if (m_pDC)
		delete m_pDC;

	m_hRC = NULL;	// Must be NULL
	m_pDC = NULL;	// Must be NULL
}

BOOL MFCFrameView::SetPixelFormat(PIXELFORMATDESCRIPTOR * pPFD)
{
	if (!pPFD) {
		PIXELFORMATDESCRIPTOR pfd = {
			sizeof(PIXELFORMATDESCRIPTOR),
			1, PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL,
			PFD_TYPE_RGBA, 24, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 16, 0, 0,
			PFD_MAIN_PLANE, 0, 0, 0, 0 };
		pPFD = &pfd;
	}

	int nPF = ::ChoosePixelFormat(m_pDC->GetSafeHdc(), pPFD);
	if (!nPF) {
		::AfxMessageBox(_T("ChoosePixelFormat failed"));
		return FALSE;
	}

	if (!::SetPixelFormat(m_pDC->GetSafeHdc(), nPF, pPFD)) {
		::AfxMessageBox(_T("SetPixelFormat failed"));
		return FALSE;
	}

	return TRUE;
}


void MFCFrameView::OnPrimitivesLine()
{
	// TODO: Add your command handler code here
	m_iGL.SetPrimitiveType(LINE);
}


void MFCFrameView::OnPrimitivesQuadrilateral()
{
	// TODO: Add your command handler code here
	m_iGL.SetPrimitiveType(QUAD);
}


void MFCFrameView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	m_iGL.OnMouseEvent(LBUTTON_DOWN, point.x, point.y);
	Invalidate();

	CView::OnLButtonDown(nFlags, point);
}


void MFCFrameView::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	m_iGL.OnMouseEvent(MOVE, point.x, point.y);
	Invalidate();

	CView::OnMouseMove(nFlags, point);
}


void MFCFrameView::OnColorBlue()
{
	// TODO: Add your command handler code here
	m_iGL.SetColor(COLOR[BLUE]);
}


void MFCFrameView::OnColorGreen()
{
	// TODO: Add your command handler code here
	m_iGL.SetColor(COLOR[GREEN]);
}


void MFCFrameView::OnColorRed()
{
	// TODO: Add your command handler code here
	m_iGL.SetColor(COLOR[RED]);
}


void MFCFrameView::OnColorYellow()
{
	// TODO: Add your command handler code here
	m_iGL.SetColor(COLOR[YELLOW]);
}


void MFCFrameView::OnFillFill()
{
	// TODO: Add your command handler code here
	m_iGL.SetFillType(GL_POLYGON);
}


void MFCFrameView::OnFillSolidline()
{
	// TODO: Add your command handler code here
	m_iGL.SetFillType(GL_LINE_LOOP);
}


void MFCFrameView::OnLinewidth1()
{
	// TODO: Add your command handler code here
	m_iGL.SetLineWidth(1.0);
}


void MFCFrameView::OnLinewidth2()
{
	// TODO: Add your command handler code here
	m_iGL.SetLineWidth(2.0);
}


void MFCFrameView::OnLinewidth3()
{
	// TODO: Add your command handler code here
	m_iGL.SetLineWidth(3.0);
}


void MFCFrameView::OnLinewidth4()
{
	// TODO: Add your command handler code here
	m_iGL.SetLineWidth(4.0);
}
