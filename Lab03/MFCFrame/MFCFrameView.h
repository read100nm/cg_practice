// example2View.h : interface of the CExample2View class
//


#pragma once

#include "GLInterface.h"

class MFCFrameView : public CView
{
protected:

	// opengl interface
    CGLInterface m_iGL;
    
protected: // create from serialization only
	MFCFrameView();
	DECLARE_DYNCREATE(MFCFrameView)

// Attributes
public:
	MFCFrameDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:

	int Initialize(CDC *pDC);
	BOOL SetPixelFormat(PIXELFORMATDESCRIPTOR *pPFD = NULL);
	void Release();

	HGLRC m_hRC;
	CDC * m_pDC;
// Implementation
public:
	virtual ~MFCFrameView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
    
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnDestroy();
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
  
	afx_msg void OnPrimitivesLine();
	afx_msg void OnPrimitivesQuadrilateral();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnColorBlue();
	afx_msg void OnColorGreen();
	afx_msg void OnColorRed();
	afx_msg void OnColorYellow();
	afx_msg void OnFillFill();
	afx_msg void OnFillSolidline();
	afx_msg void OnLinewidth1();
	afx_msg void OnLinewidth2();
	afx_msg void OnLinewidth3();
	afx_msg void OnLinewidth4();
};

#ifndef _DEBUG  // debug version in example2View.cpp
inline CExample3Doc* CExample3View::GetDocument() const
   { return reinterpret_cast<CExample3Doc*>(m_pDocument); }
#endif

