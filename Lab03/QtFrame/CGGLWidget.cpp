#include "CGGLWidget.h"

#ifdef __APPLE__
#include <glu.h>
#elif _WIN32
#include <gl/glu.h>
#endif

#include <QMouseEvent>
#include <qpushbutton.h>
#include <QHBoxLayout>

#include "QtMainWindow.h"

CGGLWidget::CGGLWidget(QWidget *parent) : QOpenGLWidget(parent)
{
	setMouseTracking(true);

	QtMainWindow* mainwindow = dynamic_cast<QtMainWindow*>(parent);

	connect(mainwindow->actionLine, &QAction::triggered, this, &CGGLWidget::OnPrimitivesLine);
	connect(mainwindow->actionQuadrilateral, &QAction::triggered, this, &CGGLWidget::OnPrimitivesQuadrilateral);
	connect(mainwindow->actionRed, &QAction::triggered, this, &CGGLWidget::OnColorRed);
	connect(mainwindow->actionGreen, &QAction::triggered, this, &CGGLWidget::OnColorGreen);
	connect(mainwindow->actionBlue, &QAction::triggered, this, &CGGLWidget::OnColorBlue);
	connect(mainwindow->actionYellow, &QAction::triggered, this, &CGGLWidget::OnColorYellow);
	connect(mainwindow->actionSolidLine, &QAction::triggered, this, &CGGLWidget::OnFillSolidline);
	connect(mainwindow->actionFill, &QAction::triggered, this, &CGGLWidget::OnFillFill);
	connect(mainwindow->action1, &QAction::triggered, this, &CGGLWidget::OnLinewidth1);
	connect(mainwindow->action2, &QAction::triggered, this, &CGGLWidget::OnLinewidth2);
	connect(mainwindow->action3, &QAction::triggered, this, &CGGLWidget::OnLinewidth3);
	connect(mainwindow->action4, &QAction::triggered, this, &CGGLWidget::OnLinewidth4);

}

CGGLWidget::~CGGLWidget(void)
{

}

void CGGLWidget::initializeGL()
{
	
}

void CGGLWidget::paintGL()
{
	m_iGL.Display();
}

void CGGLWidget::resizeGL(int w, int h)
{
	m_iGL.SetProjectView(w, h);
}

void CGGLWidget::mouseMoveEvent(QMouseEvent *mevent)
{
	m_iGL.OnMouseEvent(MOVE, mevent->x(), mevent->y());
	update();
}

void CGGLWidget::mousePressEvent(QMouseEvent *mevent)
{
	if (mevent->button() == Qt::MouseButton::LeftButton)
	{
		m_iGL.OnMouseEvent(LBUTTON_DOWN, mevent->x(), mevent->y());
		update();

	}

}

void CGGLWidget::OnPrimitivesLine()
{
	m_iGL.SetPrimitiveType(LINE);
}
void CGGLWidget::OnPrimitivesQuadrilateral()
{
	m_iGL.SetPrimitiveType(QUAD);
}
void CGGLWidget::OnColorBlue()
{
	m_iGL.SetColor(COLOR[BLUE]);
}
void CGGLWidget::OnColorGreen()
{
	m_iGL.SetColor(COLOR[GREEN]);
}
void CGGLWidget::OnColorRed()
{
	m_iGL.SetColor(COLOR[RED]);
}
void CGGLWidget::OnColorYellow()
{
	m_iGL.SetColor(COLOR[YELLOW]);
}
void CGGLWidget::OnFillFill()
{
	m_iGL.SetFillType(GL_POLYGON);
}
void CGGLWidget::OnFillSolidline()
{
	m_iGL.SetFillType(GL_LINE_LOOP);
}
void CGGLWidget::OnLinewidth1()
{
	m_iGL.SetLineWidth(1.0);
}
void CGGLWidget::OnLinewidth2()
{
	m_iGL.SetLineWidth(2.0);
}
void CGGLWidget::OnLinewidth3()
{
	m_iGL.SetLineWidth(3.0);
}
void CGGLWidget::OnLinewidth4()
{
	m_iGL.SetLineWidth(4.0);
}