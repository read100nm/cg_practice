#ifndef OPENGL_PROJECTORSCREEN_VIEWER_H_011
#define OPENGL_PROJECTORSCREEN_VIEWER_H_011

#include <qopenglwidget.h>
#include "GLInterface.h"

class QPushButton;

class CGGLWidget :public QOpenGLWidget
{
	Q_OBJECT

public:
	CGGLWidget(QWidget *parent = 0);
	~CGGLWidget();

	public slots:
	void OnPrimitivesLine();
	void OnPrimitivesQuadrilateral();
	void OnColorBlue();
	void OnColorGreen();
	void OnColorRed();
	void OnColorYellow();
	void OnFillFill();
	void OnFillSolidline();
	void OnLinewidth1();
	void OnLinewidth2();
	void OnLinewidth3();
	void OnLinewidth4();

protected:
	void initializeGL();
	void resizeGL(int w, int h);
	void paintGL();

	virtual void mouseMoveEvent(QMouseEvent *mevent);
	virtual void mousePressEvent(QMouseEvent *mevent);

	CGLInterface m_iGL;
};

#endif
