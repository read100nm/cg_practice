#include "GLInterface.h"



float BLUE[3] = { 0.0, 0.0, 1.0 };  // Blue
float YELLOW[3] = { 1.0, 1.0, 0.0 };
float GREEN[3] = { 0.0, 1.0, 0.0 };
float RED[3] = { 1.0, 0.0, 0.0 };
float WHITE[3] = { 1.0, 1.0, 1.0 };  // Blue
float GRAY[3] = { 0.5, 0.5, 0.5 };  // Blue
#define GROUND_LIMIT 10000
#define GROUND_LIMITF 10000.0f

int QUADLIST;
int CUBLIST;

void CGLInterface::DrawScene()
{
	/*		     Y
	7----|--------6
	/     |       /|
	/      |      / |
	4------|-----5| |
	|  2   /------|-/1------->x
	|     /       |/
	|3---/--------0
	/z					*/
	GLfloat cube[][3] = { { 2.0, -2.0, 2.0 }, { 2.0, -2.0, -2.0 }
		, { -2.0, -2.0, -2.0 }, { -2.0, -2.0, 2.0 }
		, { -2.0, 2.0, 2.0 }, { 2.0, 2.0, 2.0 }
	, { 2.0, 2.0, -2.0 }, { -2.0, 2.0, -2.0 } };

	//?? 정육면체를 list로 정의하시오.
	//?? define a cube as a list.


	CUBLIST = glGenLists(1);
	//?? 사실적인 그래픽 생성을 위해서는 배면과 은선 제거 
	//?? 생성된 육면체의 MC와 WC를 일치시킨다. 즉 MC를 WC로 사용한다.
	//?? CC의 원점을 WC의 (2,0,0)에 두고, z 축을 WC의 원점을 바라보게, 그리고 y 축을 (0,1,0)과 일치되도록 한다.
	//?? In order to produce realistic graphics, hidden line removal and backside.
	//?? The cube was created to match the MC and WC. That MC is used as a WC.
	//?? The origin of the CC is on (2, 0, 0) of the WC, and y-axis of the CC matches (0, 1, 0) of the WC.
	//?? The z-axis of CC is pointed the origin of WC.


	//?? 육면체 그리기 함수와 리스트 호출
	//?? call a drawing cube function and list.

}

CGLInterface::CGLInterface(void)
{
}

CGLInterface::~CGLInterface(void)
{
}

void CGLInterface::SetProjectView(int cx, int cy)
{
	// cx와 cy는 클라이언트 영역의 x축, y축 크기

	// TODO: Add your message handler code here
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	/*
	if( cx <= cy )
	glOrtho(-2.0, 2.0, -2.0*(GLfloat)cy/(GLfloat)cx,
	2.0*(GLfloat)cy/(GLfloat)cx, -10.0, 10.0);
	else
	glOrtho(-2.0*(GLfloat)cx/(GLfloat)cy,
	2.0*(GLfloat)cx/(GLfloat)cy, -2.0, 2.0, -10.0, 10.0);
	*/
	gluPerspective(65.0, (GLfloat)cx / (GLsizei)cy, 1.0, 500.0);

	glViewport(0, 0, cx, cy);
	glMatrixMode(GL_MODELVIEW);
}

void CGLInterface::InitializeDrawing()  // 그리기에 필효한 변수를 초기화 한다. 
{

}


void CGLInterface::OnKeyDown(unsigned char keyValue)
{
	// TODO: Add your message handler code here and/or call default
	switch (keyValue)
	{
	case 's':
	case 'S':
		break;
	case 'e':
	case 'E':
		break;
	case GLIF_RIGHT:
		break;
	case GLIF_LEFT:
		break;
	}
}



void CGLInterface::Display()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);	// buffer clear color 설정
	glClear(GL_COLOR_BUFFER_BIT);	// 설정된 color로 버퍼를 clear
	DrawScene();
	glFlush();
}

/*=================================================================================================
Function: Dispaly a quad
Pre: array cube should be defined.
=================================================================================================*/
void CGLInterface::DisplayQuad(GLfloat cube[][3], int a, int b, int c, int d)
{

	//	 glBegin(GL_LINE_LOOP);
	glBegin(GL_POLYGON);
	glVertex3fv(cube[a]); // 0
	glVertex3fv(cube[b]); // 1
	glVertex3fv(cube[c]); // 2
	glVertex3fv(cube[d]); // 3
	glEnd();
	glEnd();
}

void CGLInterface::DisplayCube(GLfloat cube[][3])
{
	//?? 육면체를 그려라 
	//?? Draw a cube
}
