#ifndef GL_INTERFACE_H
#define GL_INTERFACE_H

#ifdef WIN32
#include <Windows.h>
#endif

#ifndef _APPLE_OSX
#include <gl/GL.h>
#include <gl/GLU.h>
#else
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#endif

#define GLIF_RIGHT          0x27
#define GLIF_LEFT           0x25
class CGLInterface
{
public:
	CGLInterface();
	virtual ~CGLInterface();

	/// set projection
	void SetProjectView(int cx, int cy);

	/// draw display by OpenGL functions
	void Display();

	/// Key Event handler
	void OnKeyDown(unsigned char keyValue);

	void DisplayQuad(GLfloat cube[][3], int a, int b, int c, int d);
	void DisplayCube(GLfloat cube[][3]);

public:
	void InitializeDrawing();  // variable initialization. 그리기에 필요한 변수를 초기화 한다. 
	void DrawScene();  // Set the camera WRT WC and display the scene(카메라를 설정하고 scene 전체를 그린다) 

private:
};

#endif
