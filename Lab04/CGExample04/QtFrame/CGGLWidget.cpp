#include "CGGLWidget.h"

#ifdef __APPLE__
#include <glu.h>
#elif _WIN32
#include <gl/glu.h>
#endif

#include <QMouseEvent>
#include <qpushbutton.h>
#include <QHBoxLayout>

#include "QtMainWindow.h"

CGGLWidget::CGGLWidget(QWidget *parent) : QOpenGLWidget(parent)
{
	setMouseTracking(true);

	QtMainWindow* mainwindow = dynamic_cast<QtMainWindow*>(parent);
}

CGGLWidget::~CGGLWidget(void)
{

}

void CGGLWidget::initializeGL()
{
	
}

void CGGLWidget::paintGL()
{
	m_iGL.Display();
}

void CGGLWidget::resizeGL(int w, int h)
{
	m_iGL.SetProjectView(w, h);
}
