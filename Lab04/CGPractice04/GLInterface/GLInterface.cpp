#include "GLInterface.h"

void CGLInterface::DrawScene()
{
	//깊이 검사를 사용.
	glEnable(GL_DEPTH_TEST);

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	//glClear(GL_COLOR_BUFFER_BIT);
	//버퍼 초기화
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//모델뷰 관측
	glMatrixMode(GL_MODELVIEW);
	//행렬 초기화
	glLoadIdentity();

	glScalef(m_fZoom, m_fZoom, 1.0f);

	glRotatef(m_fTheta, 0.0, 1.0, 0.0);
	glRotatef(m_fTheta2, 1.0, 0.0, 0.0);

	GLfloat vertices[8][3] = {
		{ -1, -1, 1 }, { -1, 1, 1 },
		{ 1, 1, 1 }, { 1, -1, 1 }, { -1, -1, -1 },
		{ -1, 1, -1 }, { 1, 1, -1 }, { 1, -1, -1 }
	};

	GLfloat colors[8][3] = {
		{ 0, 0, 1 }, { 0, 1, 1 },
		{ 1, 1, 1 }, { 1, 0, 1 }, { 0, 0, 0 },
		{ 0, 1, 0 }, { 1, 1, 0 }, { 1, 0, 0 }
	};

	//디스플레이 리스트 시작
	glNewList(CUBE, GL_COMPILE);
	glBegin(GL_QUADS);
	// quad #1	앞면
	glColor3fv(colors[0]);	glVertex3fv(vertices[0]);
	glColor3fv(colors[3]);	glVertex3fv(vertices[3]);
	glColor3fv(colors[2]);	glVertex3fv(vertices[2]);
	glColor3fv(colors[1]);	glVertex3fv(vertices[1]);
	// quad #2	우측면
	glColor3fv(colors[2]);	glVertex3fv(vertices[2]);
	glColor3fv(colors[3]);	glVertex3fv(vertices[3]);
	glColor3fv(colors[7]);	glVertex3fv(vertices[7]);
	glColor3fv(colors[6]);	glVertex3fv(vertices[6]);
	// quad #3	밑면
	glColor3fv(colors[3]);	glVertex3fv(vertices[3]);
	glColor3fv(colors[0]);	glVertex3fv(vertices[0]);
	glColor3fv(colors[4]);	glVertex3fv(vertices[4]);
	glColor3fv(colors[7]);	glVertex3fv(vertices[7]);
	// quad #4	윗면
	glColor3fv(colors[1]);	glVertex3fv(vertices[1]);
	glColor3fv(colors[2]);	glVertex3fv(vertices[2]);
	glColor3fv(colors[6]);	glVertex3fv(vertices[6]);
	glColor3fv(colors[5]);	glVertex3fv(vertices[5]);
	// quad #5	뒷면
	glColor3fv(colors[4]);	glVertex3fv(vertices[4]);
	glColor3fv(colors[5]);	glVertex3fv(vertices[5]);
	glColor3fv(colors[6]);	glVertex3fv(vertices[6]);
	glColor3fv(colors[7]);	glVertex3fv(vertices[7]);
	// quad #6	좌측면
	glColor3fv(colors[5]);	glVertex3fv(vertices[5]);
	glColor3fv(colors[4]);	glVertex3fv(vertices[4]);
	glColor3fv(colors[0]);	glVertex3fv(vertices[0]);
	glColor3fv(colors[1]);	glVertex3fv(vertices[1]);
	glEnd();
	glEndList();	//디스플레이 리스트 끝

	glCallList(CUBE);

	SwapBuffers(wglGetCurrentDC());

}
CGLInterface::CGLInterface(void)
{
	InitializeDrawing();
}

CGLInterface::~CGLInterface(void)
{
}

void CGLInterface::SetProjectView(int cx, int cy)
{
	// cx와 cy는 클라이언트 영역의 x축, y축 크기

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	//클리핑 공간의 범위를 설정, orthogonal projection
	//윈도우의 크기가 변할 때 마다 짧은쪽이 2에서 -2의 범위를 가지도록 한다. 
	if (cx <= cy)
		glOrtho(-2.0, 2.0, -2.0*(GLfloat)cy / (GLfloat)cx,
		2.0*(GLfloat)cy / (GLfloat)cx, -10.0, 10.0);
	else
		glOrtho(-2.0*(GLfloat)cx / (GLfloat)cy,
		2.0*(GLfloat)cx / (GLfloat)cy, -2.0, 2.0, -10.0, 10.0);


	/*
	if( cx <= cy )
	glFrustum(-2.0, 2.0, -2.0*(GLfloat)cy/(GLfloat)cx,
	2.0*(GLfloat)cy/(GLfloat)cx, 1.0, 20.0);
	else
	glFrustum(-2.0*(GLfloat)cx/(GLfloat)cy,
	2.0*(GLfloat)cx/(GLfloat)cy, -2.0, 2.0, 1.0, 10.0);

	if( cx <= cy )
	gluPerspective(50,(GLfloat)cy/(GLfloat)cx,1,10.0);
	else
	gluPerspective(50,(GLfloat)cx/(GLfloat)cy,1,10.0);
	*/


	glViewport(0, 0, cx, cy);
}

void CGLInterface::InitializeDrawing()  // 그리기에 필효한 변수를 초기화 한다. 
{
	m_fTheta = 0.0;
	m_fTheta2 = 0.0;

	m_fZoom = 1.0f;
}


void CGLInterface::OnKeyDown(unsigned char keyValue)
{
	// TODO: Add your message handler code here and/or call default
	switch (keyValue)
	{
	case 'n':
	case 'N':
		break;
	case 'f':
	case 'F':
		break;
	case GLIF_RIGHT:
		break;
	case GLIF_LEFT:
		break;
	}
}



void CGLInterface::Display()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);	// buffer clear color 설정
	glClear(GL_COLOR_BUFFER_BIT);	// 설정된 color로 버퍼를 clear
	DrawScene();
	glFlush();
}
