#include "GLInterface.h"


CGLInterface::CGLInterface()
{
	InitializeDrawing(); // initialize all the variables necessary for this program.
}

CGLInterface::~CGLInterface()
{

}


void CGLInterface::Initialize()
{
	/*		     Y
	7----|--------6
	/     |       /|
	/      |      / |
	4------|-----5| |
	|  2   /------|-/1------->x
	|     /       |/
	|3---/--------0
	/z					*/
	GLfloat plane[][3] = { { 2.0, -1.0, 2.0 }, { 2.0, -1.0, -2.0 }, { -2.0, -1.0, -2.0 }, { -2.0, -1.0, 2.0 } };
	GLfloat cube[][3] = { { 2.0, -2.0, 2.0 }, { 2.0, -2.0, -2.0 }
		, { -2.0, -2.0, -2.0 }, { -2.0, -2.0, 2.0 }
		, { -2.0, 2.0, 2.0 }, { 2.0, 2.0, 2.0 }
	, { 2.0, 2.0, -2.0 }, { -2.0, 2.0, -2.0 } };

	//Register a unit Square as a list. The cube is defined on xz plane with the center as (0,0,0)
	//?? 예제 5.1 : 사각형을 list로 모델링 하라.
	//?? Example 5.1 : Model a rectangle using list.
	QUADLIST = glGenLists(1);
	glNewList(QUADLIST, GL_COMPILE);
	DisplayQuad(plane, 0, 1, 2, 3);
	glEndList();

	//?? 예제 5.1 : 육면체를 list로 모델링 하라.
	//?? Example 5.1 : Model a cube using list.
	CUBLIST = glGenLists(2);


}

void CGLInterface::SetProjectView(int cx, int cy)
{
	// cx와 cy는 클라이언트 영역의 x축, y축 크기

	// TODO: Add your message handler code here
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	

	// 65.0 fovy
	// ratio x to y
	// znear 
	// zfar
	gluPerspective(65.0, (GLfloat)cx / (GLsizei)cy, 1.0, 500.0);

	glViewport(0, 0, cx, cy);
	glMatrixMode(GL_MODELVIEW);
}

void CGLInterface::Display()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);	// buffer clear color 설정
	glClear(GL_COLOR_BUFFER_BIT);	// 설정된 color로 버퍼를 clear
	DrawScene();
	glFlush();
}

void CGLInterface::InitializeDrawing()  // 그리기에 필효한 변수를 초기화 한다. 
{
	g_Viewer[0] = 50.0f;
	g_Viewer[1] = 80.0f;
	g_Viewer[2] = 130.0f;

	g_Center[0] = 0.0f;
	g_Center[1] = 0.0f;
	g_Center[2] = 0.0f;	// 카메라의 초기 초점

	// 컬러 설정
	BLUE[0] = 0; BLUE[1] = 0; BLUE[2] = 1;				// Blue
	YELLOW[0] = 1.0; YELLOW[1] = 1.0; YELLOW[2] = 0;	// YELLOW
	GREEN[0] = 0; GREEN[1] = 1.0; GREEN[2] = 0;			// YELLOW
	RED[0] = 1.0; RED[1] = 0; RED[2] = 0;				// RED
	WHITE[0] = 1.0; WHITE[1] = 1.0; WHITE[2] = 1.0;		// WHITE
	GRAY[0] = 0.5; GRAY[1] = 0.5; GRAY[2] = 0.5;		// GRAY

	CUBLIST = -1;
}

void CGLInterface::OnKeyDown(unsigned char keyValue)
{
	// TODO: Add your message handler code here and/or call default
	switch (keyValue)
	{
	case 's':
	case 'S':
		break;
	case 'e':
	case 'E':
		break;
	case GLIF_RIGHT:
		break;
	case GLIF_LEFT:
		break;
	}
}


void CGLInterface::DrawScene()
{
	glEnable(GL_DEPTH_TEST);	// Enable hidden surface removal
	glMatrixMode(GL_MODELVIEW); // Modelview 행렬 스택 선택
	//?? 예제 5.1: CC와 WC 일치시킨 다음에 사각형을 출력하라. 결과를 바탕으로 CC, WC, MC의 관계를 설명하라. 
	//?? Example 5.1 : Match CC and WC. after then draw a rectagle. Exaplain the relationship CC, WC, and MC.
	glLoadIdentity();			// 현재 변환 행렬을 단위 행렬로 대치

	// viewing 변환 정의
	//?? 예제 5.1: CC와 WC가 그림에 정의된 관계를 갖도록 정의하고 사각형을 출력하라. 이때 WC와 MC의 관계를 설명하고 WC의 기준이 되는 것이 무엇인지를 설명하라. 
	//?? Example 5.1: CC and WC in the figure, define to have a defined relationship. Describe the relationship between WC and MC, and that what is based on WC Explain.
	//gluLookAt(0.0, 50.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0);

	//?? 예제 5.2 : glLookAt() 대신에 glTranslatef()와 glRotatef() 변환함수를 이용하여 CC와 WC의 관계를 정의
	//?? Example 5.2: glLookAt() instead of the glTranslatef() and the glRotatef() conversion function by defining the relationship between CC and WC

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // RGB color display
	// World coordinate를 그리린다.
	glColor3fv(RED);
	DrawLine(0.0, 0.0, 0.0, GROUND_LIMITF, 0.0, 0.0); // x 축 
	glColor3fv(GREEN);
	DrawLine(0.0, 0.0, 0.0, 0.0, GROUND_LIMITF, 0.0); // Y 축 
	glColor3fv(BLUE);
	DrawLine(0.0, 0.0, 0.0, 0.0, 0.0, GROUND_LIMITF); // Z 축 

	glCallList(CUBLIST);
	glCallList(QUADLIST);

	//?? 실습 5.3.2:Cube를 중심이 WC상의 (5, 0.5, 3)에 출력하라.
	//?? Practice 5.3.2: Cube and WC on the center (5, 0.5, 3) make the output.

	//?? 실습 5.3.3: Cube를 X, Y, Z 축에 대해서 각각 2배, 4배, 3배 확대하여 생성한  직육면체를 WC 상의 (5.0, 0.0, 3.)에 위치시켜라
	//?? Practice 5.3.3: Cube and X, Y, Z axis, respectively, for the 2x, 4x, 3x zoom and a WC on the generated rectangular (5.0, 0.0, 3) and push them to

	//?? 실습 5.3.4: 같은 물체를 WC 상의 (5, 2, 3)에 출력한 결과와 3의 결과가 어떻게 다른 지를 설명하고 이 차이를 보기 위해서는 어떻게 해야 하는 지를 설명하라. 
	//?? Practice 5.3.4: WC on the same object (5, 2, 3) printed on the results and explain how different the results of 3, and in order to see this difference explain what to do.

	//?? 실습 5.3.6: Cube를 y축에 관하여 R도 회전하고 x,y,z 축에 관하여 Sx,Sy, Sz 만큼 확대 한 다음, WC상의 (x,y,z)에 배치하는 함수(PlaceObject(R,Sx,Sy,Sz,x,y,z))를 작성하라
	//?? Lab 5.3.6: Cube rotation R about the y-axis and the x, y, z axis about the Sx, Sy, Sz much larger then, WC on the (x, y, z) are placed on the function (PlaceObject (R , Sx, Sy, Sz, x, y, z)) Please write
}


/*=================================================================================================
Function: Dispaly a line
Pre: start and end points should be defined.
=================================================================================================*/
void CGLInterface::DrawLine(GLfloat sx, GLfloat sy, GLfloat sz, GLfloat ex, GLfloat ey, GLfloat ez)
{
	glBegin(GL_LINE_LOOP);
	glVertex3f(sx, sy, sz); // 0
	glVertex3f(ex, ey, ez); // 0
	glEnd();
}

/*=================================================================================================
Function: Dispaly a quad
Pre: array cube should be defined.
=================================================================================================*/
void CGLInterface::DisplayQuad(GLfloat cube[][3], int a, int b, int c, int d)
{
	glBegin(GL_LINE_LOOP);
		glVertex3fv(cube[a]); // 0
		glVertex3fv(cube[b]); // 1
		glVertex3fv(cube[c]); // 2
		glVertex3fv(cube[d]); // 3
	glEnd();
}

/*=================================================================================================
Function: place a object by using instance transform
Pre: 
=================================================================================================*/
void CGLInterface::PlaceObject(float scaleX, float scaleY, float scaleZ,
	float rotateY, float posX, float posY, float posZ)
{
	glPushMatrix();	// 행렬 스택에 현재의 행렬을 push
	glTranslatef(posX, posY, posZ); // 해당 위치로 이동 
	glRotatef(rotateY, 0.0, 1.0, 0.0); // Y축에 대한 회전
	glScalef(scaleX, scaleY, scaleZ);
	glCallList(CUBLIST);
	glPopMatrix();	// 현재 행렬을 그 행렬 스택에서 밖으로 꺼낸다
}