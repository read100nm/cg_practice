#ifndef GL_INTERFACE_H
#define GL_INTERFACE_H

#ifdef WIN32
#include <Windows.h>
#endif

#ifndef _APPLE_OSX
#include <gl/GL.h>
#include <gl/GLU.h>
#else
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#endif

#define GLIF_RIGHT          0x27
#define GLIF_LEFT           0x25

#define GROUND_LIMIT 10000
#define GROUND_LIMITF 10000.0f

class CGLInterface
{
public:
	/// Constructor
	CGLInterface();

	/// Destructor
	virtual ~CGLInterface();

	/// Initialization
	void Initialize();

	/// set projection
	void SetProjectView(int cx, int cy);

	/// draw display by OpenGL functions
	void Display();


	

	/// Key Event handler
	void OnKeyDown(unsigned char keyValue); 

	/// variable initialization. 그리기에 필요한 변수를 초기화 한다. 
	void InitializeDrawing();				

	/// Set the camera WRT WC and display the scene(카메라를 설정하고 scene 전체를 그린다) 
	void DrawScene();	

	/// X, Y, Z 축 라인 그리기
	void DrawLine(GLfloat sx, GLfloat sy, GLfloat sz, GLfloat ex, GLfloat ey, GLfloat ez);

	/// 사각형 그리는 함수
	void DisplayQuad(GLfloat cube[][3], int a, int b, int c, int d);

	/// 크기, 회전, 위치 변환 후 object를 그려주는 함수 
	void CGLInterface::PlaceObject(float scaleX, float scaleY, float scaleZ,
		float rotateY, float posX, float posY, float posZ);


protected:
	GLdouble g_Viewer[3];	// Camera position
	GLdouble g_Center[3];	// the point where the camera is looking at

	float BLUE[3];  // Blue
	float YELLOW[3];// YELLOW
	float GREEN[3]; // GREEN
	float RED[3];   // RED
	float WHITE[3]; // WHITE
	float GRAY[3];  // GRAY

	int QUADLIST;
	int CUBLIST;
};

#endif
