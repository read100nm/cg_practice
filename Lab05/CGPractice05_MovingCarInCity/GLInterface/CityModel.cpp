#include "CityModel.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

/*=====================================  CCityModel Class    =============================================
Function: distructor
=================================================================================================*/
CityModel::~CityModel()
{
}


/*=====================================  CCityModel Class    =============================================
Function: Constructor
Pre : 
Post: 
=====================================================================================================*/
CityModel::CityModel()
{
	Unit = 5.0f;	// city model의 scale을 결정한다. 
	buildingBlockWidth = 6.0f*Unit;
	sideWalkWidth = 2.0f*Unit;
	streetMidleBlockSize = 16.0f*Unit;
	streetCornerSize=6.0f*Unit;
	streetWidth = 16.0f*Unit;
	crossRoadBlockSize=38.0f*Unit;
	sideWalkColor[0]=GRAY[0];// sideWalk color
	sideWalkColor[1]=GRAY[1];
	sideWalkColor[2]=GRAY[2];  

	// 컬러 설정
	BLUE[0] = 0; BLUE[1] = 0; BLUE[2] = 1;				// Blue
	YELLOW[0] = 1.0; YELLOW[1] = 1.0; YELLOW[2] = 0;	// YELLOW
	GREEN[0] = 0; GREEN[1] = 1.0; GREEN[2] = 0;			// YELLOW
	RED[0] = 1.0; RED[1] = 0; RED[2] = 0;				// RED
	WHITE[0] = 1.0; WHITE[1] = 1.0; WHITE[2] = 1.0;		// WHITE
	GRAY[0] = 0.5; GRAY[1] = 0.5; GRAY[2] = 0.5;		// GRAY
}

GLfloat hRatio[3]={1.5,1.5,1.0}; // building high ration. high=length*highRatio

/*=====================================  CCityModel Class    =============================================
 Function: draw a straight street block
		   It has it's own coordinate. 
		   Constructed by using the instance transformation and the basic cube
		   The layout of this block:

 U: unit , W = 6U, L=16U
 side walk : scale : (16U, 1.0 , 2U), location: (0, 0, -2U)
Building 1:  scale : (4U,  4*hRatio[0] , 4U),  location: (-6, 0, 4*hRatio[0] /2.0, 1U) 
Building 2:  scale : (8U,  8*hRatio[1] , 4U),  location: (0,  8*hRatio[1] /2.0, 1U)
Building 3:  scale : (4U,  4*hRatio[2] , 4U),  location: (6, 0, 4*hRatio[0] /2.0, 1U)


=================================================================================================*/
void CityModel::DrawStreetMidleBlock(GLfloat xpos, GLfloat zpos, GLfloat yRotate)
{
	glPushMatrix();
	glTranslatef(xpos,0.0f,zpos);
	glRotatef(yRotate,0.0f,1.0f,0.0f);


	// Draw Sidewalk 
	glColor3fv(sideWalkColor);
 	//(float xScale, float zScale, float yRotate, float xPos, float zPos)
  	DrawPlane(16.0f*Unit, 2.0f*Unit, 0.0f, 0.0f, -2.0f*Unit);
	
	//?? Draw the building 1: 
	//?? Draw the building 2: 
	//?? Draw the building 3: 

	glPopMatrix();	// 현재 행렬을 그 행렬 스택에서 밖으로 꺼낸다
}


/*=====================================  CCityModel Class    =============================================
Function: 
 Function: draw a street corner block
		   It has it's own coordinate. 
		   Constructed by using the instance transformation and the basic cube
		   The layout of this block:
				Width = 6U, Length = 6U
				Building :  scale (4.0U, 4.0*U), Position (1U, 1U)
				Side Walk 1: scale (6*U, 2*U), Position (0, -2*U)
				Side Walk 2: scale (2*U, 4*U), Position (-2U, 1U)

==================================================================================================*/
void CityModel::DrawStreetCornerBlock(GLfloat xpos, GLfloat zpos, GLfloat yRotate)
{
	glPushMatrix();
	glTranslatef(xpos,0.0,zpos);
	glRotatef(yRotate,0.0,1.0,0.0);

	//?? Draw Sidewalk 
	glColor3fv(sideWalkColor);
					
	//?? Upper Left sidewalk:	
 	glColor3fv(sideWalkColor);

	//?? Draw the building 1: 
	glColor3fv(GREEN);

	glPopMatrix();	// 현재 행렬을 그 행렬 스택에서 밖으로 꺼낸다
				
}


/*=====================================  CCityModel Class    =============================================
Function: Model one section of a crossroad by combining the street corner blocks and mid section blcoks
	This block size :  38 x 38
	Corner block location :  ( -(19-3), -(19-3) )  or (-16, -16)
	Upper right straight block location : (19-14, -(19-3)) or (19-14, -(19-3)) or (-5, -16)
	Upper left straight block location : ( (19-8), -(19-3))   or (11, -16)

	Left upper straight block location :  (-16, -5)
	Left Upper straight block location :  (-16, 11)
=====================================================================================================*/
void CityModel::DrawCrossRoadBlock(GLfloat xpos, GLfloat zpos, GLfloat yRotate)
{
	glPushMatrix();
	glPopMatrix();	// 현재 행렬을 그 행렬 스택에서 밖으로 꺼낸다
}


/*=====================================  CCityModel Class    =============================================
Function: Model a crossroad by combining the crossroad blocks and mid section blcoks
Pre : 
Post: 
=====================================================================================================*/
void CityModel::DrawCrossRoad(GLfloat xpos, GLfloat zpos, GLfloat yRotate)
{
	GLfloat xAccum, zAccum;

	glPopMatrix();	// 현재 행렬을 그 행렬 스택에서 밖으로 꺼낸다
}



/* 

/*=====================================  CCityModel Class    =============================================
Function: Draw checkerboard textures on the ground. 
Pre: the ground is defined on the XZ plane
=================================================================================================*/
void CityModel::DrawGround()
{
	int r, c;
	int nStep = 10;

	glBegin(GL_LINES); // 커다란 면에 선을 긋는다
		for(r = -GROUND_LIMIT; r  <= GROUND_LIMIT; r += nStep) // X축상으로 line draw
		{
			glVertex3f((float)r, 0.0f, -GROUND_LIMITF);
			glVertex3f((float)r, 0.0f, GROUND_LIMITF);
		}
		for(c = -GROUND_LIMITF; c <= GROUND_LIMITF; c += nStep)	// Z축상으로 line draw
		{
			glVertex3f(GROUND_LIMITF, 0.0f, (float)c);
			glVertex3f(-GROUND_LIMITF, 0.0f, (float)c);
		}
	glEnd();
}


/* ==================================================================================================
Function : Draw a line 
====================================================================================================*/
void CityModel::DrawLine(float xStart, float yStart, float zStart, float xEnd, float yEnd, float zEnd)
{
	glBegin(GL_LINES); 
		glVertex3f(xStart,yStart,zStart);
		glVertex3f(xEnd, yEnd, zEnd);
	glEnd();
}

/*==================================================================================
Function: Display the quad deinfed by the "RegQuad()" at (xPos,zPos) 
		  after scaling it by using the paramter (xScale, zScale) 
			and rotating it with respect to y axis by yRoate
====================================================================================*/

void CityModel::DrawPlane(float xScale, float zScale, float yRotate, float xPos, float zPos)
	{
 	glPushMatrix();	// 행렬 스택에 현재의 행렬을 push
		glTranslatef(xPos,0.0,zPos); // 해당 위치로 이동 
		glRotatef(yRotate,0.0,1.0,0.0); // Y축에 대한 회전
		glScalef(xScale,1.0,zScale);
		glCallList(QUADLIST);

	glPopMatrix();	// 현재 행렬을 그 행렬 스택에서 밖으로 꺼낸다
	}



/*==================================================================================
Function: Place a cube defined by the function "RegCube()" at the position (posX,posY,posY) 
		  after scaling the cube by using the parameter (scaleX, scaleY, scaleZ)
	====================================================================================*/
void CityModel::PlaceObject(float scaleX, float scaleY, float scaleZ,
								 float rotateY, float posX, float posY, float posZ)
{
	glPushMatrix();	// 행렬 스택에 현재의 행렬을 push
	glTranslatef(posX,posY,posZ); // 해당 위치로 이동 
    glRotatef(rotateY,0.0,1.0,0.0); // Y축에 대한 회전
	glScalef(scaleX,scaleY,scaleZ);
	glCallList(CUBLIST);
	glPopMatrix();	// 현재 행렬을 그 행렬 스택에서 밖으로 꺼낸다
}

/*=================================================================================================
 Function: Dispaly a quad
 Pre: array cube should be defined.
=================================================================================================*/
void CityModel::DisplayQuad(GLfloat cube[][3], int a, int b, int c, int d)
{ 

//	 glBegin(GL_LINE_LOOP);
	glBegin(GL_POLYGON); 

		glVertex3fv(cube[a]); // 0
		glVertex3fv(cube[b]); // 1
		glVertex3fv(cube[c]); // 2
		glVertex3fv(cube[d]); // 3
		glEnd();
	glEnd();
}


void CityModel::DefinePrimitiveListforScene()
{
	
	GLfloat unitSquare[][3]	= {{0.5, 0.0, 0.5}, {0.5, 0, -0.5}
						,{-0.5, 0.0, -0.5}, {-0.5, 0.0, 0.5}};

/*		     Y
		7----|--------6
       /     |       /|
      /      |      / | 
      4------|-----5| | 
      |  2   /------|-/1------->x
      |     /       |/
	  |3---/--------0					
	      /z					*/
	GLfloat unitCube[][3]	= {{0.5, -0.5, 0.5}, {0.5, -0.5, -0.5} 
						,{-0.5, -0.5, -0.5}, {-0.5, -0.5, 0.5}
						,{-0.5, 0.5, 0.5}, {0.5, 0.5, 0.5}
						,{0.5, 0.5, -0.5}, {-0.5, 0.5, -0.5}};
	GLfloat surfaceNorm[][3] = 	{
						{0.0, 0.0, 1.0}		 // 앞면		
						,{1.0, 0.0, 0.0}	 //오른쪽 면	
						,{0.0, 0.0, -1.0}	 // 뒷면
						,{-1.0, 0.0, 0.0}	 // 왼쪽면
						,{0.0, 1.0, 0.0}	  // 윗면		
						,{0.0, -1.0, 0.0}	 // 밑면	
						};
	//Register a unit Square as a list. The cube is defined on xz plane with the center as (0,0,0)
	QUADLIST = glGenLists(1);
	glNewList(QUADLIST, GL_COMPILE);
		glNormal3fv(surfaceNorm[4]);
		DisplayQuad(unitSquare,0,1,2,3); 
	glEndList();

	CUBLIST = glGenLists(2);
	glNewList(CUBLIST, GL_COMPILE);
		glNormal3fv(surfaceNorm[0]);
		DisplayQuad(unitCube,0,5,4,3); // 앞면
		glNormal3fv(surfaceNorm[1]);
		DisplayQuad(unitCube,0,1,6,5); //오른쪽 면
		glNormal3fv(surfaceNorm[2]);
		DisplayQuad(unitCube,1,2,7,6); // 뒷면
		glNormal3fv(surfaceNorm[3]);
		DisplayQuad(unitCube,4,7,2,3); // 왼쪽면
		glNormal3fv(surfaceNorm[4]);
		DisplayQuad(unitCube,4,5,6,7);  // 윗면
		glNormal3fv(surfaceNorm[5]);
		DisplayQuad(unitCube,0,1,2,3); // 밑면
	glEndList();	

}

/*======================================= CCityModel Class  ============================================
 Function: build a city by connecting the crossroads

=================================================================================================*/
void CityModel::RenderScene()
{
/*	GLfloat g_Viewer[3];
	GLfloat g_Center[3];
	g_Viewer[0] = 0.0f;
	g_Viewer[1] = 250.0f;
	g_Viewer[2] = 0.0f;

	g_Center[0] = 0.0f;
	g_Center[1] = 0.0f;
	g_Center[2] = 0.0f;	// 카메라의 초기 초점

	glMatrixMode(GL_MODELVIEW); // Modelview 행렬 스택 선택
	glLoadIdentity();			// 현재 변환 행렬을 단위 행렬로 대치

	// viewing 변환 정의
	gluLookAt(g_Viewer[0], g_Viewer[1], g_Viewer[2], 
				g_Center[0], g_Center[1], g_Center[2], 
				1.0, 0.0, 0.0);

*/	
	glPushMatrix();	// 행렬 스택에 현재의 행렬을 push
	glColor3fv(GRAY);
	DrawGround();	  // DrawGround 함수 호출

	// World coordinate를 그리린다.
	glColor3fv(RED);
	DrawLine(0.0,0.0,0.0,GROUND_LIMITF,0.0,0.0); // x 축 
	glColor3fv(GREEN);
	DrawLine(0.0,0.0,0.0,0.0,GROUND_LIMITF,0.0); // Y 축 
	glColor3fv(BLUE);
	DrawLine(0.0,0.0,0.0,0.0,0.0,GROUND_LIMITF); // Z 축 
 

	//?? Assignment 5
	//DrawCrossRoad(0.0, 0.0, 0.0);	

	//?? Practice 5
	//DrawStreetMidleBlock(0.0, 0.0, 0.0);
	//DrawStreetMidleBlock(0.0, 0.0, 0.0);

	// 사용예 
	//DrawPlane(float xScale, float zScale, float yRotate, float xPos, float zPos)
 	glColor3fv(RED);
 	DrawPlane(16.0f*Unit, 2.0f*Unit, 0.0f, 0.0f, -2.0f*Unit);

	// PlaceObject(float scaleX, float scaleY, float scaleZ, float rotateY, float posX, float posY, float posZ)
	glColor3fv(BLUE);
	PlaceObject(10.0f*Unit, 10.0f*Unit, 10.0f*Unit, 0.0f, 20, 20, 20);
	glPopMatrix();	// 현재 행렬을 그 행렬 스택에서 밖으로 꺼낸다
}

