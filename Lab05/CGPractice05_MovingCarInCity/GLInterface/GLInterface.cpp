#include "GLInterface.h"


CGLInterface::CGLInterface()
{
	InitializeDrawing(); // initialize all the variables necessary for this program.
}

CGLInterface::~CGLInterface()
{

}


void CGLInterface::Initialize()
{
	mCityModel.DefinePrimitiveListforScene(); // define and register primitives for scene modeling
}

void CGLInterface::SetProjectView(int cx, int cy)
{
	// cx와 cy는 클라이언트 영역의 x축, y축 크기

	// TODO: Add your message handler code here
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	

	// 65.0 fovy
	// ratio x to y
	// znear 
	// zfar
	gluPerspective(65.0, (GLfloat)cx / (GLsizei)cy, 1.0, 500.0);

	glViewport(0, 0, cx, cy);
	glMatrixMode(GL_MODELVIEW);
}

void CGLInterface::Display()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);	// buffer clear color 설정
	glClear(GL_COLOR_BUFFER_BIT);	// 설정된 color로 버퍼를 clear
	DrawScene();
	glFlush();
}

void CGLInterface::InitializeDrawing()  // 그리기에 필효한 변수를 초기화 한다. 
{
	g_Viewer[0] = 50.0f;
	g_Viewer[1] = 80.0f;
	g_Viewer[2] = 130.0f;

	g_Center[0] = 0.0f;
	g_Center[1] = 0.0f;
	g_Center[2] = 0.0f;	// 카메라의 초기 초점
}

void CGLInterface::OnKeyDown(unsigned char keyValue)
{
	// TODO: Add your message handler code here and/or call default
	switch (keyValue)
	{
	case 's':
	case 'S':
		break;
	case 'e':
	case 'E':
		break;
	case GLIF_RIGHT:
		break;
	case GLIF_LEFT:
		break;
	}
}


void CGLInterface::DrawScene()
{
	glEnable(GL_DEPTH_TEST);	// Enable hidden surface removal
	glMatrixMode(GL_MODELVIEW); // Modelview 행렬 스택 선택
	glLoadIdentity();			// 현재 변환 행렬을 단위 행렬로 대치

	g_Viewer[0] = 20.0f;
	g_Viewer[1] = 300.0f;
	g_Viewer[2] = 20.0f;

	// viewing 변환 정의

	gluLookAt(g_Viewer[0], g_Viewer[1], g_Viewer[2],
		g_Center[0], g_Center[1], g_Center[2],
		1.0, 0.0, 0.0);



	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // RGB color display

	// cityModeling
	
	mCityModel.RenderScene();	// RenderScene 함수 호출
}

