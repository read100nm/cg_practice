#ifndef GL_INTERFACE_H
#define GL_INTERFACE_H

#include "CityModel.h"

#ifdef WIN32
#include <Windows.h>
#endif

#ifndef _APPLE_OSX
#include <gl/GL.h>
#include <gl/GLU.h>
#else
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#endif

#define GLIF_RIGHT          0x27
#define GLIF_LEFT           0x25

#define GROUND_LIMIT 10000
#define GROUND_LIMITF 10000.0f

class CGLInterface
{
public:
	/// Constructor
	CGLInterface();

	/// Destructor
	virtual ~CGLInterface();

	/// Initialization
	void Initialize();

	/// set projection
	void SetProjectView(int cx, int cy);

	/// draw display by OpenGL functions
	void Display();


	

	/// Key Event handler
	void OnKeyDown(unsigned char keyValue); 

	/// variable initialization. 그리기에 필요한 변수를 초기화 한다. 
	void InitializeDrawing();				

	/// Set the camera WRT WC and display the scene(카메라를 설정하고 scene 전체를 그린다) 
	void DrawScene();	

protected:
	GLdouble g_Viewer[3];	// Camera position
	GLdouble g_Center[3];	// the point where the camera is looking at

	CityModel mCityModel;
};

#endif
