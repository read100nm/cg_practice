//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MFCFrame.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_MAINFRAME                   128
#define IDR_PRACTICE3TYPE               129
#define ID_PRIMITIVES_LINE              32771
#define ID_PRIMITIVES_QUADRILATERAL     32772
#define ID_COLOR_RED                    32773
#define ID_COLOR_GREEN                  32774
#define ID_COLOR_BLUE                   32775
#define ID_COLOR_YELLOW                 32776
#define ID_ATTRIBUTES_FILL              32777
#define ID_FILL_SOLIDLINE               32778
#define ID_FILL_FILL                    32779
#define ID_ATTRIBUTES_LINEWIDTH         32780
#define ID_LINEWIDTH_1                  32781
#define ID_LINEWIDTH_2                  32782
#define ID_LINEWIDTH_3                  32783
#define ID_LINEWIDTH_4                  32784

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32785
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
