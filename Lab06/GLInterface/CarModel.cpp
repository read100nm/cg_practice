// CarModel.cpp: implementation of the CCarModel class.
//
//////////////////////////////////////////////////////////////////////
#include "CarModel.h"

	
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////



/*====================================== CScene Class  ============================================
	- Model a car and display it at the predefined position and orientation
	
==========================================================================================================*/

/*==================================================================================
Function: constructor
====================================================================================*/
CarModel::CarModel()
{
	curTireAngle=0.0;	// 현재 바퀴의 회전 위치
	curRotSpeed=5.0;

	// 컬러 설정
	BLUE[0] = 0; BLUE[1] = 0; BLUE[2] = 1;				// Blue
	YELLOW[0] = 1.0; YELLOW[1] = 1.0; YELLOW[2] = 0;	// YELLOW
	GREEN[0] = 0; GREEN[1] = 1.0; GREEN[2] = 0;			// YELLOW
	RED[0] = 1.0; RED[1] = 0; RED[2] = 0;				// RED
	WHITE[0] = 1.0; WHITE[1] = 1.0; WHITE[2] = 1.0;		// WHITE
	GRAY[0] = 0.5; GRAY[1] = 0.5; GRAY[2] = 0.5;		// GRAY
}


/*==================================================================================
Function: destructor
====================================================================================*/
CarModel::~CarModel()
{
}

/*==================================================================================
Function: Initialize the parameters
====================================================================================*/
void CarModel::SetMovementParameters()
{
	curTireAngle=0.0;
	curRotSpeed=0.01;   

}


/*==================================================================================
Function: draw a wheel using the followings:
	position (posX,posY,posZ),
	wheel direction (steeringRotation)
	wheel rotation speed (speedRotation)

====================================================================================*/
void CarModel::DrawWheel(float steeringRotation, float speedRotation,
							   float posX, float posY, float posZ)
{
	GLfloat scaleX=1.0, scaleY=1.0, scaleZ=0.5;
	glPushMatrix();	// 행렬 스택에 현재의 행렬을 push
	glTranslatef(posX,posY,posZ); // 해당 위치로 이동 
    glRotatef(steeringRotation,0.0,1.0,0.0); // Y축에 대한 회전
	glRotatef(speedRotation,0.0,0.0,1.0); // z축에 관한 회전. tire를 rotate 시킴.
	glScalef(scaleX,scaleY,scaleZ);
	glCallList(CUBLIST);
	glPopMatrix();	// 현재 행렬을 그 행렬 스택에서 밖으로 꺼낸다
}


void CarModel::DefinePrimitiveListforCar()
{
	GLfloat unitSquare[][3]	= {{0.5, 0.0, 0.5}, {0.5, 0, -0.5}
						,{-0.5, 0.0, -0.5}, {-0.5, 0.0, 0.5}};

/*		     Y
		7----|--------6
       /     |       /|
      /      |      / | 
      4------|-----5| | 
      |  2   /------|-/1------->x
      |     /       |/
	  |3---/--------0					
	      /z					*/
	GLfloat unitCube[][3]	= {{0.5, -0.5, 0.5}, {0.5, -0.5, -0.5} 
						,{-0.5, -0.5, -0.5}, {-0.5, -0.5, 0.5}
						,{-0.5, 0.5, 0.5}, {0.5, 0.5, 0.5}
						,{0.5, 0.5, -0.5}, {-0.5, 0.5, -0.5}};
	GLfloat surfaceNorm[][3] = 	{
						{0.0, 0.0, 1.0}		 // 앞면		
						,{1.0, 0.0, 0.0}	 //오른쪽 면	
						,{0.0, 0.0, -1.0}	 // 뒷면
						,{-1.0, 0.0, 0.0}	 // 왼쪽면
						,{0.0, 1.0, 0.0}	  // 윗면		
						,{0.0, -1.0, 0.0}	 // 밑면	
						};
	//Register a unit Square as a list. The cube is defined on xz plane with the center as (0,0,0)
	QUADLIST = glGenLists(1);
	glNewList(QUADLIST, GL_COMPILE);
		glNormal3fv(surfaceNorm[4]);
		DisplayQuad(unitSquare,0,1,2,3); 
	glEndList();

	CUBLIST = glGenLists(2);
	glNewList(CUBLIST, GL_COMPILE);
		glNormal3fv(surfaceNorm[0]);
		DisplayQuad(unitCube,0,5,4,3); // 앞면
		glNormal3fv(surfaceNorm[1]);
		DisplayQuad(unitCube,0,1,6,5); //오른쪽 면
		glNormal3fv(surfaceNorm[2]);
		DisplayQuad(unitCube,1,2,7,6); // 뒷면
		glNormal3fv(surfaceNorm[3]);
		DisplayQuad(unitCube,4,7,2,3); // 왼쪽면
		glNormal3fv(surfaceNorm[4]);
		DisplayQuad(unitCube,4,5,6,7);  // 윗면
		glNormal3fv(surfaceNorm[5]);
		DisplayQuad(unitCube,0,1,2,3); // 밑면
	glEndList();	

}

/*==================================================================================
Function: Display the quad deinfed by the "RegQuad()" at (xPos,zPos) 
		  after scaling it by using the paramter (xScale, zScale) 
			and rotating it with respect to y axis by yRoate
====================================================================================*/

void CarModel::DrawPlane(float xScale, float zScale, float yRotate, float xPos, float zPos)
	{
 	glPushMatrix();	// 행렬 스택에 현재의 행렬을 push
		glTranslatef(xPos,0.0,zPos); // 해당 위치로 이동 
		glRotatef(yRotate,0.0,1.0,0.0); // Y축에 대한 회전
		glScalef(xScale,1.0,zScale);
		glCallList(QUADLIST);

	glPopMatrix();	// 현재 행렬을 그 행렬 스택에서 밖으로 꺼낸다
	}



/*==================================================================================
Function: Place a cube defined by the function "RegCube()" at the position (posX,posY,posY) 
		  after scaling the cube by using the parameter (scaleX, scaleY, scaleZ)
	====================================================================================*/
void CarModel::PlaceObject(float scaleX, float scaleY, float scaleZ,
								 float rotateY, float posX, float posY, float posZ)
{
	glPushMatrix();	// 행렬 스택에 현재의 행렬을 push
	glTranslatef(posX,posY,posZ); // 해당 위치로 이동 
    glRotatef(rotateY,0.0,1.0,0.0); // Y축에 대한 회전
	glScalef(scaleX,scaleY,scaleZ);
	glCallList(CUBLIST);
	glPopMatrix();	// 현재 행렬을 그 행렬 스택에서 밖으로 꺼낸다
}

/*=================================================================================================
 Function: Dispaly a quad
 Pre: array cube should be defined.
=================================================================================================*/
void CarModel::DisplayQuad(GLfloat cube[][3], int a, int b, int c, int d)
{ 

	glBegin(GL_POLYGON); 
 		glVertex3fv(cube[a]); // 0
		glVertex3fv(cube[b]); // 1
		glVertex3fv(cube[c]); // 2
		glVertex3fv(cube[d]); // 3
	glEnd();
}


/*==================================================================================
Function: Display a car
Parameters:
	handleTurn: steering angle defined by steering wheel
	wheelTurn : amount of turn of the wheel.
====================================================================================*/
void CarModel::Display(float handleTurn, float wheelTrun)
{

	if (handleTurn>0) handleTurn = 40.0f;
	else if (handleTurn<0) handleTurn = -40.0f;
	else				 handleTurn = 0.0f;
	glPushMatrix();	// 행렬 스택에 현재의 행렬을 push
	
	//?? 자동차 몸체 A (아래부분) :
	//?? bottom body of car
	glColor3fv(YELLOW);


	//?? 자동차 몸체 B (윗부분) : 
	//?? top body of car
	glColor3fv(GREEN); // 차동차 의 윗부분 


	//?? 앞 바퀴 
	//?? front tire
	curTireAngle += wheelTrun;
	if (curTireAngle > 360.0) curTireAngle = 0.0;
	glColor3fv(GREEN);
	

  	//?? 뒷 바퀴 
	//?? rear tire
	glColor3fv(BLUE);
	
	
  
	glPopMatrix();	// 현재 행렬을 그 행렬 스택에서 밖으로 꺼낸다

 }

						
