// example3Doc.cpp : implementation of the CExample3Doc class
//

#include "stdafx.h"
#include "MFCFrame.h"

#include "MFCFrameDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CExample3Doc

IMPLEMENT_DYNCREATE(MFCFrameDoc, CDocument)

BEGIN_MESSAGE_MAP(MFCFrameDoc, CDocument)
END_MESSAGE_MAP()


// CExample3Doc construction/destruction

MFCFrameDoc::MFCFrameDoc()
{
	// TODO: add one-time construction code here

}

MFCFrameDoc::~MFCFrameDoc()
{
}

BOOL MFCFrameDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CExample3Doc serialization

void MFCFrameDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CExample3Doc diagnostics

#ifdef _DEBUG
void MFCFrameDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void MFCFrameDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CExample3Doc commands
