#include "GLInterface.h"
#include <math.h>


CGLInterface::CGLInterface()
{
	
}

CGLInterface::~CGLInterface()
{

}


void CGLInterface::Initialize()
{
	InitializeDrawing();
}

void CGLInterface::SetProjectView(int cx, int cy)
{
	// cx와 cy는 클라이언트 영역의 x축, y축 크기

	// TODO: Add your message handler code here
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	

	// 65.0 fovy
	// ratio x to y
	// znear 
	// zfar
	gluPerspective(65.0, (GLfloat)cx / (GLsizei)cy, 1.0, 500.0);

	glViewport(0, 0, cx, cy);
	glMatrixMode(GL_MODELVIEW);
}

void CGLInterface::Display()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);	// buffer clear color 설정
	glClear(GL_COLOR_BUFFER_BIT);	// 설정된 color로 버퍼를 clear
	DrawScene();
	glFlush();
}

void CGLInterface::InitializeDrawing()  // 그리기에 필효한 변수를 초기화 한다. 
{
	//quadric object 생성
	p = gluNewQuadric();

	//set attributes of the quadric object. 속성 설정(GLU_POINT, GLU_LINE, GLU_FILL, GLU_SILHOUETTE)
	gluQuadricDrawStyle(p, GLU_LINE);

	// delta
	m_fTheta = 0;

	//첫번째 키 프레임 부터.
	m_pitcherStance = keyStances[0];

	m_bAnimating = false;	// robot in stop position.
	m_nKeyFrameIdx = 0;     // current key frame index
	m_nSubFrameIdx = 1;		// next key frame. Generate intermediate frames(IF) between current and next key frame
	m_deltas;				// initialize variations between IFs
	m_bHoldingBall = true;  //???
	m_bRelease = false;
	m_bReset = false;

	m_deltas = keyStances[7];
}

void CGLInterface::OnKeyDown(unsigned char keyValue,int &timerSet)
{
	// TODO: Add your message handler code here and/or call default
	timerSet = 0; // do not change timer
	switch (keyValue)
	{
		// Start animation and set timer. 키가 눌렸을 때 애니메이션 시작
	case 's':
	case 'S':
		//??
		break;
		// End the animation and kill timer.  키가 눌렸을 때 애니메이션 종료
	case 'e':
	case 'E':
		//??
		break;
		// > right cursor: rotate robot 5 degree right. 오른쪽 화살표키가 눌렸을때 5도 만큼 회전 
		// 360도가 넘으면 처음부터 시작 
	case GLIF_RIGHT:
		//??
		break;
		//< left cursor: rotate robot -5 degree. 왼쪽 화살표키가 눌렸을때 -5도 만큼 회전
		// 360도가 넘으면 처음부터 시작 
	case GLIF_LEFT:
		if (m_fTheta>0)
			m_fTheta -= 5;
		else
			m_fTheta = 355;
		break;
	}

	// 0,1,2, ... 6: display the key frame represented by the key. 입력된 넘버에 해당되는 키프레임 출력 
	if (keyValue >= 49 && keyValue <= 55)
		m_pitcherStance = keyStances[keyValue - 49];
}

// --------------------------------- --------------------------------------------------------
//  - set the carmera in WC 
//  - draw scene and robot.
// --------------------------------- --------------------------------------------------------
void CGLInterface::DrawScene()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(50, 50, -30, 0, 0, 0, 0, 1, 0);

	glRotatef(m_fTheta, 0, 1, 0);

	DrawPitcher();


	// double buffering
}

/* -------------------------------------------------------------------------------------------------------------
Function: Draw a body.
- Bend the upper body WRT the waist. The location of the waist in the body coordinate is (0, -4, 0)
- Face the front of the body.
- Draw the parts of the robot WRT the body.
-------------------------------------------------------------------------------------------------------------*/

void CGLInterface::Torso()
{
	glPushMatrix();
	// Define orientation of body in WC. WC에 대한 몸통의 방향을 결정한다. 
	glRotatef(m_pitcherStance.body_x, 1.0, 0.0, 0.0);
	glRotatef(m_pitcherStance.body_y, 0.0, 1.0, 0.0);
	glRotatef(m_pitcherStance.body_z, 0.0, 0.0, 1.0);

	// Rotate the body WRT the waist. 
	// The posision of the waist in the body coordinate is (0.0, -4.0, 0.0)
	// 허리를 중심으로 몸통의 굽혀진 정도를 결정한다.
	// 여기서 허리의 위치는 몸통 좌표계상에서(0.0, -4.0, 0.0)에 있다.

	//?? 그려질 로봇의 몸통의 위치를 위와 같이 정하시오.
	//?? set the postiion of a robot torso as shown above.

	glPushMatrix();
	//크기 (6.0, 8.0, 2.3) 의 cube를 그린다.
	glScalef(6.0, 8.0, 2.3);
	glColor3f(1.0, 0.0, 0.0);
	glutWireCube(1.0);
	glPopMatrix();

	// draw each part of the robot. 각 신체 부위
	//?? 로봇의 각 부분을 그리시오. 
	//?? draw each part of the robot.

	glPopMatrix();
}

/* -------------------------------------------------------------------------------------------------------------
Function: Draw the head.
- Let the head face the direction defined by m_pithcherStand.neck
- The location of the head coordinate in the Body coordinate is (0. 7. 0)
- The location of the neck in the head coordinate is (0.0, -3, 0)
- The location of the left eye in the head coordinate is (-1.0, 1.0, -2.5)
- The location of the right eye in the head coordinate is (1.0, 1.0, -2.5)
-------------------------------------------------------------------------------------------------------------*/
void CGLInterface::Head()
{
	glPushMatrix();
	//머리좌표계의 중심은 body coordinate 상의 (0, 7, 0)에 위치 
	//??

	// rotate head
	//??


	// draw head
	glColor3f(1.0, 0.5, 0.5);
	glutWireSphere(3.0, 10, 10);

	// left eye. 왼쪽 눈
	glPushMatrix();
	glTranslatef(-1.0, 1.0, -2.5);
	glColor3f(0.0, 0.0, 1.0);
	glutWireSphere(0.5, 10, 10);
	glPopMatrix();

	// right eye. 오른쪽 눈
	glPushMatrix();
	glTranslatef(1.0, 1.0, -2.5);
	glColor3f(0.0, 0.0, 1.0);
	glutWireSphere(0.5, 10, 10);
	glPopMatrix();
	glPopMatrix();
}


/* -------------------------------------------------------------------------------------------------------------
Function: // draw left upper arm.
- The location of the left arm in the body coordinate is (-4.0, 2.0, 0.0)
- The location of the left upper arm joint in the upper arm joint coordinate is (1.0 2.0 0.0)
- Draw the left lower arm WRT the left upper arm.
-------------------------------------------------------------------------------------------------------------*/
void CGLInterface::LeftUpperArm()
{
	glPushMatrix();
	// position the left upper arm in the body coordinate
	//??

	// rotate left shoulder. 
	// The location of the left upper arm joint in the upper arm joint coordinate is (1.0 2.0 0.0)
	//??

	// Draw the upper arm by scaling a cube by (2, 4, 1.75). Use glutWireCube(). 
	glPushMatrix();
	glScalef(2.0, 4.0, 1.75);
	glColor3f(0.6, 0.6, 0.6);
	glutWireCube(1.0);
	glPopMatrix();

	// Draw left lower arm 
	LeftLowerArm();
	glPopMatrix();
}


/* -------------------------------------------------------------------------------------------------------------
Function:  draw left lowr arm.
- The location of the left lower arm in the left upper arm coordinate is (0.0, -4.5, 0.0)
- The location of the left elbow in the left upper arm coordinate is (0.0 2.5 0.0)
-------------------------------------------------------------------------------------------------------------*/
void CGLInterface::LeftLowerArm()
{
	// position the lower left arm in the left upper arm coorindate. 
	//The location of the left lower arm in the left upper arm coordinate is (0.0, -4.5, 0.0)
	//??

	// rotate left elbow. the elbow location in the left upper arm coordinate is (0.0, 2.5, 0.0)
	//??

	// draw left lower arm by scaling a cube by (2, 5, 1.75). Use glutWireCube().
	glPushMatrix();
	glScalef(2.0, 5.0, 1.75);
	glColor3f(0.7, 0.7, 0.7);
	glutWireCube(1.0);
	glPopMatrix();
}

/* -------------------------------------------------------------------------------------------------------------
Function: // draw right upper arm.
- The location of the right arm in the body coordinate is (4.0, 2.0, 0.0)
- The location of the riht upper arm joint in the upper arm joint coordinate is (-1.0 2.0 0.0)
- Draw the right lower arm WRT the right upper arm.
-------------------------------------------------------------------------------------------------------------*/
void CGLInterface::RightUpperArm()
{
	glPushMatrix();
	// position the right upper arm in the body coordinate
	glTranslatef(4.0, 2.0, 0.0);

	// rotate right shoulder
	glTranslatef(-1.0, 2.0, 0.0);
	glRotatef(m_pitcherStance.armR_x, 1.0, 0.0, 0.0);
	glRotatef(m_pitcherStance.armR_y, 0.0, 1.0, 0.0);
	glRotatef(m_pitcherStance.armR_z, 0.0, 0.0, 1.0);
	glTranslatef(1.0, -2.0, 0.0);

	// Draw the upper arm by scaling a cube by (2, 4, 1.75). Use glutWireCube(). 
	glPushMatrix();
	glScalef(2.0, 4.0, 1.75);
	glColor3f(0.6, 0.6, 0.6);
	glutWireCube(1.0);
	glPopMatrix();

	// draw right lower arm
	RightLowerArm();
	glPopMatrix();
}

/* -------------------------------------------------------------------------------------------------------------
Function:  draw right lower arm.
- The location of the right lower arm in the right upper arm coordinate is (0.0, -4.5, 0.0)
- The location of the right elbow in the right upper arm coordinate is (0.0 2.5 0.0)
-------------------------------------------------------------------------------------------------------------*/
void CGLInterface::RightLowerArm()
{
	// position the lower right arm in the right upper arm coorindate. 
	glTranslatef(0.0, -4.5, 0.0);
	// rotate right elbow
	glTranslatef(0.0, 2.5, 0.0);
	glRotatef(m_pitcherStance.elbowR_x, 1.0, 0.0, 0.0);
	glRotatef(m_pitcherStance.elbowR_y, 0.0, 1.0, 0.0);
	glRotatef(m_pitcherStance.elbowR_z, 0.0, 0.0, 1.0);
	glTranslatef(0.0, -2.5, 0.0);

	// Draw the lower arm by scaling a cube by (2, 5, 1.75). Use glutWireCube().
	glPushMatrix();
	glScalef(2.0, 5.0, 1.75);
	glColor3f(0.7, 0.7, 0.7);
	glutWireCube(1.0);
	glPopMatrix();
}


/* -------------------------------------------------------------------------------------------------------------
Function:  draw left upper leg(LUL).
- The location of LUL in the body coordinate is (-2.0, -7.0, 0.0)
- The location of the upper joint of LUL in LUL coordinate is 0.0, 3.0, 0.0)
- Draw the left lower leg.
-------------------------------------------------------------------------------------------------------------*/
void CGLInterface::LeftUpperLeg()
{
	glPushMatrix();
	// potision the LUL in the body coordinate. The location is (-2.0, -7.0, 0.0)
	//??

	// rotate the LUL WRT the body. the location of the joint is (0.0, 3.0, 0.0)
	//??

	// draw LUL by scaling a cube by (2, 6, 2). Use glutWireCube()
	glPushMatrix();
	glScalef(2.0, 6.0, 2.0);
	glColor3f(0.6, 0.6, 0.6);
	glutWireCube(1.0);
	glPopMatrix();

	// draw left lower leg
	LeftLowerLeg();
	glPopMatrix();
}



/* -------------------------------------------------------------------------------------------------------------
Function:  draw left lower leg(LLL).
- The location of LLL in the LUL coordinate is (0.0, -5.5, 0.0)
- The location of the upper joint of LLL in LLL coordinate is (0.0, 2.5, 0.0)
- Draw the left lower leg.
- Draw a left foot(LF) and ankle (발과 발목)
- location of the foot in the LLL coordinate is (0.0, -3.0, 0.0)
- location of the foot joint (ankle) in the LF coordinate is (0.0, 0.5, 0.0)
-------------------------------------------------------------------------------------------------------------*/
void CGLInterface::LeftLowerLeg()
{
	// position the LLL in the LUL coordinate. the position is (0.0, -5.5, 0.0)
	glTranslatef(0.0, -5.5, 0.0);

	// rotate left knee. the joint is located at (0.0, 2.5, 0.0) in the LLL coordinate 
	//??

	//draw left lower leg by scaling a cube by (2, 5, 2). Use glutWireCube()
	glPushMatrix();
	glScalef(2.0, 5.0, 2.0);
	glColor3f(1.0, 0.0, 0.0);
	glutWireCube(1.0);
	glPopMatrix();

	// position the LF in the LLL coordinate
	glTranslatef(0.0, -3.0, 0.0);

	// rotate left ankle. The ankle position in LF coordinate is (0.0, 0.5, 0.0)
	//??

	// draw a LF scaling a cube by (2, 1, 4). Use glutWireCube()
	glPushMatrix();
	glTranslatef(0.0, 0.0, -1.0);
	glScalef(2.0, 1.0, 4.0);
	glColor3f(0.2, 0.0, 0.0);
	glutWireCube(1.0);
	glPopMatrix();
}

/* -------------------------------------------------------------------------------------------------------------
Function:  draw right upper leg(RUL).
- The location of RUL in the body coordinate is (2.0, -7.0, 0.0)
- The location of the upper joint of RUL in RUL coordinate is 0.0, 3.0, 0.0)
- Draw the right lower leg.
-------------------------------------------------------------------------------------------------------------*/
void CGLInterface::RightUpperLeg()
{
	glPushMatrix();
	// potision the RUL in the body coordinate. The location is (2.0, -7.0, 0.0)
	glTranslatef(2.0, -7.0, 0.0);

	// rotate the RUL WRT the body. the location of the joint is (0.0, 3.0, 0.0)
	glTranslatef(0.0, 3.0, 0.0);
	glRotatef(m_pitcherStance.legR_x, 1.0, 0.0, 0.0);
	glRotatef(m_pitcherStance.legR_y, 0.0, 1.0, 0.0);
	glRotatef(m_pitcherStance.legR_z, 0.0, 0.0, 1.0);
	glTranslatef(0.0, -3.0, 0.0);

	// draw RUL by scaling a cube by (2, 6, 2). Use glutWireCube()
	glPushMatrix();
	glScalef(2.0, 6.0, 2.0);
	glColor3f(0.6, 0.6, 0.6);
	glutWireCube(1.0);
	glPopMatrix();

	RightLowerLeg();
	glPopMatrix();
}

/* -------------------------------------------------------------------------------------------------------------
Function:  draw right lower leg(RLL).
- The location of RLL in the RUL coordinate is (0.0, -5.5, 0.0)
- The location of the upper joint of RLL in RLL coordinate is (0.0, 2.5, 0.0)
- Draw the right lower leg.
- Draw a right foot(RF) and ankle (발과 발목)
- location of the foot in the RLL coordinate is (0.0, -3.0, 0.0)
- location of the foot joint (ankle) in the RF coordinate is (0.0, 0.5, 0.0)
-------------------------------------------------------------------------------------------------------------*/
void CGLInterface::RightLowerLeg()
{
	// position the RLL in the RUL coordinate. the position is (0.0, -5.5, 0.0)
	glTranslatef(0.0, -5.5, 0.0);

	// rotate right knee. the joint is located at (0.0, 2.5, 0.0) in the LLL coordinate 
	glTranslatef(0.0, 2.5, 0.0);
	glRotatef(m_pitcherStance.kneeR_x, 1.0, 0.0, 0.0);
	glRotatef(m_pitcherStance.kneeR_y, 0.0, 1.0, 0.0);
	glRotatef(m_pitcherStance.kneeR_z, 0.0, 0.0, 1.0);
	glTranslatef(0.0, -2.5, 0.0);

	//draw right lower leg by scaling a cube by (2, 5, 2). Use glutWireCube()
	glPushMatrix();
	glScalef(2.0, 5.0, 2.0);
	glColor3f(1.0, 0.0, 0.0);
	glutWireCube(1.0);
	glPopMatrix();

	// position the RF in the RLL coordinate
	glTranslatef(0.0, -3.0, 0.0);
	// rotate right ankle. The ankle position in RF coordinate is (0.0, 0.5, 0.0)
	glTranslatef(0.0, 0.5, 0.0);
	glRotatef(m_pitcherStance.ankleR_x, 1.0, 0.0, 0.0);
	glRotatef(m_pitcherStance.ankleR_y, 0.0, 1.0, 0.0);
	glRotatef(m_pitcherStance.ankleR_z, 0.0, 0.0, 1.0);
	glTranslatef(0.0, -0.5, 0.0);

	// draw a RF scaling a cube by (2, 1, 4). Use glutWireCube()
	glPushMatrix();
	glTranslatef(0.0, 0.0, -1.0);
	glScalef(2.0, 1.0, 4.0);
	glColor3f(0.2, 0.0, 0.0);
	glutWireCube(1.0);
	glPopMatrix();
}

/* -------------------------------------------------------------------------------------------------------------
Function:  Compute the variation between two intermediate frames
- stance 1 : current key frame
- stance 2 : next key frame
- n : the number of intermediate frames between those two key frames
- m_deltas : the structure variables holding the variance.
-------------------------------------------------------------------------------------------------------------*/
void CGLInterface::CalcDeltas(const Stance *stance1, const Stance *stance2, int n)
{
	// Divide the difference between two key frames by the number of intermediate frames.
	//?? 중간 프레임 간 변화량을 계산하라. 

}

/* -------------------------------------------------------------------------------------------------------------
Function:  update the current stance when the timer is on.
-------------------------------------------------------------------------------------------------------------*/
void CGLInterface::OnTimer()
{
	
}

void CGLInterface::DrawPitcher()
{
	glPushMatrix();
	Torso();
	glPopMatrix();

}

