#include "CGGLWidget.h"

#include <QMouseEvent>
#include <qpushbutton.h>
#include <QHBoxLayout>
#include <qtimer.h>

#include "QtMainWindow.h"

#include <iostream>


CGGLWidget::CGGLWidget(QWidget *parent) : QOpenGLWidget(parent)
{
	setMouseTracking(true);

	QtMainWindow* mainwindow = dynamic_cast<QtMainWindow*>(parent);

	setFocus();

	timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(timeout()));
}

CGGLWidget::~CGGLWidget(void)
{

}

void CGGLWidget::initializeGL()
{
	m_iGL.Initialize();
}

void CGGLWidget::paintGL()
{
	m_iGL.Display();
}

void CGGLWidget::resizeGL(int w, int h)
{
	m_iGL.SetProjectView(w, h);
}

void CGGLWidget::keyPressEvent(QKeyEvent *ev)
{
	int timerSet = 0;


	m_iGL.OnKeyDown(ev->nativeVirtualKey(), timerSet);

	if (timerSet>0)
	{
		if (timerSet == 1) // set the timer
			timer->start(DELAY);
		else if (timerSet == 2) // kill the timer
			timer->stop();
		else timerSet = 0;
	}


	update();
}

void CGGLWidget::timeout()
{
	m_iGL.OnTimer();
	update();

	std::cout << "test timer" << std::endl;
}