#ifndef GL_INTERFACE_H
#define GL_INTERFACE_H

#ifdef WIN32
#include <Windows.h>
#endif

#ifndef _APPLE_OSX
#include <gl/GL.h>
#include <gl/GLU.h>
#include <gl/glut.h>
#else
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#endif


#define GLIF_RIGHT          0x27
#define GLIF_LEFT           0x25

// Local Variable Start: SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSs


// Robot parameters
typedef struct {
	GLfloat body_x, body_y, body_z;		// Facing direction of body
	GLfloat neck_x, neck_y, neck_z;		// Facing direction of neck
	GLfloat armL_x, armL_y, armL_z;		// Orientation of left arm with respect to (WRT) body
	GLfloat armR_x, armR_y, armR_z;		// orientation of right arm WRT body
	GLfloat elbowL_x, elbowL_y, elbowL_z;	// orientation of lower left arm WRT upper left arm
	GLfloat elbowR_x, elbowR_y, elbowR_z;	// orientation of lower right arm WRT upper right arm
	GLfloat waist_x, waist_y, waist_z;	// orientation of waist
	GLfloat legL_x, legL_y, legL_z;		// orientation of left leg WRT body
	GLfloat legR_x, legR_y, legR_z;		// orientation of right leg WRT body
	GLfloat kneeL_x, kneeL_y, kneeL_z;	// orientation of lower left leg WRP upper left leg
	GLfloat kneeR_x, kneeR_y, kneeR_z;	// orientation of lower right leg WRP upper Right leg
	GLfloat ankleL_x, ankleL_y, ankleL_z;	// orientation of left ankle(발목)
	GLfloat ankleR_x, ankleR_y, ankleR_z;	// orientation of right ankle
} Stance;


// variables necessary for animation
const int FRAMES_PER_INTV = 30;				// number of frames between two key frames(두 키 프레임 사이의 프레임 수)
const float PI = 3.14159265;
const int DELAY = 350 / FRAMES_PER_INTV;	//프레임 간격, 각 키 프레임 마다 350ms 동안 

// Definition of Key frames. array keyStances[] holds information for 7 key frames
// 배열 keyStances[]에 7개의 키프레임 정보가 저장된다. 
const Stance keyStances[8] = // KEY FRAME 1
{ { 0.0, -90.0, 0.0, // body
0.0, 0.0, 0.0, // neck
0.0, 0.0, 0.0, // left shoulder
0.0, 0.0, 0.0, // right shoulder
0.0, 0.0, 0.0, // left elbow
0.0, 0.0, 0.0, // right elbow
0.0, 0.0, 0.0, // waist
0.0, 0.0, 0.0, // left hip
0.0, 0.0, 0.0, // right hip
0.0, 0.0, 0.0, // left knee
0.0, 0.0, 0.0, // right knee
0.0, 0.0, 0.0, // left ankle
0.0, 0.0, 0.0 },// right ankle

// KEY FRAME 2
{ 0.0, -90.0, 0.0, // body
0.0, 0.0, 0.0, // neck
0.0, 0.0, 0.0, // left shoulder
0.0, 0.0, 0.0, // right shoulder
100.0, 0.0, 40.0, // left elbow
100.0, 0.0, -40.0, // right elbow
0.0, 0.0, 0.0, // waist
0.0, 0.0, 0.0, // left hip
0.0, 0.0, 0.0, // right hip
0.0, 0.0, 0.0, // left knee
0.0, 0.0, 0.0, // right knee
0.0, 0.0, 0.0, // left ankle
0.0, 0.0, 0.0 },// right ankle

// KEY FRAME 3
{ 0.0, -90.0, 0.0, // body
0.0, 0.0, 0.0, // neck
0.0, 0.0, 0.0, // left shoulder
0.0, 0.0, 0.0, // right shoulder
100.0, 0.0, 40.0, // left elbow
100.0, 0.0, -40.0, // right elbow
0.0, 0.0, 0.0, // waist
100.0, 0.0, 0.0, // left hip
0.0, 0.0, 0.0, // right hip
-100.0, 0.0, 0.0, // left knee
0.0, 0.0, 0.0, // right knee
-70.0, 0.0, 0.0, // left ankle
0.0, 0.0, 0.0 },// right ankle

// KEY FRAME 4
{ 0.0, -90.0, 0.0, // body
0.0, 90.0, 0.0, // neck
0.0, 0.0, 0.0, // left shoulder
0.0, 0.0, 0.0, // right shoulder
100.0, 0.0, 40.0, // left elbow
100.0, 0.0, -40.0, // right elbow
0.0, 0.0, 0.0, // waist
100.0, 0.0, 0.0, // left hip
0.0, 0.0, 0.0, // right hip
-100.0, 0.0, 0.0, // left knee
0.0, 0.0, 0.0, // right knee
-70.0, 0.0, 0.0, // left ankle
0.0, 0.0, 0.0 },// right ankle

// KEY FRAME 5
{ 0.0, -90.0, 0.0, // body
0.0, 90.0, 0.0, // neck
0.0, 0.0, 0.0, // left shoulder
180.0, 0.0, 90.0, // right shoulder
100.0, 0.0, 40.0, // left elbow
0.0, -45.0, -20.0, // right elbow
0.0, 0.0, 0.0, // waist
100.0, 0.0, 0.0, // left hip
0.0, 0.0, 0.0, // right hip
-100.0, 0.0, 0.0, // left knee
0.0, 0.0, 0.0, // right knee
-70.0, 0.0, 0.0, // left ankle
0.0, 0.0, 0.0 },// right ankle

// KEY FRAME 6
{ 0.0, 0.0, 0.0, // body
0.0, 0.0, 0.0, // neck
0.0, 0.0, 0.0, // left shoulder
160.0, 0.0, 0.0, // right shoulder
80.0, 0.0, 40.0, // left elbow
0.0, 0.0, 0.0, // right elbow
-20.0, 0.0, 0.0, // waist
60.0, 0.0, 0.0, // left hip
0.0, 0.0, 0.0, // right hip
-60.0, 0.0, 0.0, // left knee
0.0, 0.0, 0.0, // right knee
-50.0, 0.0, 0.0, // left ankle
0.0, 0.0, 0.0 },// right ankle

// KEY FRAME 7
{ 0.0, 0.0, 0.0, // body
0.0, 0.0, 0.0, // neck
0.0, 0.0, 0.0, // left shoulder
60.0, 0.0, 0.0, // right shoulder
80.0, 0.0, 40.0, // left elbow
0.0, 0.0, 0.0, // right elbow
-50.0, 0.0, 0.0, // waist
40.0, 0.0, 0.0, // left hip
-15.0, 0.0, 0.0, // right hip
0.0, 0.0, 0.0, // left knee
-50.0, 0.0, 0.0, // right knee
0.0, 0.0, 0.0, // left ankle
0.0, 0.0, 0.0 }, // right ankle
// Initial 
{ 0.0, 0.0, 0.0, // body
0.0, 0.0, 0.0, // neck
0.0, 0.0, 0.0, // left shoulder
0.0, 0.0, 0.0, // right shoulder
0.0, 0.0, 0.0, // left elbow
0.0, 0.0, 0.0, // right elbow
0.0, 0.0, 0.0, // waist
0.0, 0.0, 0.0, // left hip
0.0, 0.0, 0.0, // right hip
0.0, 0.0, 0.0, // left knee
0.0, 0.0, 0.0, // right knee
0.0, 0.0, 0.0, // left ankle
0.0, 0.0, 0.0 } }; // right ankle


// Local Variable End : EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEe

class CGLInterface
{
public:
	/// Constructor
	CGLInterface();

	/// Destructor
	virtual ~CGLInterface();

	/// Initialization
	void Initialize();

	/// set projection
	void SetProjectView(int cx, int cy);

	/// draw display by OpenGL functions
	void Display();


// Function Definition Start: SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSs
public:
	
	/// Set the camera WRT WC and display the scene(카메라를 설정하고 scene 전체를 그린다) 
	void DrawScene(); 

	/// draw the robot pitching a ball. 투구하는 로봇을 그린다. 
	void DrawPitcher();


	/// Generate the intermediate frames. 각 키 프레임의 중간 애니메이션 생성
	void CalcDeltas(const Stance *stance1, const Stance *stance2, int n);

	/// draw robot head, body, arm, and leg.
	void Torso();		// body
	void Head();
	void LeftUpperArm();
	void LeftLowerArm();
	void RightUpperArm();
	void RightLowerArm();
	void LeftUpperLeg();
	void LeftLowerLeg();
	void RightUpperLeg();
	void RightLowerLeg();

	/// Key Event handler
	void OnKeyDown(unsigned char keyValue,int &timerSet); 

	/// variable initialization. 그리기에 필요한 변수를 초기화 한다. 
	void InitializeDrawing();				

	/// timer interrupt event
	void OnTimer();

// Function Definition End: EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEe
protected:

	
	
// Variable Definition Start: SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSs
	GLfloat m_fTheta;

	// Quadraric object needed for cylinder drawing. cylinder를 그리기 위한 quadric object
	GLUquadricObj *p;

	//current key frame. 현재 로봇의 키 프레임
	Stance m_pitcherStance;

	// variables for animation.  움직임을 위함 변수들
	bool m_bAnimating;		//animation flag
	int m_nKeyFrameIdx;		//current key frame index.   현재 키 프레임
	int m_nSubFrameIdx;		//next key frame index

	//??  the following variabls are necessary to add a ball. 
	bool m_bHoldingBall;	// true if a ball is added
	bool m_bRelease;		// true if a ball has been thrown.
	bool m_bReset;			// reset the flags.

	Stance m_deltas;		// variation between two intermediate frames. 두 중간 프레임 사이의 변화량



// Variable Definition End : EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEe
};

#endif
