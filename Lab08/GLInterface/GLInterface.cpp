#include "GLInterface.h"
#include <math.h>


CGLInterface::CGLInterface()
{
	
}

CGLInterface::~CGLInterface()
{

}


void CGLInterface::Initialize()
{
	InitializeDrawing();
}

void CGLInterface::SetProjectView(int cx, int cy)
{
	// cx와 cy는 클라이언트 영역의 x축, y축 크기

	// TODO: Add your message handler code here
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	

	// 65.0 fovy
	// ratio x to y
	// znear 
	// zfar
	gluPerspective(65.0, (GLfloat)cx / (GLsizei)cy, 1.0, 500.0);

	glViewport(0, 0, cx, cy);
	glMatrixMode(GL_MODELVIEW);
}

void CGLInterface::Display()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);	// buffer clear color 설정
	glClear(GL_COLOR_BUFFER_BIT);	// 설정된 color로 버퍼를 clear
	DrawScene();
	glFlush();
}

void CGLInterface::InitializeDrawing()  // 그리기에 필효한 변수를 초기화 한다. 
{
	m_theta[0] = 0;
	m_theta[1] = 0;
	m_theta[2] = 0;
}

void CGLInterface::OnKeyDown(unsigned char keyValue)
{
	switch (keyValue)
	{
	case 's':
	case 'S':
		break;
	case 'e':
	case 'E':
		break;
	case GLIF_RIGHT:
		break;
	case GLIF_LEFT:
		break;
	}
}

void CGLInterface::DrawScene()
{
	// Enable hidden surface removal
	glEnable(GL_DEPTH_TEST);	

	// background color setting
	glClearColor(0.3f, 0.3f, 0.3f, 1.0f);	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// initialize modelview matrix as identity matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//gluLookAt(0, 2.0, 2.0, 0, 0, 0, 0, 1.0, 0);  // for teapot
	gluLookAt(0, 8.0, 6.0, 0, 0, 0, 0, 1.0, 0);

	// WC에 대한 Object coordinate를 설정한다. m_theta[]를 변화시켜 animiation 가능 
	glRotatef(m_theta[2], 0.0f, 0.0f, 1.0f);
	glRotatef(m_theta[1], 0.0f, 1.0f, 0.0f);
	glRotatef(m_theta[0], 1.0f, 0.0f, 0.0f);

	///////////
	//??????


	glutSolidTeapot(3.0);
}


//
//
//void CGLInterface::DrawSphere()
//{
//	///////////////////////////////////////////
//	// Flat-Shaded Sphere
//
//	point3 v[4] = { { 0.0f, 0.0f, 1.0f },
//	{ 0.0f, 0.942809f, -0.333333f },
//	{ -0.816497f, -0.471405f, -0.333333f },
//	{ 0.816497f, -0.471405f, -0.333333f } };
//
//	// level of subdivison
//	int n = 5;
//
//	// a tetrahedron
//	DivideTriangle(v[0], v[1], v[2], n);
//	DivideTriangle(v[0], v[2], v[3], n);
//	DivideTriangle(v[0], v[3], v[1], n);
//	DivideTriangle(v[3], v[2], v[1], n);
//}
//
//
//
//
//
//
//void CGLInterface::DivideTriangle(point3 &a, point3 &b, point3 &c, int n)
//{
//	if( n > 0 )
//	{
//		point3 v1, v2, v3;
//		for(register int j=0; j<3; j++)
//		{
//			v1[j] = a[j] + b[j];
//			v2[j] = b[j] + c[j];
//			v3[j] = c[j] + a[j];
//		}
//		Normalize(v1);
//		Normalize(v2);
//		Normalize(v3);
//
//		DivideTriangle(a, v1, v3, n-1);
//		DivideTriangle(b, v2, v1, n-1);
//		DivideTriangle(c, v3, v2, n-1);
//		DivideTriangle(v1, v2, v3, n-1);
//	}
//	else
//		DrawTriangle(a, b, c);
//}
//
//void CGLInterface::Normalize(point3 &p)
//{
//	double d = p[0]*p[0] + p[1]*p[1] + p[2]*p[2];
//	if( d > 0 )
//	{
//		float len = (float)(1.0 / sqrt(d));
//		p[0] *= len;
//		p[1] *= len;
//		p[2] *= len;
//	}
//}
//
//void CGLInterface::DrawTriangle(point3 &a, point3 &b, point3 &c)
//{
//	point3 n;
//	CrossProduct(a, b, c, n);
//
//	// smooth shading
//	glShadeModel(GL_FLAT);
//
//	glBegin(GL_TRIANGLES);
//	glNormal3fv(a);
//	glVertex3fv(a);
//	glNormal3fv(b);
//	glVertex3fv(b);
//	glNormal3fv(c);
//	glVertex3fv(c);
//	glEnd();
//}
//
//void CGLInterface::CrossProduct(point3 &a, point3 &b, point3 &c, point3 &r)
//{
//	r[0] = (b[1]-a[1])*(c[2]-a[2]) - (b[2]-a[2])*(c[1]-a[1]);
//	r[1] = (b[2]-a[2])*(c[0]-a[0]) - (b[0]-a[0])*(c[2]-a[2]);
//	r[2] = (b[0]-a[0])*(c[1]-a[1]) - (b[1]-a[1])*(c[0]-a[0]);
//	Normalize(r);
//}


