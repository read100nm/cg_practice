#ifndef GL_INTERFACE_H
#define GL_INTERFACE_H

#ifdef WIN32
#include <Windows.h>
#endif

#ifndef _APPLE_OSX
#include <gl/GL.h>
#include <gl/GLU.h>
#include <gl/glut.h>
#else
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#endif


#define GLIF_RIGHT          0x27
#define GLIF_LEFT           0x25


typedef GLfloat point3[3];


class CGLInterface
{
public:
	/// Constructor
	CGLInterface();

	/// Destructor
	virtual ~CGLInterface();

	/// Initialization
	void Initialize();

	/// set projection
	void SetProjectView(int cx, int cy);

	/// draw display by OpenGL functions
	void Display();


// Function Definition Start: SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSs
public:

	/// DrawSphere
	void DrawSphere();

	/// Draw Trinable
	void DrawTriangle(point3 &a, point3 &b, point3 &c);	

	/// compute normal direction
	void CrossProduct(point3 &a, point3 &b, point3 &c, point3 &r); 
	
	/// compute unit vector
	void DivideTriangle(point3 &a, point3 &b, point3 &c, int n);	
	
	/// normalize vector
	void Normalize(point3 &p);

	/// Set the camera WRT WC and display the scene(카메라를 설정하고 scene 전체를 그린다) 
	void DrawScene(); 


	/// Key Event handler
	void OnKeyDown(unsigned char keyValue); 
	/// variable initialization. 그리기에 필요한 변수를 초기화 한다. 
	void InitializeDrawing();				
	/// timer interrupt event
	void OnTimer();

// Function Definition End: EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEe
protected:

	
	
// Variable Definition Start: SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSs
	int  lightSourceSelection;   // 1: point source, 2: specular, 3: 
	float m_theta[3];
// Variable Definition End : EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEe

};

#endif
