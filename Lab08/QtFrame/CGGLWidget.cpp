#include "CGGLWidget.h"

#include <QMouseEvent>
#include <qpushbutton.h>
#include <QHBoxLayout>
#include <qtimer.h>

#include "QtMainWindow.h"

#include <iostream>


CGGLWidget::CGGLWidget(QWidget *parent) : QOpenGLWidget(parent)
{
	setMouseTracking(true);

	QtMainWindow* mainwindow = dynamic_cast<QtMainWindow*>(parent);

	setFocus();
}

CGGLWidget::~CGGLWidget(void)
{

}

void CGGLWidget::initializeGL()
{
	m_iGL.Initialize();
}

void CGGLWidget::paintGL()
{
	m_iGL.Display();
}

void CGGLWidget::resizeGL(int w, int h)
{
	m_iGL.SetProjectView(w, h);
}

void CGGLWidget::keyPressEvent(QKeyEvent *ev)
{
	int timerSet = 0;

	// keyboard interface operations related to the application should be done in OnKeyDown()
	m_iGL.OnKeyDown(ev->nativeVirtualKey());
	update();
}
