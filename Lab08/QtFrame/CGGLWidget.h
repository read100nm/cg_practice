#ifndef OPENGL_PROJECTORSCREEN_VIEWER_H_011
#define OPENGL_PROJECTORSCREEN_VIEWER_H_011

#include <qopenglwidget.h>
#include "GLInterface.h"

class QPushButton;

class CGGLWidget :public QOpenGLWidget
{
	Q_OBJECT

public:
	CGGLWidget(QWidget *parent = 0);
	~CGGLWidget();

protected:
	void initializeGL();
	void resizeGL(int w, int h);
	void paintGL();

	virtual void  keyPressEvent(QKeyEvent *event);

	CGLInterface m_iGL;
};

#endif
