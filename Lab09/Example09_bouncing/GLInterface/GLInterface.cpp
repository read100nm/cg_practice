

#include "GLInterface.h"
#include <math.h>
#include <stdio.h>

CGLInterface::CGLInterface()
{
	
}

CGLInterface::~CGLInterface()
{

}


void CGLInterface::Initialize()
{
	InitializeDrawing();
}

void CGLInterface::SetProjectView(int cx, int cy)
{
	// cx와 cy는 클라이언트 영역의 x축, y축 크기
	glViewport(0, 0, cx, cy);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	float aspect = cx / (float)(cy ? cy : 1);
	glFrustum(-aspect, aspect, -1, 1, 10, 100);
	glTranslatef(-0.5f, -0.5f, -0.5f);
	glTranslatef(0.0f, 0.0f, -15.0f);
}

void CGLInterface::Display()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);	// buffer clear color 설정
	glClear(GL_COLOR_BUFFER_BIT);	// 설정된 color로 버퍼를 clear
	DrawScene();
	glFlush();
}

void CGLInterface::InitializeDrawing()  // 그리기에 필효한 변수를 초기화 한다. 
{
	// TODO: add construction code here
	m_theta[0] = 0;
	m_theta[1] = 0;
	m_theta[2] = 0;

	// vertices
	m_vertices[0][0] = -1; m_vertices[0][1] = -1; m_vertices[0][2] = 1; // vertex 1
	m_vertices[1][0] = -1; m_vertices[1][1] = 1; m_vertices[1][2] = 1; // vertex 2
	m_vertices[2][0] = 1; m_vertices[2][1] = 1; m_vertices[2][2] = 1; // vertex 3
	m_vertices[3][0] = 1; m_vertices[3][1] = -1; m_vertices[3][2] = 1; // vertex 4
	m_vertices[4][0] = -1; m_vertices[4][1] = -1; m_vertices[4][2] = -1; // vertex 5
	m_vertices[5][0] = -1; m_vertices[5][1] = 1; m_vertices[5][2] = -1; // vertex 6
	m_vertices[6][0] = 1; m_vertices[6][1] = 1; m_vertices[6][2] = -1; // vertex 7
	m_vertices[7][0] = 1; m_vertices[7][1] = -1; m_vertices[7][2] = -1; // vertex 8

	// normals
	m_normals[0][0] = 0; m_normals[0][1] = 0; m_normals[0][2] = 1; // (0, 3, 2, 1)
	m_normals[1][0] = 0; m_normals[1][1] = 1; m_normals[1][2] = 0; // (1, 2, 6, 5)
	m_normals[2][0] = 1; m_normals[2][1] = 0; m_normals[2][2] = 0; // (2, 3, 7, 6)
	m_normals[3][0] = 0; m_normals[3][1] = -1; m_normals[3][2] = 0; // (3, 0, 4, 7)
	m_normals[4][0] = 0; m_normals[4][1] = 0; m_normals[4][2] = -1; // (4, 5, 6, 7)
	m_normals[5][0] = -1; m_normals[5][1] = 0; m_normals[5][2] = 0; // (5, 4, 0, 1)

	// colors
	m_colors[0][0] = 0; m_colors[0][1] = 0; m_colors[0][2] = 1; m_colors[0][3] = 0.5;
	m_colors[1][0] = 0; m_colors[1][1] = 1; m_colors[1][2] = 1; m_colors[1][3] = 0.5;
	m_colors[2][0] = 1; m_colors[2][1] = 1; m_colors[2][2] = 1; m_colors[2][3] = 0.5;
	m_colors[3][0] = 1; m_colors[3][1] = 0; m_colors[3][2] = 1; m_colors[3][3] = 0.5;
	m_colors[4][0] = 0; m_colors[4][1] = 0; m_colors[4][2] = 0; m_colors[4][3] = 0.5;
	m_colors[5][0] = 0; m_colors[5][1] = 1; m_colors[5][2] = 0; m_colors[5][3] = 0.5;
	m_colors[6][0] = 1; m_colors[6][1] = 1; m_colors[6][2] = 0; m_colors[4][3] = 0.5;
	m_colors[7][0] = 1; m_colors[7][1] = 0; m_colors[7][2] = 0; m_colors[5][3] = 0.5;

	glEnable(GL_TEXTURE_2D);
	LoadTexture();

	for (int i = 0; i < 3; ++i) {
		yOffs[i] = 0.0f;
		xInc[i] = 0.005f;
		rot[i] = 0.0f;
	}
	xOffs[0] = 0.0f;
	xOffs[1] = 0.5f;
	xOffs[2] = 1.0f;
}

void CGLInterface::OnKeyDown(int keyValue)
{
	switch (keyValue)
	{
	case GLIF_LEFT:
		m_theta[1] -= 2.0f;
		if (m_theta[1] < 0) m_theta[1] += 360.0f;
		break;
	case GLIF_RIGHT:
		m_theta[1] += 2.0f;
		if (m_theta[1] >= 360) m_theta[1] -= 360.0f;
		break;
	case GLIF_UP:
		m_theta[0] -= 2.0f;
		if (m_theta[0] < 0) m_theta[0] += 360.0f;
		break;
	case GLIF_DOWN:
		m_theta[0] += 2.0f;
		if (m_theta[0] >= 360) m_theta[0] -= 360.0f;
		break;
	case GLIF_HOME:
		m_theta[2] -= 2.0f;
		if (m_theta[2] < 0) m_theta[2] += 360.0f;
		break;
	case GLIF_END:
		m_theta[2] += 2.0f;
		if (m_theta[2] >= 360) m_theta[2] -= 360.0f;
		break;
	}
}

void CGLInterface::DrawScene()
{
	glEnable(GL_DEPTH_TEST);

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_TEXTURE_2D);

	// draw the bouncing cubes
	drawCube(0, 0.0f, 1.5f, 2.5f, 1.5f);
	drawCube(1, 1.0f, 2.0f, 2.5f, 2.0f);
	drawCube(2, 2.0f, 3.5f, 2.5f, 2.5f);
}

void CGLInterface::LoadTexture()
{
	m_texRec[0] = LoadBMP("texture.bmp", 256, 256);
	m_texRec[1] = LoadBMP("texture2.bmp", 256, 256);
	glGenTextures(3, &m_texID[0]);	// Generates texture names

	// 이미지로 부터 Texutre 생성
	for (register int i = 0; i < 2; i++) {
		// The creation of a named texture that is bound to a texture target
		glBindTexture(GL_TEXTURE_2D, m_texID[i]);
		glTexImage2D(GL_TEXTURE_2D, 0, 3,
			256, 256,
			0, GL_RGB, GL_UNSIGNED_BYTE, m_texRec[i]);

		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

		// 다음을 처리하지 않을 경우 memory leak 발생
		delete[] m_texRec[i];
	}

	// Check Board Texture 생성
	for (register int i = 0, j, temp; i < 64; i++) {
		for (j = 0; j < 64; j++) {
			temp = (!(i & 0x8) ^ !(j & 0x8)) * 255;
			m_image[i][j][0] = (GLubyte)temp;
			m_image[i][j][1] = (GLubyte)temp;
			m_image[i][j][2] = (GLubyte)temp;
		}
	}

	// 3번째 생성된 텍스처 아이디 사용
	glBindTexture(GL_TEXTURE_2D, m_texID[2]);

	//checkboard texture
	for (register int i = 0, j, temp; i < 64; i++) {
		for (j = 0; j < 64; j++) {
			temp = (!(i & 0x8) ^ !(j & 0x8)) * 255;
			m_image[i][j][0] = (GLubyte)temp;
			m_image[i][j][1] = (GLubyte)temp;
			m_image[i][j][2] = (GLubyte)temp;
		}
	}

	glTexImage2D(GL_TEXTURE_2D, 0, 3, 64, 64, 0,
		GL_RGB, GL_UNSIGNED_BYTE, m_image);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);



	// Set the Rendering Mothod for Texture Mode: default mode
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	// Set the Rendering Mothod for Texture Mode: decal mode
	//	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

	// Set the Rendering Mothod for Texture Mode: blend mode
	//	GLfloat rgba[4] = {0.0, 0.0, 1.0, 1.0};
	//	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND);
	//	glTexEnvfv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, rgba);
}

// Step 3: Texture mapping을 위한 텍스쳐 좌표 설정
void CGLInterface::DrawQuad(int a, int b, int c, int d)
{
	glBegin(GL_QUADS);
	glNormal3fv(m_normals[a]);
	glColor4fv(m_colors[a]);
	// set the current texture coordinates
	glTexCoord2f(0.0, 0.0);
	glVertex3fv(m_vertices[a]);
	glColor4fv(m_colors[b]);
	// set the current texture coordinates
	glTexCoord2f(1.0, 0.0);
	glVertex3fv(m_vertices[b]);
	glColor3fv(m_colors[c]);
	// set the current texture coordinates
	glTexCoord2f(1.0, 1.0);
	glVertex3fv(m_vertices[c]);
	glColor4fv(m_colors[d]);
	// set the current texture coordinates
	glTexCoord2f(0.0, 1.0);
	glVertex3fv(m_vertices[d]);
	glEnd();
}

void CGLInterface::CBTextureCube()
{
	glBindTexture(GL_TEXTURE_2D, m_texID[2]);
	DrawQuad(0, 3, 2, 1);
	DrawQuad(1, 2, 6, 5);
	DrawQuad(2, 3, 7, 6);
	DrawQuad(3, 0, 4, 7);
	DrawQuad(4, 5, 6, 7);
	DrawQuad(5, 4, 0, 1);

}


void CGLInterface::TextureCube()
{
	// 다음은 cube의 각 면에 대하여 두 개의 텍스쳐를 번갈아가며
	// mapping한다.
	glBindTexture(GL_TEXTURE_2D, m_texID[1]);
	DrawQuad(0, 3, 2, 1);
	glBindTexture(GL_TEXTURE_2D, m_texID[0]);
	DrawQuad(1, 2, 6, 5);
	glBindTexture(GL_TEXTURE_2D, m_texID[1]);
	DrawQuad(2, 3, 7, 6);
	glBindTexture(GL_TEXTURE_2D, m_texID[0]);
	DrawQuad(3, 0, 4, 7);
	glBindTexture(GL_TEXTURE_2D, 0);//m_texID[1]);
	DrawQuad(4, 5, 6, 7);
	glBindTexture(GL_TEXTURE_2D, m_texID[0]);
	DrawQuad(5, 4, 0, 1);

}

void CGLInterface::drawCube(int i, GLfloat z, GLfloat rotation, GLfloat jmp, GLfloat amp)
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(xOffs[i], yOffs[i], z);
	glTranslatef(0.5f, 0.5f, 0.5f);
	GLfloat scale = 0.75f + i*(0.25f / 2.f);
	glScalef(scale, scale, scale);
	glRotatef(rot[i], 1.0f, 1.0f, 1.0f);
	glTranslatef(-0.5f, -0.5f, -0.5f);
	glColor4f(1.0f, 1.0f, 1.0f, 0.8f);

	if (i == 0 || i == 2)
		CBTextureCube();
	else if (i == 1)
		TextureCube();


	if (xOffs[i] > 1.0f || xOffs[i] < -1.0f) {
		xInc[i] = -xInc[i];
		xOffs[i] = xOffs[i] > 1.0f ? 1.0f : -1.0f;
	}
	xOffs[i] += xInc[i];
	yOffs[i] = fabs(cos((-3.141592f * jmp) * (xOffs[i])) * amp) - 1;
	rot[i] += rotation;

}

unsigned char* CGLInterface::LoadBMP(char* filename, int width, int height)
{
	unsigned char * data;

	FILE * file;

	file = fopen(filename, "rb");

	//if (file == NULL) return 0;
	data = new unsigned char[width * height * 3];
	//int size = fseek(file,);
	fread(data, width * height * 3, 1, file);
	fclose(file);

	for (int i = 0; i < width * height; ++i)
	{
		int index = i * 3;
		unsigned char B, R;
		B = data[index];
		R = data[index + 2];

		data[index] = R;
		data[index + 2] = B;

	}

	fclose(file);
	return  data;
}
