// example3Doc.h : interface of the CExample3Doc class
//


#pragma once


class MFCFrameDoc : public CDocument
{
protected: // create from serialization only
	MFCFrameDoc();
	DECLARE_DYNCREATE(MFCFrameDoc)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~MFCFrameDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};


