

#include "GLInterface.h"
#include <math.h>
#include <stdio.h>

CGLInterface::CGLInterface()
{
	
}

CGLInterface::~CGLInterface()
{

}


void CGLInterface::Initialize()
{
	InitializeDrawing();
}

void CGLInterface::SetProjectView(int cx, int cy)
{
	glViewport(0, 0, cx, cy);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)cx / (GLfloat)cy, 0.1f, 100.0f);

	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();									// Reset The Modelview Matrix

}

void CGLInterface::Display()
{
	DrawScene();
	glFlush();
}

void CGLInterface::InitializeDrawing()  // 그리기에 필효한 변수를 초기화 한다. 
{
	int i;
	for (i = 0; i<3; i++)
	{
		wiggle_count[i] = 0;
		xrot[i] = 0;				// X Rotation ( NEW )
		yrot[i] = 0;				// Y Rotation ( NEW )
		zrot[i] = 0;				// Z Rotation ( NEW )
	}

	LoadFlagTexture();

	glEnable(GL_TEXTURE_2D);							// Enable Texture Mapping 
	glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
	glClearColor(0.0f, 0.0f, 0.0f, 0.5f);				// Black Background
	//glClearDepth(1.0f);									// Depth Buffer Setup
	//glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
	//glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations
	glPolygonMode(GL_BACK, GL_FILL);					// Back Face Is Solid
	glPolygonMode(GL_FRONT, GL_LINE);					// Front Face Is Made Of Lines

	int x, y;
	for (x = 0; x<45; x++)
	{
		for (y = 0; y<45; y++)
		{
			points_korea[x][y][0] = float((x / 5.0f) - 4.5f);
			points_korea[x][y][1] = float((y / 5.0f) - 4.5f);
			points_korea[x][y][2] = float(sin((((x / 5.0f)*40.0f) / 360.0f)*3.141592654*2.0f));
		}
	}

	for (x = 0; x<45; x++)
	{
		for (y = 0; y<45; y++)
		{
			points_china[x][y][0] = float((x / 5.0f) - 4.5f);
			points_china[x][y][1] = float((y / 5.0f) - 4.5f);
			points_china[x][y][2] = float(sin((((x / 5.0f)*40.0f) / 360.0f)*3.141592654*2.0f));
		}
	}

	for (x = 0; x<45; x++)
	{
		for (y = 0; y<45; y++)
		{
			points_england[x][y][0] = float((x / 5.0f) - 4.5f);
			points_england[x][y][1] = float((y / 5.0f) - 4.5f);
			points_england[x][y][2] = float(sin((((x / 5.0f)*40.0f) / 360.0f)*3.141592654*2.0f));
		}
	}
}

void CGLInterface::OnKeyDown(int keyValue)
{
	switch (keyValue)
	{
	case GLIF_LEFT:
		break;
	case GLIF_RIGHT:
		break;
	case GLIF_UP:
		break;
	case GLIF_DOWN:
		break;
	case GLIF_HOME:
		break;
	case GLIF_END:
		break;
	}
}

void CGLInterface::DrawScene()
{
	int x, y;
	float float_x, float_y, float_xb, float_yb;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear The Screen And The Depth Buffer
	glLoadIdentity();									// Reset The View

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glPushMatrix();
	glTranslatef(0.0f, 0.0f, -12.0f);
	DrawKoreaFlag();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(1.0f, 2.0f, -13.0f);
	DrawChinaFlag();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-1.0f, -2.0f, -18.0f);
	DrawEnglandFlag();
	glPopMatrix();
}

void CGLInterface::LoadFlagTexture()
{
	m_texRec[0] = LoadBMP("flag_korea.bmp", 1024, 768);
	m_texRec[1] = LoadBMP("flag_china.bmp", 1024, 768);
	m_texRec[2] = LoadBMP("flag_england.bmp", 1024, 768);

	glGenTextures(3, &m_FlagTexID[0]);	// Generates texture names

	int i;
	for (i = 0; i<3; i++)
	{
		// The creation of a named texture that is bound to a texture target
		glBindTexture(GL_TEXTURE_2D, m_FlagTexID[i]);

		// param 1(GL_TEXTURE_2D): 텍스처가 2D 인지 1D인지 설정
		// param 2(Midmap): Midmap이라는 구현 방식의 Detail level 여기서는 그냥 0
		// param 3(3): 색상 구성 요소 개수
		// param 4(m_texRec->sizeX): 텍스처 x해상도
		// param 5(m_texRec->sizeY): 텍스처 y해상도
		// param 6(0): 텍스처 경계 두께 
		// param 7(GL_RGB): 컬러 구성 요소
		// param 8(GL_UNSIGNED_BYTE): 컬러 구성 요소의 데이터 타입
		// param 9(transBuf): 실제 데이터 값
		glTexImage2D(GL_TEXTURE_2D, 0, 3,
			1024, 768,
			0, GL_RGB, GL_UNSIGNED_BYTE, m_texRec[i]);

		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

		// 다음을 처리하지 않을 경우 memory leak 발생
		delete[] m_texRec[i];
	}
	// Set the Rendering Mothod for Texture Mode: default mode
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
}

void CGLInterface::DrawKoreaFlag()
{
	int x, y;
	float float_x, float_y, float_xb, float_yb;

	glRotatef(xrot[0], 1.0f, 0.0f, 0.0f);
	glRotatef(yrot[0], 0.0f, 1.0f, 0.0f);
	glRotatef(zrot[0], 0.0f, 0.0f, 1.0f);

	glBindTexture(GL_TEXTURE_2D, m_FlagTexID[0]);

	glBegin(GL_QUADS);
	glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
	for (x = 0; x < 44; x++)
	{
		for (y = 0; y < 44; y++)
		{
			float_x = float(x) / 44.0f;
			float_y = float(y) / 44.0f;
			float_xb = float(x + 1) / 44.0f;
			float_yb = float(y + 1) / 44.0f;

			glTexCoord2f(float_x, float_y);
			glVertex3f(points_korea[x][y][0], points_korea[x][y][1], points_korea[x][y][2]);

			glTexCoord2f(float_x, float_yb);
			glVertex3f(points_korea[x][y + 1][0], points_korea[x][y + 1][1], points_korea[x][y + 1][2]);

			glTexCoord2f(float_xb, float_yb);
			glVertex3f(points_korea[x + 1][y + 1][0], points_korea[x + 1][y + 1][1], points_korea[x + 1][y + 1][2]);

			glTexCoord2f(float_xb, float_y);
			glVertex3f(points_korea[x + 1][y][0], points_korea[x + 1][y][1], points_korea[x + 1][y][2]);
		}
	}
	glEnd();

	float temp;
	if (wiggle_count[0] == 2)
	{
		for (y = 0; y < 45; y++)
		{
			temp = points_korea[0][y][2];
			for (x = 0; x < 44; x++)
			{
				points_korea[x][y][2] = points_korea[x + 1][y][2];
			}
			points_korea[44][y][2] = temp;
		}
		wiggle_count[0] = 0;
	}

	wiggle_count[0]++;

	xrot[0] += 0.3f;
	yrot[0] += 0.2f;
	zrot[0] += 0.4f;
}

void CGLInterface::DrawChinaFlag()
{
	int x, y;
	float float_x, float_y, float_xb, float_yb;

	glRotatef(xrot[1], 1.0f, 0.0f, 0.0f);
	glRotatef(yrot[1], 0.0f, 1.0f, 0.0f);
	glRotatef(zrot[1], 0.0f, 0.0f, 1.0f);

	glBindTexture(GL_TEXTURE_2D, m_FlagTexID[1]);

	glBegin(GL_QUADS);
	glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
	for (x = 0; x < 44; x++)
	{
		for (y = 0; y < 44; y++)
		{
			float_x = float(x) / 44.0f;
			float_y = float(y) / 44.0f;
			float_xb = float(x + 1) / 44.0f;
			float_yb = float(y + 1) / 44.0f;

			glTexCoord2f(float_x, float_y);
			glVertex3f(points_china[x][y][0], points_china[x][y][1], points_china[x][y][2]);

			glTexCoord2f(float_x, float_yb);
			glVertex3f(points_china[x][y + 1][0], points_china[x][y + 1][1], points_china[x][y + 1][2]);

			glTexCoord2f(float_xb, float_yb);
			glVertex3f(points_china[x + 1][y + 1][0], points_china[x + 1][y + 1][1], points_china[x + 1][y + 1][2]);

			glTexCoord2f(float_xb, float_y);
			glVertex3f(points_china[x + 1][y][0], points_china[x + 1][y][1], points_china[x + 1][y][2]);
		}
	}
	glEnd();

	float temp;
	if (wiggle_count[1] == 2)
	{
		for (y = 0; y < 45; y++)
		{
			temp = points_china[0][y][2];
			for (x = 0; x < 44; x++)
			{
				points_china[x][y][2] = points_china[x + 1][y][2];
			}
			points_china[44][y][2] = temp;
		}
		wiggle_count[1] = 0;
	}

	wiggle_count[1]++;

	xrot[1] += 0.4f;
	yrot[1] += 0.1f;
	zrot[1] += 0.24f;
}

void CGLInterface::DrawEnglandFlag()
{
	int x, y;
	float float_x, float_y, float_xb, float_yb;

	glRotatef(xrot[2], 1.0f, 0.0f, 0.0f);
	glRotatef(yrot[2], 0.0f, 1.0f, 0.0f);
	glRotatef(zrot[2], 0.0f, 0.0f, 1.0f);

	glBindTexture(GL_TEXTURE_2D, m_FlagTexID[2]);

	glBegin(GL_QUADS);
	glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
	for (x = 0; x < 44; x++)
	{
		for (y = 0; y < 44; y++)
		{
			float_x = float(x) / 44.0f;
			float_y = float(y) / 44.0f;
			float_xb = float(x + 1) / 44.0f;
			float_yb = float(y + 1) / 44.0f;

			glTexCoord2f(float_x, float_y);
			glVertex3f(points_england[x][y][0], points_england[x][y][1], points_england[x][y][2]);

			glTexCoord2f(float_x, float_yb);
			glVertex3f(points_england[x][y + 1][0], points_england[x][y + 1][1], points_england[x][y + 1][2]);

			glTexCoord2f(float_xb, float_yb);
			glVertex3f(points_england[x + 1][y + 1][0], points_england[x + 1][y + 1][1], points_england[x + 1][y + 1][2]);

			glTexCoord2f(float_xb, float_y);
			glVertex3f(points_england[x + 1][y][0], points_england[x + 1][y][1], points_england[x + 1][y][2]);
		}
	}
	glEnd();

	float temp;
	if (wiggle_count[2] == 2)
	{
		for (y = 0; y < 45; y++)
		{
			temp = points_england[0][y][2];
			for (x = 0; x < 44; x++)
			{
				points_england[x][y][2] = points_england[x + 1][y][2];
			}
			points_england[44][y][2] = temp;
		}
		wiggle_count[2] = 0;
	}

	wiggle_count[2]++;

	xrot[2] += 0.1f;
	yrot[2] += 0.1f;
	zrot[2] += 0.5f;
}

unsigned char* CGLInterface::LoadBMP(char* filename, int width, int height)
{
	unsigned char * data;

	FILE * file;

	file = fopen(filename, "rb");

	//if (file == NULL) return 0;
	data = new unsigned char[width * height * 3];
	//int size = fseek(file,);
	fread(data, width * height * 3, 1, file);
	fclose(file);

	for (int i = 0; i < width * height; ++i)
	{
		int index = i * 3;
		unsigned char B, R;
		B = data[index];
		R = data[index + 2];

		data[index] = R;
		data[index + 2] = B;

	}

	fclose(file);
	return  data;
}
