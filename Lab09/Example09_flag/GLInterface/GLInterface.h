#ifndef GL_INTERFACE_H
#define GL_INTERFACE_H

#ifdef WIN32
#include <Windows.h>
#endif

#ifndef _APPLE_OSX
#include <gl/GL.h>
#include <gl/GLU.h>
//#include <gl/glut.h>
#else
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
//#include <GLUT/glut.h>
#endif


#ifdef _AFXDLL   // for MFC
#define GLIF_RIGHT          0x27
#define GLIF_LEFT           0x25
#define GLIF_UP				0x26
#define GLIF_DOWN           0x28
#define GLIF_HOME           0x24
#define GLIF_END            0x23
#else           // for Qt
#define GLIF_RIGHT          0x01000014
#define GLIF_LEFT           0x01000012
#define GLIF_UP				0x01000013
#define GLIF_DOWN           0x01000015
#define GLIF_HOME           0x01000010
#define GLIF_END            0x01000011
#endif


typedef GLfloat point3[3];


class CGLInterface
{
public:
	/// Constructor
	CGLInterface();

	/// Destructor
	virtual ~CGLInterface();

	/// Initialization
	void Initialize();

	/// set projection
	void SetProjectView(int cx, int cy);

	/// draw display by OpenGL functions
	void Display();


// Function Definition Start: SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSs
public:

	/// Set the camera WRT WC and display the scene(카메라를 설정하고 scene 전체를 그린다) 
	void DrawScene(); 

	/// Key Event handler
	void OnKeyDown(int keyValue); 
	/// variable initialization. 그리기에 필요한 변수를 초기화 한다. 
	void InitializeDrawing();				
	/// timer interrupt event
	void OnTimer();

	/// load texture 
	void LoadFlagTexture();

	/// draw korea flag
	void DrawKoreaFlag();

	/// draw china flag
	void DrawChinaFlag();

	/// draw england flag
	void DrawEnglandFlag();

	/// load bmp file
	unsigned char* LoadBMP(char* filename, int width, int height);
// Function Definition End: EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEe
protected:

	float points_korea[45][45][3];    // The Array For The Points On The Grid Of Our "Wave"
	float points_china[45][45][3];    // The Array For The Points On The Grid Of Our "Wave"
	float points_england[45][45][3];    // The Array For The Points On The Grid Of Our "Wave"

	int wiggle_count[3];		// Counter Used To Control How Fast Flag Waves

	GLfloat	xrot[3];				// X Rotation ( NEW )
	GLfloat	yrot[3];				// Y Rotation ( NEW )
	GLfloat	zrot[3];				// Z Rotation ( NEW )
	GLfloat hold[3];				// Temporarily Holds A Floating Point Value

	// Texture Mapping을 위한 멤버 추가
	unsigned char*    m_texRec[3];
	GLuint			  m_FlagTexID[3];  // texture id

	
	
};

#endif
