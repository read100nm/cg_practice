#include "GLInterface.h"
#include <math.h>
#include <stdio.h>

CGLInterface::CGLInterface()
{
	// TODO: add construction code here
	m_theta[0] = 0;
	m_theta[1] = 0;
	m_theta[2] = 0;

	// vertices
	m_vertices[0][0] = -1; m_vertices[0][1] = -1; m_vertices[0][2] = 1; // vertex 1
	m_vertices[1][0] = -1; m_vertices[1][1] = 1; m_vertices[1][2] = 1; // vertex 2
	m_vertices[2][0] = 1; m_vertices[2][1] = 1; m_vertices[2][2] = 1; // vertex 3
	m_vertices[3][0] = 1; m_vertices[3][1] = -1; m_vertices[3][2] = 1; // vertex 4
	m_vertices[4][0] = -1; m_vertices[4][1] = -1; m_vertices[4][2] = -1; // vertex 5
	m_vertices[5][0] = -1; m_vertices[5][1] = 1; m_vertices[5][2] = -1; // vertex 6
	m_vertices[6][0] = 1; m_vertices[6][1] = 1; m_vertices[6][2] = -1; // vertex 7
	m_vertices[7][0] = 1; m_vertices[7][1] = -1; m_vertices[7][2] = -1; // vertex 8

	// normals
	m_normals[0][0] = 0; m_normals[0][1] = 0; m_normals[0][2] = 1; // (0, 3, 2, 1)
	m_normals[1][0] = 0; m_normals[1][1] = 1; m_normals[1][2] = 0; // (1, 2, 6, 5)
	m_normals[2][0] = 1; m_normals[2][1] = 0; m_normals[2][2] = 0; // (2, 3, 7, 6)
	m_normals[3][0] = 0; m_normals[3][1] = -1; m_normals[3][2] = 0; // (3, 0, 4, 7)
	m_normals[4][0] = 0; m_normals[4][1] = 0; m_normals[4][2] = -1; // (4, 5, 6, 7)
	m_normals[5][0] = -1; m_normals[5][1] = 0; m_normals[5][2] = 0; // (5, 4, 0, 1)

	// colors
	m_colors[0][0] = 0; m_colors[0][1] = 0; m_colors[0][2] = 1; m_colors[0][3] = 1;
	m_colors[1][0] = 0; m_colors[1][1] = 1; m_colors[1][2] = 1; m_colors[1][3] = 1;
	m_colors[2][0] = 1; m_colors[2][1] = 1; m_colors[2][2] = 1; m_colors[2][3] = 1;
	m_colors[3][0] = 1; m_colors[3][1] = 0; m_colors[3][2] = 1; m_colors[3][3] = 1;
	m_colors[4][0] = 0; m_colors[4][1] = 0; m_colors[4][2] = 0; m_colors[4][3] = 1;
	m_colors[5][0] = 0; m_colors[5][1] = 1; m_colors[5][2] = 0; m_colors[5][3] = 1;
	m_colors[6][0] = 1; m_colors[6][1] = 1; m_colors[6][2] = 0; m_colors[4][3] = 1;
	m_colors[7][0] = 1; m_colors[7][1] = 0; m_colors[7][2] = 0; m_colors[5][3] = 1;
}

CGLInterface::~CGLInterface()
{

}


void CGLInterface::Initialize()
{
	InitializeDrawing();
}

void CGLInterface::SetProjectView(int cx, int cy)
{
	// cx와 cy는 클라이언트 영역의 x축, y축 크기

	// TODO: Add your message handler code here
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// orthographic projection
	if (cx <= cy)
		glOrtho(-2.0, 2.0, -2.0*(GLfloat)cy / (GLfloat)cx,
		2.0*(GLfloat)cy / (GLfloat)cx, -10.0, 10.0);
	else
		glOrtho(-2.0*(GLfloat)cx / (GLfloat)cy,
		2.0*(GLfloat)cx / (GLfloat)cy, -2.0, 2.0, -10.0, 10.0);

	glViewport(0, 0, cx, cy);
}

void CGLInterface::Display()
{
	DrawScene();
	glFlush();
}

void CGLInterface::InitializeDrawing()  // 그리기에 필효한 변수를 초기화 한다. 
{
	// TODO: add construction code here
	glEnable(GL_TEXTURE_2D);
	LoadCBTexture();
}

void CGLInterface::OnKeyDown(unsigned char keyValue)
{
	switch (keyValue)
	{
	case GLIF_LEFT:
		m_theta[1] -= 2.0f;
		if (m_theta[1] < 0) m_theta[1] += 360.0f;
		break;
	case GLIF_RIGHT:
		m_theta[1] += 2.0f;
		if (m_theta[1] >= 360) m_theta[1] -= 360.0f;
		break;
	case GLIF_UP:
		m_theta[0] -= 2.0f;
		if (m_theta[0] < 0) m_theta[0] += 360.0f;
		break;
	case GLIF_DOWN:
		m_theta[0] += 2.0f;
		if (m_theta[0] >= 360) m_theta[0] -= 360.0f;
		break;
	case GLIF_HOME:
		m_theta[2] -= 2.0f;
		if (m_theta[2] < 0) m_theta[2] += 360.0f;
		break;
	case GLIF_END:
		m_theta[2] += 2.0f;
		if (m_theta[2] >= 360) m_theta[2] -= 360.0f;


		break;
	}
}

void CGLInterface::DrawScene()
{
	glEnable(GL_DEPTH_TEST);

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(2.0, 2.0, 2.0, 0, 0, 0, 0, 1.0, 0);

	glRotatef(m_theta[2], 0.0f, 0.0f, 1.0f);
	glRotatef(m_theta[1], 0.0f, 1.0f, 0.0f);
	glRotatef(m_theta[0], 1.0f, 0.0f, 0.0f);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
	glEnable(GL_COLOR_MATERIAL);

	// Step 2: Texture의 사용이 가능하도록 flag 활성화
	glEnable(GL_TEXTURE_2D);
	glPushMatrix();
	glTranslatef(-2.0, 0.0, 0.0);
	CBTextureCube();
	glPopMatrix();
	glTranslatef(2.0, 0.0, 0.0);
	TextureCube();
	glPopMatrix();
}

// Step 1: 체크보드 무늬의 텍스쳐를 생성한다.
void CGLInterface::LoadCBTexture()
{
	//checkboard texture
	for (register int i = 0, j, temp; i < 64; i++) {
		for (j = 0; j < 64; j++) {
			temp = (!(i & 0x8) ^ !(j & 0x8)) * 255;
			m_image[i][j][0] = (GLubyte)temp;
			m_image[i][j][1] = (GLubyte)temp;
			m_image[i][j][2] = (GLubyte)temp;
		}
	}

	// ????????

	// Set the Rendering Mothod for Texture Mode: default mode
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

}


void CGLInterface::LoadImgTexture()
{
	
}



// Step 3: Texture mapping을 위한 텍스쳐 좌표 설정
void CGLInterface::DrawQuad(int a, int b, int c, int d)
{
	glBegin(GL_QUADS);
	glNormal3fv(m_normals[a]);
	glColor3fv(m_colors[a]);

	//?? set the current texture coordinates
	glVertex3fv(m_vertices[a]);
	glColor3fv(m_colors[b]);

	//?? set the current texture coordinates
	glVertex3fv(m_vertices[b]);
	glColor3fv(m_colors[c]);

	//?? set the current texture coordinates
	glVertex3fv(m_vertices[c]);
	glColor3fv(m_colors[d]);

	//?? set the current texture coordinates
	glVertex3fv(m_vertices[d]);
	glEnd();
}

void CGLInterface::CBTextureCube()
{
	DrawQuad(0, 3, 2, 1);
	DrawQuad(1, 2, 6, 5);
	DrawQuad(2, 3, 7, 6);
	DrawQuad(3, 0, 4, 7);
	DrawQuad(4, 5, 6, 7);
	DrawQuad(5, 4, 0, 1);
}


void CGLInterface::TextureCube()
{

}

unsigned char* CGLInterface::LoadBMP(char* filename, int width, int height)
{
	unsigned char * data;

	FILE * file;

	file = fopen(filename, "rb");

	//if (file == NULL) return 0;
	data = new unsigned char [width * height * 3];
	//int size = fseek(file,);
	fread(data, width * height * 3, 1, file);
	fclose(file);

	for (int i = 0; i < width * height; ++i)
	{
		int index = i * 3;
		unsigned char B, R;
		B = data[index];
		R = data[index + 2];

		data[index] = R;
		data[index + 2] = B;

	}

	fclose(file);
	return  data;
}

