#ifndef GL_INTERFACE_H
#define GL_INTERFACE_H

#ifdef WIN32
#include <Windows.h>
#endif

#ifndef _APPLE_OSX
#include <gl/GL.h>
#include <gl/GLU.h>
#else
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#endif


#define GLIF_END            0x23
#define GLIF_HOME           0x24
#define GLIF_LEFT           0x25
#define GLIF_UP             0x26
#define GLIF_RIGHT          0x27
#define GLIF_DOWN           0x28

typedef GLfloat point3[3];


class CGLInterface
{
public:
	/// Constructor
	CGLInterface();

	/// Destructor
	virtual ~CGLInterface();

	/// Initialization
	void Initialize();

	/// set projection
	void SetProjectView(int cx, int cy);

	/// draw display by OpenGL functions
	void Display();


// Function Definition Start: SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSs
public:

	void DrawScene();
	void LoadImgTexture();
	void LoadCBTexture();
	void CBTextureCube();  // draw cube with checkerboard texture
	void TextureCube();	// draw cube with textures in the bit maps read in
	void DrawQuad(int a, int b, int c, int d);
	unsigned char* LoadBMP(char* filename, int width, int height);


	/// Key Event handler
	void OnKeyDown(unsigned char keyValue); 
	/// variable initialization. 그리기에 필요한 변수를 초기화 한다. 
	void InitializeDrawing();				
	/// timer interrupt event

	

// Function Definition End: EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEe
protected:

	
	
	GLfloat m_theta[3];			// for rotate
	GLfloat m_vertices[8][3];	// for drawing a quad
	GLfloat m_colors[8][4];		// for coloring
	GLfloat m_normals[6][3];	// normal

	// Texture Mapping을 위한 멤버 추가
	GLubyte m_image[64][64][3];
	unsigned char* m_texRec[2];
	GLuint		   m_texID[2];  // test id
};

#endif
