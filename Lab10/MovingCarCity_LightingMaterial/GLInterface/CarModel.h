// CarModel.h: interface for the CCarModel class.
//
//////////////////////////////////////////////////////////////////////
#ifndef GL_CAR_MODEL_H
#define GL_CAR_MODEL_H

#ifdef WIN32
#include <Windows.h>
#endif

#ifndef _APPLE_OSX
#include <gl/GL.h>
#include <gl/GLU.h>
//#include <gl/glut.h>
#else
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
//#include <GLUT/glut.h>
#endif

#define	PI  3.14159265359f
#define RIGHT 1
#define LEFT 2
#define STRAIGHT 0
extern GLfloat BLUE[3];  // Blue
extern GLfloat YELLOW[3];  
extern GLfloat GREEN[3];  
extern GLfloat RED[3];  
extern GLfloat WHITE[3];  // Blue
extern GLfloat GRAY[3];  // Blue

/*====================================== CCarModel  Class  ============================================
	- Model a car and display it at the predefined position and orientation
	
==========================================================================================================*/
/*====================================== CCarModel  Class  ============================================
- Model a car and display it at the predefined position and orientation

==========================================================================================================*/
class CCarModel
{
public:

	CCarModel();   //constructor
	// set the parameters need for simulation
	void SetMovementParameters();  //Initialize the parameters
	//Draw wheel
	void DrawWheel(float rotateY, float rotateZ, float posX, float posY, float posZ);
	//draw a car with the followings: steering angle(handleTurn) and amount of turn of the wheel(wheelTurn)
	void Display(float handleTurn, float wheelTurn);  // handleTurn: 
	virtual ~CCarModel();

	void DisplayQuad(GLfloat arr[][3], int a, int b, int c, int d);	// display a quad defined by the four points: a,b,c,d

	/*==================================================================================
	Function: Place a cube defined by the function "RegCube()" at the position (posX,posY,posY)
	after scaling the cube by using the parameter (scaleX, scaleY, scaleZ)
	====================================================================================*/
	void PlaceObject(float scaleX, float scaleY, float scaleZ,
		float rotateY,
		float posX, float posY, float posZ);

	/*==================================================================================
	Function: Display the quad deinfed by the "RegQuad()" at (xPos,zPos)
	after scaling it by using the paramter (xScale, zScale)
	and rotating it with respect to y axis by yRoate
	====================================================================================*/
	void DrawPlane(float xScale, float zScale,
		float yRotate,
		float xPos, float zPos);
	/* ==================================================================================================
	Function : define GL lists for square and cube. They are specific to the car modeling
	=====================================================================================================*/
	void DefinePrimitiveListforCar();

protected:

	GLfloat  curTireAngle;	// 현재 바퀴의 회전 위치
	GLfloat  curRotSpeed;   // 회전 속도: 
	GLint   QUADLIST;   // OpenGL List pointer for the quad defined by "RegQuad()"
	GLint   CUBLIST;	// OpenGL List pointer for the Cube defined by "RegCube()"
};

#endif // !defined(AFX_CARMODEL_H__F3461EB6_C438_4E98_B7A4_CDC94B39B6CA__INCLUDED_)
