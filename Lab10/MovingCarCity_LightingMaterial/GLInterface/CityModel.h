// CityModel.h: interface for the CCityModel class.
//
//////////////////////////////////////////////////////////////////////
#ifndef GL_CITY_MODEL_H
#define GL_CITY_MODEL_H

#ifdef WIN32
#include <Windows.h>
#endif

#ifndef _APPLE_OSX
#include <gl/GL.h>
#include <gl/GLU.h>
//#include <gl/glut.h>
#else
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
//#include <GLUT/glut.h>
#endif

#define	PI  3.14159265359f
extern GLfloat BLUE[3];  // Blue
extern GLfloat YELLOW[3];  
extern GLfloat GREEN[3];  
extern GLfloat RED[3];  
extern GLfloat WHITE[3];  // Blue
extern GLfloat GRAY[3];  // Blue

#define STREETWIDTH 80

#define GROUND_LIMIT 10000
#define GROUND_LIMITF 10000.0f




/* =========================================   CCityModel Class    =========================================
The scene construction procedures:
- Draw the checkerboard textures on the XZ plane of SC
- Generate a small straight street block
- Generate a small street corner block
- Generate a small city block by connecting the street blocks
- Generate the scene by using connecting the city blocks
=======================================================================================================*/
class CCityModel
{
public:
	CCityModel();
	virtual ~CCityModel();

	void DrawGround();		// Draw checkerboard textures on the ground. Assume that the ground is XZ plane
	void DrawStreetMidleBlock(GLfloat xpos, GLfloat zpos, GLfloat yRotate);		// draw a straight street block
	void DrawStreetCornerBlock(GLfloat xpos, GLfloat zpos, GLfloat yRotate);	// draw abstreet corner block
	void DrawCrossRoadBlock(GLfloat xpos, GLfloat zpos, GLfloat yRotate);	    // draw Lower right hand corner section of a crossroad
	void DrawCrossRoad(GLfloat xpos, GLfloat zpos, GLfloat yRotate);
	//void DrawCityBlock();			// build a street block by connecting the two street blocks
	void RenderScene();				// build a city street by connecting the city block
	void DefinePrimitiveListforScene(); // register primitives for scene modeling
	void DisplayQuad(GLfloat arr[][3], int a, int b, int c, int d);	// display a quad defined by the four points: a,b,c,d

	/*==================================================================================
	Function: Place a cube defined by the function "RegCube()" at the position (posX,posY,posY)
	after scaling the cube by using the parameter (scaleX, scaleY, scaleZ)
	====================================================================================*/
	void PlaceObject(float scaleX, float scaleY, float scaleZ,
		float rotateY,
		float posX, float posY, float posZ);

	/*==================================================================================
	Function: Display the quad deinfed by the "RegQuad()" at (xPos,zPos)
	after scaling it by using the paramter (xScale, zScale)
	and rotating it with respect to y axis by yRoate
	====================================================================================*/
	void DrawPlane(float xScale, float zScale,
		float yRotate,
		float xPos, float zPos);
	/* ==================================================================================================
	Function : Draw a line
	=====================================================================================================*/
	void DrawLine(float xStart, float yStart, float zStart, float xEnd, float yEnd, float zEnd);

private:
	GLfloat Unit;
	GLfloat buildingBlockWidth;			// = 6Unit
	GLfloat sideWalkWidth; // = 2Unit
	GLfloat streetMidleBlockSize;	 // = 16Unit   
	GLfloat streetCornerSize; //=6Unit
	GLfloat sideWalkColor[3]; // = {5.0,2.0,1.0};  // sideWalk color
	GLfloat streetWidth; // = 16Unit
	GLfloat crossRoadBlockSize; //=38U,   38U x 38U
	GLint   QUADLIST;   // OpenGL List pointer for the quad defined by "RegQuad()"
	GLint   CUBLIST;	// OpenGL List pointer for the Cube defined by "RegCube()"

};





#endif // !defined(AFX_CITYMODEL_H__DDCB4B98_BD2A_4FAB_B9CB_9FD4ABD54D82__INCLUDED_)
