#ifndef GL_INTERFACE_H
#define GL_INTERFACE_H

#ifdef WIN32
#include <Windows.h>
#endif

#ifndef _APPLE_OSX
#include <gl/GL.h>
#include <gl/GLU.h>
//#include <gl/glut.h>
#else
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
//#include <GLUT/glut.h>
#endif

#include "CityModel.h"
#include "CarModel.h"


#ifdef _AFXDLL   // for MFC
#define GLIF_RIGHT          0x27
#define GLIF_LEFT           0x25
#define GLIF_UP				0x26
#define GLIF_DOWN           0x28
#define GLIF_HOME           0x24
#define GLIF_END            0x23
#define GLIF_1				0x31
#define GLIF_2				0x32
#define GLIF_3				0x33
#else           // for Qt
#define GLIF_RIGHT          0x01000014
#define GLIF_LEFT           0x01000012
#define GLIF_UP				0x01000013
#define GLIF_DOWN           0x01000015
#define GLIF_HOME           0x01000010
#define GLIF_END            0x01000011
#define GLIF_1				0x31
#define GLIF_2				0x32
#define GLIF_3				0x33
#endif


class CGLInterface
{
public:
	/// Constructor
	CGLInterface();

	/// Destructor
	virtual ~CGLInterface();

	/// Initialization
	void Initialize();

	/// set projection
	void SetProjectView(int cx, int cy);

	/// draw display by OpenGL functions
	void Display();


// Function Definition Start: SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSs
public:

	/// Set the camera WRT WC and display the scene(카메라를 설정하고 scene 전체를 그린다) 
	void DrawScene(); 

	/// Key Event handler
	void OnKeyDown(int keyValue); 
	/// variable initialization. 그리기에 필요한 변수를 초기화 한다. 
	void InitializeDrawing();				
	/// timer interrupt event
	void OnTimer();

	/// Update the parameters necessary for the car simulation 
	void SetCarMovement();
	

	/// load bmp file
	static unsigned char* LoadBMP(char* filename, int width, int height);
// Function Definition End: EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEe
protected:

	GLdouble g_Viewer[3];	// Camera position
	GLdouble g_Center[3];	// the point where the camera is looking at

							// data structures that holds the boundary information of streets and sidewalks

	CCityModel cityModel;		// create a city model
	CCarModel carModel;			// car model
	int cameraPositon; // 1: at the pixed position, 2: 운전석, 3: 자동차 상단. 
	GLfloat carXPos, carZPos; // current car postion
	GLfloat carAngleSpeed; // speed of the car. angle speed( degree / frame)
	GLfloat carOrientation;  // current car orientation
	GLfloat steeringDirection;  // turnDirection
	GLfloat steeringSpeed; // turning speed of the handle. degree/ frame
	GLfloat carSize; //determine the size of the car. 1: standard, 3: 3 times lager
	int cameraPosition; // 1: at the pixed position, 2: at driver seat, 3: top of the car 
	GLfloat hdLght1PosX, hdLght1PosY, hdLght1PosZ;	// head light postion

};

#endif
