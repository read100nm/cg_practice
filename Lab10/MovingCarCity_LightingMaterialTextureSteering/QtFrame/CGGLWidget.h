#ifndef OPENGL_PROJECTORSCREEN_VIEWER_H_011
#define OPENGL_PROJECTORSCREEN_VIEWER_H_011

#include "GLInterface.h"
#include <qopenglwidget.h>
#include <qtimer.h>
class QPushButton;

class CGGLWidget :public QOpenGLWidget
{
	Q_OBJECT

public:
	CGGLWidget(QWidget *parent = 0);
	~CGGLWidget();

public slots:
	void timeout();

protected:
	void initializeGL();
	void resizeGL(int w, int h);
	void paintGL();

	virtual void  keyPressEvent(QKeyEvent *event);

	CGLInterface m_iGL;

	QTimer m_Timter;
};

#endif
