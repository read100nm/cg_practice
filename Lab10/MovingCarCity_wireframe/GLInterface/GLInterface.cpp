

#include "GLInterface.h"
#include <math.h>
#include <stdio.h>

float BLUE[3] = { 0.0,0.0,1.0 };  // Blue
float YELLOW[3] = { 1.0,1.0,0.0 };
float GREEN[3] = { 0.0,1.0,0.0 };
float RED[3] = { 1.0,0.0,0.0 };
float WHITE[3] = { 1.0,1.0,1.0 };  // Blue
float GRAY[3] = { 0.5,0.5,0.5 };  // Blue


CGLInterface::CGLInterface()
{
	
}

CGLInterface::~CGLInterface()
{

}


void CGLInterface::Initialize()
{
	InitializeDrawing();
}

void CGLInterface::SetProjectView(int cx, int cy)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(65.0, (GLfloat)cx / (GLsizei)cy, 1.0, 500.0);
	glViewport(0, 0, cx, cy);
}

void CGLInterface::Display()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);	// buffer clear color 설정
	glClear(GL_COLOR_BUFFER_BIT);			// 설정된 color로 버퍼를 clear
	DrawScene();
	glFlush();
}

void CGLInterface::InitializeDrawing()  // 그리기에 필효한 변수를 초기화 한다. 
{
	g_Viewer[0] = 50.0f;
	g_Viewer[1] = 80.0f;
	g_Viewer[2] = 130.0f;

	g_Center[0] = 0.0f;
	g_Center[1] = 0.0f;
	g_Center[2] = 0.0f;	// 카메라의 초기 초점

	cameraPositon = 1; // 1: at the pixed position, 2: 운전석, 3: 자동차 상단.
	carXPos = 0;
	carZPos = 0; // 자동차의 현재 위치
	carAngleSpeed = 0.1; // 자동차의 각속도, 
	carOrientation = 0;  // 자동차 orientation
	carDirection = 1.0;
	turnAngle = 1; // 핸들의 방향 
	carSize = 10;
	
	cityModel.DefinePrimitiveListforScene(); // define and register primitives for scene modeling
	carModel.DefinePrimitiveListforCar(); //define and register primitive lists for car modeling

}

void CGLInterface::OnKeyDown(int keyValue)
{
	switch (keyValue)
	{
	case GLIF_LEFT:
		break;
	case GLIF_RIGHT:
		break;
	case GLIF_UP:
		break;
	case GLIF_DOWN:
		break;
	case GLIF_HOME:
		break;
	case GLIF_END:
		break;
	}
}

void CGLInterface::SetCarMovement()
{
#define WHEELSIZE 1
#define TURNSPEED 0.05
#define CITY_LIMIT 40

	float theta, carMovingSpeed;
	float wheelSize = WHEELSIZE*carSize;

	// 이동 거리를 계산: 바퀴의 속도에 비례 
	carMovingSpeed = 2.0f * PI * wheelSize * carAngleSpeed / 360.0f;

	// 바퀴의 좌우 회전 방향(각도)를 계산
	if (carDirection == RIGHT)
		turnAngle = -TURNSPEED;
	else if (carDirection == LEFT)
		turnAngle = TURNSPEED;
	else
		turnAngle = 0.0;

	// 현재 자동차의 축적된 orientation 계산
	carOrientation += turnAngle;
	if (carOrientation > 360.0f)
		carOrientation = 0.0f;
	else if (carOrientation < 0.0f)
		carOrientation = 360.0f;

	// 축적된 자동차의 위치 계산
	theta = -carOrientation*PI / 180.0;
	carXPos += carMovingSpeed * cos(theta);
	carZPos += carMovingSpeed * sin(theta);
	if (carXPos > CITY_LIMIT)
		carXPos = CITY_LIMIT;
	else if (carXPos < -CITY_LIMIT)
		carXPos = CITY_LIMIT;

	if (carZPos > CITY_LIMIT)
		carZPos = CITY_LIMIT;
	else if (carZPos < -CITY_LIMIT)
		carZPos = CITY_LIMIT;


}
void CGLInterface::DrawScene()
{

	glEnable(GL_DEPTH_TEST);	// Enable hidden surface removal
	glMatrixMode(GL_MODELVIEW); // Modelview 행렬 스택 선택
	glLoadIdentity();			// 현재 변환 행렬을 단위 행렬로 대치

	g_Viewer[0] = 20.0f;
	g_Viewer[1] = 300.0f;
	g_Viewer[2] = 20.0f;

	// viewing 변환 정의
	gluLookAt(g_Viewer[0], g_Viewer[1], g_Viewer[2],
		g_Center[0], g_Center[1], g_Center[2],
		1.0, 0.0, 0.0);


    // RGB color display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 

    // RenderScene 함수 호출
	cityModel.RenderScene();	

	// car modeling
	glPushMatrix();	// 현재 행렬을 그 행렬 스택에서 밖으로 꺼낸다
	glTranslatef(carXPos, 0.0, carZPos); // 해당 위치로 이동 
	glRotatef(carOrientation, 0.0, 1.0, 0.0); // Y축에 대한 회전
	SetCarMovement();
	glPushMatrix();
	glScalef(carSize, carSize, carSize); // magnify the car
	carModel.Display(turnAngle, carAngleSpeed); //CCarModel::Display(float handleTurn, float wheelTrun)
	glPopMatrix();
	glPopMatrix();	// 현재 행렬을 그 행렬 스택에서 밖으로 꺼낸다
}

unsigned char* CGLInterface::LoadBMP(char* filename, int width, int height)
{
	unsigned char * data;

	FILE * file;

	file = fopen(filename, "rb");

	//if (file == NULL) return 0;
	data = new unsigned char[width * height * 3];
	//int size = fseek(file,);
	fread(data, width * height * 3, 1, file);
	fclose(file);

	for (int i = 0; i < width * height; ++i)
	{
		int index = i * 3;
		unsigned char B, R;
		B = data[index];
		R = data[index + 2];

		data[index] = R;
		data[index + 2] = B;

	}

	fclose(file);
	return  data;
}
