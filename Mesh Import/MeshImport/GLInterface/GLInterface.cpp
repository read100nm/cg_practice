

#include "GLInterface.h"
#include <math.h>


CGLInterface::CGLInterface()
{
	
}

CGLInterface::~CGLInterface()
{

}


void CGLInterface::Initialize()
{
	InitializeDrawing();
}

void CGLInterface::SetProjectView(int cx, int cy)
{
	// cx와 cy는 클라이언트 영역의 x축, y축 크기

	// TODO: Add your message handler code here
	glViewport(0, 0, cx, cy);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	float aspect = cx / (float)(cy ? cy : 1);
	gluPerspective(45, 1.0*(float)cx / float(cy), 1, 20);
}

void CGLInterface::Display()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);	// buffer clear color 설정
	glClear(GL_COLOR_BUFFER_BIT);	// 설정된 color로 버퍼를 clear
	DrawScene();
	glFlush();
}

void CGLInterface::InitializeDrawing()  // 그리기에 필효한 변수를 초기화 한다. 
{
	m_theta[0] = 0;
	m_theta[1] = 0;
	m_theta[2] = 0;
	//m3DObject.LoadModel("C:\\Users\\byungyong\\Downloads\\3d_objects\\3d_objects\\Nathan\\nathan.obj");
}

void CGLInterface::OnKeyDown(int keyValue)
{
	switch (keyValue)
	{
	case GLIF_LEFT:
		m_theta[1] -= 2.0f;
		if (m_theta[1] < 0) m_theta[1] += 360.0f;
		break;
	case GLIF_RIGHT:
		m_theta[1] += 2.0f;
		if (m_theta[1] >= 360) m_theta[1] -= 360.0f;
		break;
	case GLIF_UP:
		m_theta[0] -= 2.0f;
		if (m_theta[0] < 0) m_theta[0] += 360.0f;
		break;
	case GLIF_DOWN:
		m_theta[0] += 2.0f;
		if (m_theta[0] >= 360) m_theta[0] -= 360.0f;
		break;
	case GLIF_HOME:
		m_theta[2] -= 2.0f;
		if (m_theta[2] < 0) m_theta[2] += 360.0f;
		break;
	case GLIF_END:
		m_theta[2] += 2.0f;
		if (m_theta[2] >= 360) m_theta[2] -= 360.0f;
		break;
	}
}

void CGLInterface::DrawScene()
{
	glClearColor(1.0, 1.0, 1.0, 1.0);	// 흰색으로 배경 컬러 초기화
	glEnable(GL_DEPTH_TEST);		// Depth 설정
	glDepthMask(GL_TRUE);			// Depth Mask true
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // 컬러 및 Depth 초기화

	// Model View Matrix 초기화
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// 광원 설정
	setLightModel();

	// 원점을 0,0,-2로 이동
	glTranslatef(0, 0, -2);

	// 회전 적용
	glRotatef(m_theta[0], 1.0f, 0.0f, 0.0f);
	glRotatef(m_theta[1], 0.0f, 1.0f, 0.0f);
	glRotatef(m_theta[2], 0.0f, 0.0f, 1.0f);

	m3DObject.Render();
	//glutSolidTeapot(3.0);
}

void CGLInterface::setLightModel()
{
	glEnable(GL_LIGHTING);	// 광원 On
	glEnable(GL_LIGHT0);	// LIGHT0 on

	glEnable(GL_NORMALIZE); // Normal vector nomalization

	float lightPosF[] = { 0.0, 0.0, 1.0, 0.0 };							// 광원 위치
	GLfloat ambient0[4] = { 0.12549f, 0.12549f, 0.12549f, 1.0f };   // Ambient 성분 값 
	GLfloat diffuse0[4] = { 0.8f, 0.8f, 0.8f, 1.0f };				// Diffuse 성분 값
	GLfloat specular0[4] = { 1.0f, 1.0f, 1.0f, 1.0f };				// Specular 성분 값

	// 위치 및 Ambient, Diffuse, Specular 성분 적용
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosF);
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient0);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse0);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular0);
}

void CGLInterface::OnTimer()
{
	m_theta[1] += 0.3f;
}

void CGLInterface::LoadMesh(std::string filePath)
{
	m3DObject.LoadModel(filePath);
}