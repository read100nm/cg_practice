#ifndef GL_INTERFACE_H
#define GL_INTERFACE_H

#include <MeshImport.h>

#ifdef WIN32
#include <Windows.h>
#endif

#ifndef _APPLE_OSX
#include <gl/GL.h>
#include <gl/GLU.h>
//#include <gl/glut.h>
#else
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
//#include <GLUT/glut.h>
#endif

#ifdef _AFXDLL   // for MFC
#define GLIF_RIGHT          0x27
#define GLIF_LEFT           0x25
#define GLIF_UP				0x26
#define GLIF_DOWN           0x28
#define GLIF_HOME           0x24
#define GLIF_END            0x23
#else           // for Qt
#define GLIF_RIGHT          0x01000014
#define GLIF_LEFT           0x01000012
#define GLIF_UP				0x01000013
#define GLIF_DOWN           0x01000015
#define GLIF_HOME           0x01000010
#define GLIF_END            0x01000011
#endif

typedef GLfloat point3[3];


class CGLInterface
{
public:
	/// Constructor
	CGLInterface();

	/// Destructor
	virtual ~CGLInterface();

	/// Initialization
	void Initialize();

	/// set projection
	void SetProjectView(int cx, int cy);

	/// draw display by OpenGL functions
	void Display();


// Function Definition Start: SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSs
public:

	/// Set the camera WRT WC and display the scene(카메라를 설정하고 scene 전체를 그린다) 
	void DrawScene(); 

	/// 광원설정
	void setLightModel();
	
	/// Key Event handler
	void OnKeyDown(int keyValue);
	/// variable initialization. 그리기에 필요한 변수를 초기화 한다. 
	void InitializeDrawing();				
	/// timer interrupt event
	void OnTimer();

	/// load mesh 
	void LoadMesh(std::string filePath);
// Function Definition End: EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEe
protected:

	
	/// for rotation
	float m_theta[3];

	/// for mesh model
	MeshImport m3DObject;
};

#endif
