// example3View.cpp : implementation of the CExample3View class
//

#include "stdafx.h"
#include "MFCFrame.h"

#include "MFCFrameDoc.h"
#include "MFCFrameView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CExample3View

IMPLEMENT_DYNCREATE(MFCFrameView, CView)

BEGIN_MESSAGE_MAP(MFCFrameView, CView)
    ON_WM_SIZE()
    ON_WM_ERASEBKGND()
    ON_WM_DESTROY()
    ON_WM_CREATE()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_KEYDOWN()
	ON_WM_TIMER()
	ON_COMMAND(ID_FILE_IMPORTMESH, &MFCFrameView::OnFileImportmesh)
END_MESSAGE_MAP()

// CExample3View construction/destruction

MFCFrameView::MFCFrameView()
{
	// TODO: add construction code here
	m_hRC = NULL;
	m_pDC = NULL;
}

MFCFrameView::~MFCFrameView()
{
}

BOOL MFCFrameView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	cs.style |= WS_CLIPCHILDREN | WS_CLIPSIBLINGS;

	return CView::PreCreateWindow(cs);
}

// CExample3View drawing

void MFCFrameView::OnDraw(CDC* /*pDC*/)
{
	MFCFrameDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: add draw code for native data here
	m_iGL.Display();
}


// CExample3View diagnostics

#ifdef _DEBUG
void MFCFrameView::AssertValid() const
{
	CView::AssertValid();
}

void MFCFrameView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

MFCFrameDoc* MFCFrameView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(MFCFrameDoc)));
	return (MFCFrameDoc*)m_pDocument;
}
#endif //_DEBUG

void MFCFrameView::OnSize(UINT nType, int cx, int cy)
{
    CView::OnSize(nType, cx, cy);

    // TODO: Add your message handler code here
    m_iGL.SetProjectView(cx, cy);
}

BOOL MFCFrameView::OnEraseBkgnd(CDC* pDC)
{
    // TODO: Add your message handler code here and/or call default

    //return CView::OnEraseBkgnd(pDC);
    return false;
}


int MFCFrameView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CView::OnCreate(lpCreateStruct) == -1)
        return -1;

    // TODO:  Add your specialized creation code here
	return Initialize(new CClientDC(this));
}

void MFCFrameView::OnDestroy()
{
	CView::OnDestroy();

	// TODO: Add your message handler code here
	Release();
}

int MFCFrameView::Initialize(CDC *pDC)
{
	if (m_pDC || m_hRC) {
		::AfxMessageBox(_T("Already initialized"));
		return -1;
	}
	if (!(m_pDC = pDC))	{
		::AfxMessageBox(_T("Fail to get device context"));
		return -1;
	}

	if (!SetPixelFormat(NULL)) {	// Pixel format 설정
		::AfxMessageBox(_T("SetupPixelFormat failed"));
		return -1;
	}
	if (!(m_hRC = wglCreateContext(m_pDC->GetSafeHdc()))) {	// RC 획득
		::AfxMessageBox(_T("wglCreateContext failed"));
		return -1;
	}
	if (!wglMakeCurrent(m_pDC->GetSafeHdc(), m_hRC)) {// Current RC 설정
		::AfxMessageBox(_T("wglMakeCurrent failed"));
		return -1;
	}

	m_iGL.Initialize();
	SetTimer(1394, 30, NULL);
	return 0;	// -1: Fail | 0: Well done
}

void MFCFrameView::Release(void)
{
	if (!wglMakeCurrent(0, 0))
		::AfxMessageBox(_T("wglMakeCurrent failed"));

	if (m_hRC && !wglDeleteContext(m_hRC))
		::AfxMessageBox(_T("wglDeleteContext filed"));

	if (m_pDC)
		delete m_pDC;

	m_hRC = NULL;	// Must be NULL
	m_pDC = NULL;	// Must be NULL
}

BOOL MFCFrameView::SetPixelFormat(PIXELFORMATDESCRIPTOR * pPFD)
{
	if (!pPFD) {
		PIXELFORMATDESCRIPTOR pfd = {
			sizeof(PIXELFORMATDESCRIPTOR),
			1, PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL,
			PFD_TYPE_RGBA, 24, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 16, 0, 0,
			PFD_MAIN_PLANE, 0, 0, 0, 0 };
		pPFD = &pfd;
	}

	int nPF = ::ChoosePixelFormat(m_pDC->GetSafeHdc(), pPFD);
	if (!nPF) {
		::AfxMessageBox(_T("ChoosePixelFormat failed"));
		return FALSE;
	}

	if (!::SetPixelFormat(m_pDC->GetSafeHdc(), nPF, pPFD)) {
		::AfxMessageBox(_T("SetPixelFormat failed"));
		return FALSE;
	}

	return TRUE;
}



void MFCFrameView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: Add your message handler code here and/or call default

	// check value for timer interrup
	int timerSet = 0;

	m_iGL.OnKeyDown(nChar);
	
	// invalidate client area for refresh. 모든 클라이언트 영역을 무효화 한다. 새로 그려줌 
	Invalidate();
	
	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}



void MFCFrameView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	switch (nIDEvent)
	{
	case 1394:
		m_iGL.OnTimer();
		Invalidate();
		break;
	}
	CView::OnTimer(nIDEvent);
}


void MFCFrameView::OnFileImportmesh()
{
	// 1. File Open Dialog 실행

	CFileDialog fOpenDlg(TRUE, "", "", OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, "3D object file formats |*.ply;*.stl;*.obj;*.3ds|Polygon File Format (*.ply)|*.ply|STereoLithography file (*.stl)|*.stl|3D Max file format (*.3ds)|*.3ds|OBJ geometry format (*.obj)|*.obj|", this);

	fOpenDlg.m_pOFN->lpstrTitle = "3D file open";

	fOpenDlg.m_pOFN->lpstrInitialDir = ".";

	if (fOpenDlg.DoModal() == IDOK)
	{
		CString filePath = fOpenDlg.GetPathName();
		std::string filePathStr = filePath;
		m_iGL.LoadMesh(filePathStr);
	}

	Invalidate();
}
