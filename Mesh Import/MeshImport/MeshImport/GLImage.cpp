#include "GLImage.h"
#include <algorithm>

//#include "opengl/include/glaux.h"

#include <png.h>

#ifdef WIN32
#include <Windows.h>
#endif

#ifndef _APPLE_OSX
#include <gl/GL.h>
#include <gl/GLU.h>
#else
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#endif

GLImage::GLImage()
{
	ImageWidth =0;
	ImageHeight =0;
	imageData =0;
}

GLImage::~GLImage()
{
	if(imageData != NULL)
		delete[] imageData;
}

bool GLImage::load(std::string path)
{
	std::string ext = GetExtension(path);

	std::string extUpper = ext;
	std::transform(extUpper.begin(), extUpper.end(),extUpper.begin(), ::toupper);

	if(extUpper.compare("TGA") ==0)
	{
		bool res = load_tga(path);
		if(res == false)
			return false;
		else
			return true;
	}
	else if(extUpper.compare("PNG") ==0)
	{
		bool res = load_png(path);
		if(res == false)
			return false;
		else
			return true;
	}
	return false;
}

std::string GLImage::GetExtension(std::string input)
{
	std::string extension = input.substr(input.find_last_of(".") + 1);
	return extension;
}

bool GLImage::load_tga(std::string path)
{
	TGAFILE tgaFile;
	bool res = LoadTGAFile(path.c_str(),&tgaFile);
	if(res == true)
	{
		// RGBA인지 RGB인지 체크 
		// RGB는 RGBA로 변환
		if( (tgaFile.bitCount / 8) == 3)
		{
			// Change from BGR to RGB so OpenGL can read the image data.
			int imageIdx =0;
			int imageIdx2 = 0;
			int colorMode = tgaFile.bitCount / 8;
			int imageSize = tgaFile.imageWidth * tgaFile.imageHeight * colorMode;

			unsigned char *newBuf = new unsigned char[ tgaFile.imageWidth * tgaFile.imageHeight * 4];
			memset((void*)newBuf,1,tgaFile.imageWidth * tgaFile.imageHeight * 4);

			
			for (imageIdx = 0; imageIdx < imageSize; imageIdx += colorMode)
			{
				newBuf[imageIdx2] = tgaFile.imageData[imageIdx];
				newBuf[imageIdx2+1] = tgaFile.imageData[imageIdx+1];
				newBuf[imageIdx2+2] = tgaFile.imageData[imageIdx+2];
				imageIdx2 += 4;
			}
			delete tgaFile.imageData;
			tgaFile.imageData = newBuf;
			tgaFile.bitCount = 32;
		}
		imageData = tgaFile.imageData;
		ImageWidth = tgaFile.imageWidth;
		ImageHeight = tgaFile.imageHeight;

		//GLScale();
	}

	return res;
}

bool GLImage::load_png(std::string path)
{
	bool res = true;
	
	png_structp png_ptr;
    png_infop info_ptr;
    unsigned int sig_read = 0;
    int color_type, interlace_type;
    FILE *fp;
 
	if ((fp = fopen(path.c_str(), "rb")) == NULL)
        return false;
 
    /* Create and initialize the png_struct
     * with the desired error handler
     * functions.  If you want to use the
     * default stderr and longjump method,
     * you can supply NULL for the last
     * three parameters.  We also supply the
     * the compiler header file version, so
     * that we know if the application
     * was compiled with a compatible version
     * of the library.  REQUIRED
     */
    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,
                                     NULL, NULL, NULL);
 
    if (png_ptr == NULL) {
        fclose(fp);
        return false;
    }
 
    /* Allocate/initialize the memory
     * for image information.  REQUIRED. */
    info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
        fclose(fp);
        png_destroy_read_struct(&png_ptr, NULL, NULL);
        return false;
    }
 
    /* Set error handling if you are
     * using the setjmp/longjmp method
     * (this is the normal method of
     * doing things with libpng).
     * REQUIRED unless you  set up
     * your own error handlers in
     * the png_create_read_struct()
     * earlier.
     */
    if (setjmp(png_jmpbuf(png_ptr))) {
        /* Free all of the memory associated
         * with the png_ptr and info_ptr */
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        fclose(fp);
        /* If we get here, we had a
         * problem reading the file */
        return false;
    }
 
    /* Set up the output control if
     * you are using standard C streams */
    png_init_io(png_ptr, fp);
 
    /* If we have already
     * read some of the signature */
    png_set_sig_bytes(png_ptr, sig_read);
 
    /*
     * If you have enough memory to read
     * in the entire image at once, and
     * you need to specify only
     * transforms that can be controlled
     * with one of the PNG_TRANSFORM_*
     * bits (this presently excludes
     * dithering, filling, setting
     * background, and doing gamma
     * adjustment), then you can read the
     * entire image (including pixels)
     * into the info structure with this
     * call
     *
     * PNG_TRANSFORM_STRIP_16 |
     * PNG_TRANSFORM_PACKING  forces 8 bit
     * PNG_TRANSFORM_EXPAND forces to
     *  expand a palette into RGB
     */
    png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_PACKING | PNG_TRANSFORM_EXPAND, NULL);
 
    png_uint_32 width, height;
    int bit_depth;
    png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type,
                 &interlace_type, NULL, NULL);

	bool outHasAlpha = (color_type == PNG_COLOR_TYPE_RGBA);
    ImageWidth = width;
    ImageHeight = height;
 
    unsigned int row_bytes = png_get_rowbytes(png_ptr, info_ptr);
    imageData = (unsigned char*) malloc(row_bytes * ImageHeight);
 
    png_bytepp row_pointers = png_get_rows(png_ptr, info_ptr);
 
    for (int i = 0; i < ImageHeight; i++) {
        // note that png is ordered top to
        // bottom, but OpenGL expect it bottom to top
        // so the order or swapped
        memcpy(imageData+(row_bytes * (ImageHeight-1-i)), row_pointers[i], row_bytes);
    }
 
    /* Clean up after the read,
     * and free any memory allocated */
    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
 
    /* Close the file */
    fclose(fp);
 
    /* That's it */
	if(outHasAlpha == false)
	{
		// RGBA인지 RGB인지 체크 
		// RGB는 RGBA로 변환
		// Change from BGR to RGB so OpenGL can read the image data.
		int imageIdx =0;
		int imageIdx2 = 0;
		int colorMode = 3;
		int imageSize = ImageWidth * ImageHeight * colorMode;

		unsigned char *newBuf = new unsigned char[ ImageWidth * ImageHeight * 4];
		memset((void*)newBuf,1,ImageWidth * ImageHeight * 4);

			
		for (imageIdx = 0; imageIdx < imageSize; imageIdx += colorMode)
		{
			newBuf[imageIdx2] = imageData[imageIdx];
			newBuf[imageIdx2+1] = imageData[imageIdx+1];
			newBuf[imageIdx2+2] = imageData[imageIdx+2];
			imageIdx2 += 4;
		}
		delete imageData;
		imageData = newBuf;
	}


    return true;
}

void GLImage::GLScale()
{
	// image has to be scaled to a 2^n size. We choose the first 2^N >= picture size.
	GLint MaxTextureSize;
	glGetIntegerv(GL_MAX_TEXTURE_SIZE,&MaxTextureSize);

	int bestW=RoundUpToTheNextHighestPowerOf2(ImageWidth );
	int bestH=RoundUpToTheNextHighestPowerOf2(ImageHeight );
	while(bestW>MaxTextureSize) bestW /=2;
	while(bestH>MaxTextureSize) bestH /=2;

	if((bestW == ImageWidth) && (bestH == ImageHeight))
		return;

	int i,j;
	unsigned char *newImage = new unsigned char[bestW*bestH];
	unsigned char **pDestinationArray = new unsigned char*[bestH];
	unsigned char **pSourceArray = new unsigned char*[ImageHeight];
	for(i=0; i< bestH; i++)
		pDestinationArray[i] = newImage+(i * bestW);

	for(i=0; i< ImageHeight; i++)
		pSourceArray[i] = imageData+(i * ImageWidth);

	// 실제 처리 부분
	double ScaleX = double(bestW)/ImageWidth;
	double ScaleY = double(bestH)/ImageHeight;
	int   x = bestW;
	int   y = bestH;
	int   x_source, y_source;
	double EWweight, NSweight, EWtop,EWbottom;
	double floatX, floatY;
	int   NW, NE, SW, SE;

	for (i = 0; i < y ; i++)
	{
		floatY   = double(i) / ScaleY;
		y_source = int(floatY);// + 0.5);
		NSweight = floatY - int(floatY);

		for (j = 0 ; j < x ; j++)
		{
			floatX   = double(j) / ScaleX;
			x_source = int(floatX);// + 0.5);
			EWweight = floatX - int(floatX);

			if (x_source + 1>= ImageWidth && y_source + 1 >= ImageHeight)
			{
				NW = pSourceArray[ y_source - 1][ x_source - 1];
				NE = pSourceArray[ y_source - 1][ x_source ];
				SW = pSourceArray[ y_source    ][ x_source - 1];
				SE = pSourceArray[ y_source    ][ x_source ];
				EWtop    = NW + EWweight * (NE - NW);
				EWbottom = SW + EWweight * (SE - SW);
			}
			else if (x_source + 1 >= ImageWidth)
			{
				NW = pSourceArray[ y_source ]    [ x_source -1];
				NE = pSourceArray[ y_source ]    [ x_source ];
				SW = pSourceArray[ y_source + 1 ][ x_source -1 ];
				SE = pSourceArray[ y_source + 1 ][ x_source ];
				EWtop    = NW + EWweight * (NE - NW);
				EWbottom = SW + EWweight * (SE - SW);
			}
			else if (y_source + 1 >= ImageHeight )
			{
				NW = pSourceArray[ y_source - 1][ x_source ];
				NE = pSourceArray[ y_source - 1][ x_source + 1];
				SW = pSourceArray[ y_source ]   [ x_source ];
				SE = pSourceArray[ y_source ]   [ x_source + 1];
				EWtop    = NW + EWweight * (NE - NW);
				EWbottom = SW + EWweight * (SE - SW);
			}
			else
			{
				NW = pSourceArray[ y_source ]    [ x_source ];
				NE = pSourceArray[ y_source ]    [ x_source + 1];
				SW = pSourceArray[ y_source + 1 ][ x_source ];
				SE = pSourceArray[ y_source + 1 ][ x_source + 1];
				EWtop    = NW + EWweight * (NE - NW);
				EWbottom = SW + EWweight * (SE - SW);
			}
			pDestinationArray[i][j] = (unsigned char) (EWtop + NSweight * (EWbottom - EWtop) + 0.5) ;
		}
	}

	delete[] pDestinationArray;
	delete[] pSourceArray;
	delete[] imageData;
	imageData = newImage;
	ImageWidth = bestW;
	ImageHeight = bestH;
}

// compute the next highest power of 2 of 32-bit v
int GLImage::RoundUpToTheNextHighestPowerOf2(unsigned int v)
{
	v--;
	v |= v >> 1;
	v |= v >> 2;
	v |= v >> 4;
	v |= v >> 8;
	v |= v >> 16;
	v++;
	return v;
}
