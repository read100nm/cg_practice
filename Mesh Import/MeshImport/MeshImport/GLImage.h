#ifndef GL_IMAGE_H_00
#define GL_IMAGE_H_00

#include "tgaLoad.h"
#include <string>

class GLImage {

public:
	GLImage();
	~GLImage();
	
	bool load(std::string path);
	std::string GetExtension(std::string input);
	
	bool load_tga(std::string path);
	bool load_png(std::string path);

	unsigned char * getData(){return imageData;};
	int ImageWidth;
	int ImageHeight;
	
	// imageData를 2^N 해상도로 바꿔주는 함수
	void GLScale();

	int RoundUpToTheNextHighestPowerOf2(unsigned int v);
protected:

	unsigned char *imageData;

};


#endif