

#include "MeshImport.h"
#include "GLImage.h"
#include <algorithm>

#ifndef WIN32
#include <unistd.h>
#endif

MeshImport::MeshImport()
{
	model1 = NULL;
	isLoaded = false;

	trackball.center=vcg::Point3f(0, 0, 0);
	trackball.radius= 1;
	trackball_light.center=vcg::Point3f(0, 0, 0);
	trackball_light.radius= 1;
}

MeshImport::~MeshImport()
{
	if(model1 != NULL)
		delete model1;
}

// 파일로 부터 모델을 불러온다. 
bool MeshImport::LoadModel(const std::string path)
{
	
	std::string ext = this->GetExtension(path);
	std::string label = this->GetFileName(path);
	std::string abPath = this->GetAbosolutePath(path);
	
	if(model1 != NULL) delete model1;
	model1 = NULL;
	MeshModel *tempModel1 = new MeshModel(path,label);

	int mask = 0;

	// 텍스쳐를 읽기 위한 Woring Directory 변경
#ifdef WIN32	
  bool sucuss = SetCurrentDirectory(abPath.c_str());
#else
  bool sucuss = chdir(abPath.c_str());
#endif
	// 확장자 대문자 변환
	std::string extUpper = ext;
	std::transform(extUpper.begin(), extUpper.end(),extUpper.begin(), ::toupper);

	bool result_load = false;
	if(extUpper.compare("3DS") == 0)
		result_load = meshio_open(ext,path,*tempModel1,mask);
	else if(extUpper.compare("PLY") == 0)
		result_load = baseio_open(ext,path,*tempModel1,mask);
	else if(extUpper.compare("STL") == 0)
		result_load = baseio_open(ext,path,*tempModel1,mask);
	else if(extUpper.compare("OBJ") == 0)
		result_load = baseio_open(ext,path,*tempModel1,mask);

	if(result_load == false)
	{
		delete tempModel1;
		tempModel1 = NULL;
		return false;
	}

	if( mask & vcg::tri::io::Mask::IOM_FACECOLOR) setColorMode(vcg::GLW::CMPerFace);
	if( mask & vcg::tri::io::Mask::IOM_VERTCOLOR) setColorMode(vcg::GLW::CMPerVert);
	setDrawMode(vcg::GLW::DMSmooth);

	if(!tempModel1->cm.textures.empty())
	{
		if(vcg::tri::HasPerVertexTexCoord(tempModel1->cm) )
			setTextureMode(vcg::GLW::TMPerVert);
		if(vcg::tri::HasPerWedgeTexCoord(tempModel1->cm) )
			setTextureMode(vcg::GLW::TMPerWedgeMulti);
	}

	// In case of polygonal meshes the normal should be updated accordingly
	if( mask & vcg::tri::io::Mask::IOM_BITPOLYGONAL)
	{
		tempModel1->updateDataMask(MeshModel::MM_POLYGONAL); // just to be sure. Hopefully it should be done in the plugin...
		int degNum = vcg::tri::Clean<CMeshO>::RemoveDegenerateFace(tempModel1->cm);
    //if(degNum)
    //  GLA()->log->Logf(0,"Warning model contains %i degenerate faces. Removed them.",degNum);
		tempModel1->updateDataMask(MeshModel::MM_FACEFACETOPO);
		vcg::tri::UpdateNormal<CMeshO>::PerBitQuadFaceNormalized(tempModel1->cm);
		vcg::tri::UpdateNormal<CMeshO>::PerVertexFromCurrentFaceNormal(tempModel1->cm);
	} // standard case
	else
	{
		vcg::tri::UpdateNormal<CMeshO>::PerFaceNormalized(tempModel1->cm);
		if(!( mask & vcg::tri::io::Mask::IOM_VERTNORMAL) )
			vcg::tri::UpdateNormal<CMeshO>::PerVertexAngleWeighted(tempModel1->cm);
	}

	vcg::tri::UpdateBounding<CMeshO>::Box(tempModel1->cm);					// updates bounding box

	if(tempModel1->cm.fn==0 && tempModel1->cm.en==0){
		if(!(mask & vcg::tri::io::Mask::IOM_VERTNORMAL))
			;
		else
			tempModel1->updateDataMask(MeshModel::MM_VERTNORMAL);
	}
	if(tempModel1->cm.fn==0 && tempModel1->cm.en>0){
    if(!(mask & vcg::tri::io::Mask::IOM_VERTNORMAL))
		;
	else
		tempModel1->updateDataMask(MeshModel::MM_VERTNORMAL);
	}
	else
		tempModel1->updateDataMask(MeshModel::MM_VERTNORMAL);

	int delVertNum = vcg::tri::Clean<CMeshO>::RemoveDegenerateVertex(tempModel1->cm);
	int delFaceNum = vcg::tri::Clean<CMeshO>::RemoveDegenerateFace(tempModel1->cm);

	model1 = tempModel1;
	resetTrackBall();
	return true;
}

// 불러온 모델을 화면에 보여준다. 
void MeshImport::Render()
{
	if(model1 == NULL)
		return;

	initTexture();
	
	glPushMatrix();
	trackball.GetView();
	trackball.Apply(false);
	glPushMatrix();
	setLightModel();

	// Set proper colorMode
	if(rm.colorMode != vcg::GLW::CMNone)
	{
		glEnable(GL_COLOR_MATERIAL);
		glColorMaterial(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE);
	}
	else
	{
		glColor3f(0.752941f,0.752941f,0.752941f);
	}

	glPushAttrib(GL_ALL_ATTRIB_BITS);
	model1->glw.SetHintParamf(vcg::GLW::HNPPointSize,2);
	model1->glw.SetHintParami(vcg::GLW::HNPPointDistanceAttenuation,1);
	model1->glw.SetHintParami(vcg::GLW::HNPPointSmooth,1);
	model1->render(rm.drawMode,rm.colorMode,rm.textureMode);

	glPopAttrib();
	glPopMatrix();

	glPopMatrix();
}

std::string MeshImport::GetExtension(std::string input)
{
	std::string extension = input.substr(input.find_last_of(".") + 1);

	return extension;
}

std::string MeshImport::GetFileName(std::string input)
{
    std::string fileName;
    size_t pos = input.find_last_of("\\");
    
    if (pos == std::string::npos)
        pos = input.find_last_of("/");
    
    if (pos != std::string::npos)
        fileName.assign(input.begin()+pos+1, input.end());
    else
        fileName = input;
    
    return fileName;
}

std::string MeshImport::GetAbosolutePath(std::string input)
{
    std::string path;
    size_t pos = input.find_last_of("\\");
    
    if (pos == std::string::npos)
        pos = input.find_last_of("/");
    
    if (pos != std::string::npos)
        path = input.substr(0, pos+1);
    else
        path = input;
    
    return path;
}


bool MeshImport::EndsWith(std::string source, std::string ending)
{
	if (source.length() >= ending.length()) {
        return (0 == source.compare (source.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}


void MeshImport::initTexture()
{
	if(!model1->cm.textures.empty() && model1->glw.TMId.empty()){
		glEnable(GL_TEXTURE_2D);
		GLint MaxTextureSize;
		glGetIntegerv(GL_MAX_TEXTURE_SIZE,&MaxTextureSize);
		//QString unexistingtext = "In mesh file <i>" + mp->fullName() + "</i> : Failure loading textures:<br>";
		bool sometextfailed = false;
		for(unsigned int i =0; i< model1->cm.textures.size();++i)
		{
				GLImage img;
				//QImage img, imgScaled, imgGL;
				bool res = img.load(model1->cm.textures[i].c_str());
				sometextfailed = sometextfailed || !res;
				if(!res)
				{
					// Note that sometimes (in collada) the texture names could have been encoded with a url-like style (e.g. replacing spaces with '%20') so making some other attempt could be harmless
					std::string ConvertedName = model1->cm.textures[i];
					size_t pos = 0;
					while((pos = ConvertedName.find("%20", pos)) != std::string::npos)
					{
						ConvertedName.replace(pos,3," ");
						pos += 1;
					}
					res = img.load(ConvertedName);
					if(!res)
					{
						//this->Logf(0,"Failure of loading texture %s",mp->cm.textures[i].c_str());
						//unexistingtext += "<font color=red>" + QString(mp->cm.textures[i].c_str()) + "</font><br>";
					}
					else 
						;
		//				this->Logf(0,"Warning, texture loading was successful only after replacing %%20 with spaces;\n Loaded texture %s instead of %s",qPrintable(ConvertedName),mp->cm.textures[i].c_str());
					
					model1->glw.TMId.push_back(0);
					glGenTextures( 1, (GLuint*)&(model1->glw.TMId.back()) );
					glBindTexture( GL_TEXTURE_2D, model1->glw.TMId.back() );
					glTexImage2D( GL_TEXTURE_2D, 0, 3, 0, 0, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0 );
				}
				//if(!res && EndsWith(model1->cm.textures[i],"dds"))
				//{
					//qDebug("DDS binding!");
				//	int newTexId = bindTexture(QString(mp->cm.textures[i].c_str()));
				//	mp->glw.TMId.push_back(newTexId);
				//}
				if(res)
				{
					// image has to be scaled to a 2^n size. We choose the first 2^N >= picture size.
					model1->glw.TMId.push_back(0);
					//qDebug("      	will be loaded as GL texture id %i  ( %i x %i )",model1->glw.TMId.back() ,imgGL.width(), imgGL.height());
					glGenTextures( 1, (GLuint*)&(model1->glw.TMId.back()) );
					glBindTexture( GL_TEXTURE_2D, model1->glw.TMId.back() );
					glTexImage2D( GL_TEXTURE_2D, 0, 3, img.ImageWidth, img.ImageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, img.getData() );
					gluBuild2DMipmaps(GL_TEXTURE_2D, 3, img.ImageWidth, img.ImageHeight, GL_RGBA, GL_UNSIGNED_BYTE, img.getData() );
				}
				glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
				glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );

				glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
			}
		//	if (sometextfailed)
		//		QMessageBox::warning(this,"Texture files has not been correctly loaded",unexistingtext);
	}
	glDisable(GL_TEXTURE_2D);
}

void MeshImport::setView()
{

}

void MeshImport::drawLight()
{

}

void MeshImport::setLightModel()
{

}

void MeshImport::resetTrackBall()
{
	trackball.Reset();
	FullBBox.SetNull();
	FullBBox.Add(model1->cm.Tr, model1->cm.bbox);

	float newScale= 1.0f/FullBBox.Diag();
	trackball.track.sca = newScale;
	trackball.track.tra =  -FullBBox.Center();
}

void MeshImport::setDrawMode(vcg::GLW::DrawMode mode)
{
	rm.drawMode = mode;
}

void MeshImport::setColorMode(vcg::GLW::ColorMode mode)
{
	rm.colorMode = mode;
}
void MeshImport::setTextureMode(vcg::GLW::TextureMode mode)
{
	rm.textureMode = mode;
}
