#ifndef MESH_IMPORT_H_00
#define MESH_IMPORT_H_00

//#ifdef WIN32
//#include <Windows.h>
//#endif
//
//#ifndef _APPLE_OSX
//#include <gl/GL.h>
//#include <gl/GLU.h>
//#else
//#include <OpenGL/gl.h>
//#include <OpenGL/glu.h>
//#endif

#include "meshmodel.h"
#include "baseio.h"
#include "trackball.h"

#include <iostream>
#include <string>
#include <stdlib.h>
#include <math.h>

class MeshImport
{
public:
	MeshImport();
	~MeshImport();

	// 파일로 부터 모델을 불러온다. 
	bool LoadModel(const std::string path);

	// 불러온 모델을 화면에 보여준다. 
	void Render();

	// 렌더링 모드 설정
	//virtual void RenderMode(
	
	void initTexture();
	void setView();
	void drawLight();
	void setLightModel();
	void resetTrackBall();

	void setDrawMode(vcg::GLW::DrawMode mode);
	void setColorMode(vcg::GLW::ColorMode mode);
	void setTextureMode(vcg::GLW::TextureMode mode);

protected:
	std::string GetExtension(std::string input);
	std::string GetFileName(std::string input);
	std::string GetAbosolutePath(std::string input);
	bool EndsWith(std::string source, std::string ending);
	MeshModel *model1;
	vcg::Trackball trackball;
	vcg::Trackball trackball_light;
	vcg::Box3f FullBBox;

	bool isLoaded;

	RenderMode rm;
};


#endif

