#include <algorithm>
#include <string>

#include "baseio.h"

#include <wrap/io_trimesh/import_ply.h>
#include <wrap/io_trimesh/import_stl.h>
#include <wrap/io_trimesh/import_obj.h>
#include <wrap/io_trimesh/import_off.h>
#include <wrap/io_trimesh/import_ptx.h>
#include <wrap/io_trimesh/import_vmi.h>

#include <wrap/io_trimesh/export_ply.h>
#include <wrap/io_trimesh/export_stl.h>
#include <wrap/io_trimesh/export_obj.h>
#include <wrap/io_trimesh/export_vrml.h>
#include <wrap/io_trimesh/export_dxf.h>
#include <wrap/io_trimesh/export_vmi.h>
#include <wrap/io_trimesh/export.h>

#include "import_3ds.h"

using namespace vcg;



bool baseio_open(const std::string &formatName, const std::string &fileName, MeshModel &m, int& mask)
{
	bool normalsUpdated = false;
	
	// initializing mask
	mask = 0;

	std::string formatNameUpper = formatName;
	std::transform(formatNameUpper.begin(), formatNameUpper.end(),formatNameUpper.begin(), ::toupper);
	
	if(formatNameUpper.compare("PLY") == 0)
	{

		tri::io::ImporterPLY<CMeshO>::LoadMask(fileName.c_str(), mask); 
		// small patch to allow the loading of per wedge color into faces.  
		if(mask & tri::io::Mask::IOM_WEDGCOLOR) mask |= tri::io::Mask::IOM_FACECOLOR;
		m.Enable(mask);

		int result = tri::io::ImporterPLY<CMeshO>::Open(m.cm, fileName.c_str(), mask);
		if (result != 0) // all the importers return 0 on success
		{
			if(tri::io::ImporterPLY<CMeshO>::ErrorCritical(result) )
			{
				//errorMessage = errorMsgFormat.arg(fileName, tri::io::ImporterPLY<CMeshO>::ErrorMsg(result));
				return false;
			}
		}
	}
	else if (formatNameUpper.compare("STL") == 0)
	{
		int mask = 0;
		int result = tri::io::ImporterSTL<CMeshO>::Open(m.cm, fileName.c_str(), mask);
		if (result != 0) // all the importers return 0 on success
		{
			//errorMessage = errorMsgFormat.arg(fileName, tri::io::ImporterSTL<CMeshO>::ErrorMsg(result));
			return false;
		}
	}
	else if (formatNameUpper.compare("OBJ") == 0)
	{
		tri::io::ImporterOBJ<CMeshO>::Info oi;	
		oi.cb = NULL;
		if (!tri::io::ImporterOBJ<CMeshO>::LoadMask(fileName.c_str(), oi))
			return false;
		
		m.Enable(oi.mask);
		
		int result = tri::io::ImporterOBJ<CMeshO>::Open(m.cm, fileName.c_str(), oi);
		if (result != tri::io::ImporterOBJ<CMeshO>::E_NOERROR)
		{
			if (result & tri::io::ImporterOBJ<CMeshO>::E_NON_CRITICAL_ERROR)
			{
				//errorMessage = errorMsgFormat.arg(fileName, tri::io::ImporterOBJ<CMeshO>::ErrorMsg(result));
				//return false;
			}
			else
			{
				//errorMessage = errorMsgFormat.arg(fileName, tri::io::ImporterOBJ<CMeshO>::ErrorMsg(result));
				return false;
			}
		}

		if(oi.mask & tri::io::Mask::IOM_WEDGNORMAL)
			normalsUpdated = true;
        m.Enable(oi.mask);
        if(m.hasDataMask(MeshModel::MM_POLYGONAL)) std::cout<<"Mesh is Polygonal!";
		mask = oi.mask;
	}

	// verify if texture files are present
	//QString missingTextureFilesMsg = "The following texture files were not found:\n";
	bool someTextureNotFound = false;
	for ( unsigned textureIdx = 0; textureIdx < m.cm.textures.size(); ++textureIdx)
	{
		std::ifstream f(m.cm.textures[textureIdx].c_str());
		if(f.good() == false)
		{
		//	missingTextureFilesMsg.append("\n");
		//	missingTextureFilesMsg.append(m.cm.textures[textureIdx].c_str());
			someTextureNotFound = true;
		}
	}

	return true;
}

bool meshio_open(const std::string &formatName, const std::string &fileName, MeshModel &m, int& mask)
{
	// initializing mask
	mask = 0;
	
	bool normalsUpdated = false;

	// initializing mask
	mask = 0;

	std::string formatNameUpper = formatName;
	std::transform(formatNameUpper.begin(), formatNameUpper.end(),formatNameUpper.begin(), ::toupper);


	if (formatNameUpper.compare("3DS") == 0)
	{
		vcg::tri::io::_3dsInfo info;	
		info.cb = NULL;
		Lib3dsFile *file = NULL;
		vcg::tri::io::Importer3DS<CMeshO>::LoadMask(fileName.c_str(), file, info);
		m.Enable(info.mask);
		
		int result = vcg::tri::io::Importer3DS<CMeshO>::Open(m.cm, fileName.c_str(), file, info);
		if (result != vcg::tri::io::Importer3DS<CMeshO>::E_NOERROR)
		{
			//QMessageBox::warning(parent, tr("3DS Opening Error"), errorMsgFormat.arg(fileName, vcg::tri::io::Importer3DS<CMeshO>::ErrorMsg(result)));
			return false;
		}

		if(info.mask & vcg::tri::io::Mask::IOM_WEDGNORMAL)
			normalsUpdated = true;

		mask = info.mask;
	}

	// verify if texture files are present
	//QString missingTextureFilesMsg = "The following texture files were not found:\n";
	bool someTextureNotFound = false;
	for ( unsigned textureIdx = 0; textureIdx < m.cm.textures.size(); ++textureIdx)
	{
		FILE* pFile = fopen (m.cm.textures[textureIdx].c_str(), "r");
		if (pFile == NULL)
		{
			//missingTextureFilesMsg.append("\n");
			//missingTextureFilesMsg.append(m.cm.textures[textureIdx].c_str());
			someTextureNotFound = true;
		}
		else
			fclose (pFile);
	}
	if (someTextureNotFound)
		;
		//QMessageBox::warning(parent, tr("Missing texture files"), missingTextureFilesMsg);

	vcg::tri::UpdateBounding<CMeshO>::Box(m.cm);					// updates bounding box
	if (!normalsUpdated) 
		vcg::tri::UpdateNormal<CMeshO>::PerVertex(m.cm);		// updates normals

	//if (cb != NULL)	(*cb)(99, "Done");

	return true;


}