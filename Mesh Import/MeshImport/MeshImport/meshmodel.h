/****************************************************************************
* MeshLab                                                           o o     *
* A versatile mesh processing toolbox                             o     o   *
*                                                                _   O  _   *
* Copyright(C) 2005                                                \/)\/    *
* Visual Computing Lab                                            /\/|      *
* ISTI - Italian National Research Council                           |      *
*                                                                    \      *
* All rights reserved.                                                      *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License (http://www.gnu.org/licenses/gpl.txt)          *
* for more details.                                                         *
*                                                                           *
****************************************************************************/

#ifndef MESHMODEL_H
#define MESHMODEL_H

#include <GL/glew.h>


#include <stdio.h>
#include <time.h>

#include <vcg/complex/complex.h>
//#include <vcg/simplex/vertex/base.h>
//#include <vcg/simplex/vertex/component_ocf.h>
//#include <vcg/simplex/edge/base.h>
//#include <vcg/simplex/face/base.h>
#include <vcg/simplex/face/component_ocf.h>

#include <vcg/complex/used_types.h>
#include <vcg/complex/complex.h>
#include <vcg/complex/allocate.h>

#include <vcg/simplex/face/topology.h>

#include <vcg/complex/algorithms/update/bounding.h>
#include <vcg/complex/algorithms/update/color.h>
#include <vcg/complex/algorithms/update/flag.h>
#include <vcg/complex/algorithms/update/normal.h>
#include <vcg/complex/algorithms/update/position.h>
#include <vcg/complex/algorithms/update/quality.h>
#include <vcg/complex/algorithms/update/selection.h>
#include <vcg/complex/algorithms/update/topology.h>

#include <wrap/gl/trimesh.h>
#include <wrap/callback.h>
#include <wrap/io_trimesh/io_mask.h>
#include <wrap/io_trimesh/additionalinfo.h>

// Forward declarations needed for creating the used types
class CVertexO;
class CEdgeO;
class CFaceO;

// Declaration of the semantic of the used types
class CUsedTypesO: public vcg::UsedTypes < vcg::Use<CVertexO>::AsVertexType,
	vcg::Use<CEdgeO   >::AsEdgeType,
	vcg::Use<CFaceO  >::AsFaceType >{};


// The Main Vertex Class
// Most of the attributes are optional and must be enabled before use.
// Each vertex needs 40 byte, on 32bit arch. and 44 byte on 64bit arch.

class CVertexO  : public vcg::Vertex< CUsedTypesO,
	vcg::vertex::InfoOcf,           /*  4b */
	vcg::vertex::Coord3f,           /* 12b */
	vcg::vertex::BitFlags,          /*  4b */
	vcg::vertex::Normal3f,          /* 12b */
	vcg::vertex::Qualityf,          /*  4b */
	vcg::vertex::Color4b,           /*  4b */
	vcg::vertex::VFAdjOcf,          /*  0b */
	vcg::vertex::MarkOcf,           /*  0b */
	vcg::vertex::TexCoordfOcf,      /*  0b */
	vcg::vertex::CurvaturefOcf,     /*  0b */
	vcg::vertex::CurvatureDirfOcf,  /*  0b */
	vcg::vertex::RadiusfOcf         /*  0b */
>{
};


// The Main Edge Class
// Currently it does not contains anything.
class CEdgeO : public vcg::Edge<CUsedTypesO,
	vcg::edge::BitFlags,          /*  4b */
	vcg::edge::EVAdj,
	vcg::edge::EEAdj
>{
};

// Each face needs 32 byte, on 32bit arch. and 48 byte on 64bit arch.
class CFaceO    : public vcg::Face<  CUsedTypesO,
	vcg::face::InfoOcf,              /* 4b */
	vcg::face::VertexRef,            /*12b */
	vcg::face::BitFlags,             /* 4b */
	vcg::face::Normal3f,             /*12b */
	vcg::face::QualityfOcf,          /* 0b */
	vcg::face::MarkOcf,              /* 0b */
	vcg::face::Color4bOcf,           /* 0b */
	vcg::face::FFAdjOcf,             /* 0b */
	vcg::face::VFAdjOcf,             /* 0b */
	vcg::face::WedgeTexCoordfOcf     /* 0b */
> {};

class CMeshO    : public vcg::tri::TriMesh< vcg::vertex::vector_ocf<CVertexO>, vcg::face::vector_ocf<CFaceO> > 
{
public :
	int sfn;    //The number of selected faces.
	int svn;    //The number of selected vertices.
	vcg::Matrix44f Tr; // Usually it is the identity. It is applied in rendering and filters can or cannot use it. (most of the filter will ignore this)

	const vcg::Box3f &trBB()
	{
		static vcg::Box3f bb;
		bb.SetNull();
		bb.Add(Tr,bbox);
		return bb;
	}
};

/*An ack in order to avoid to duplicate code. Should be used as MeshLabRenderMesh class itself ONLY inside MeshLabRenderState and as DERIVED class in MeshModel*/

class MeshLabRenderMesh
{
public:
	MeshLabRenderMesh();
	~MeshLabRenderMesh();

	//WARNING!!!!!: the constructor create a copy of the mesh passed as parameter. 
	//The parameter should be const but this is impossible cause of vcg::tri::Append::MeshCopy implementation in the vcglib
	MeshLabRenderMesh(CMeshO& mesh);

	bool render(vcg::GLW::DrawMode dm,vcg::GLW::ColorMode cm,vcg::GLW::TextureMode tm );
	bool renderSelectedFace();
	bool renderSelectedVert();

	vcg::GlTrimesh<CMeshO> glw;
	CMeshO cm;
};

/*
MeshModel Class
The base class for representing a single mesh.
It contains a single vcg mesh object with some additional information for keeping track of its origin and of what info it has.
*/

class MeshModel : public MeshLabRenderMesh
{
public:
	/*
	This enum specify the various simplex components
	It is used in various parts of the framework:
	- to know what elements are currently active and therefore can be saved on a file
	- to know what elements are required by a filter and therefore should be made ready before starting the filter (e.g. if a
	- to know what elements are changed by a filter and therefore should be saved/restored in case of dynamic filters with a preview
	*/
	enum MeshElement{
		MM_NONE             = 0x00000000,
		MM_VERTCOORD        = 0x00000001,
		MM_VERTNORMAL       = 0x00000002,
		MM_VERTFLAG         = 0x00000004,
		MM_VERTCOLOR        = 0x00000008,
		MM_VERTQUALITY      = 0x00000010,
		MM_VERTMARK	        = 0x00000020,
		MM_VERTFACETOPO     = 0x00000040,
		MM_VERTCURV	        = 0x00000080,
		MM_VERTCURVDIR      = 0x00000100,
		MM_VERTRADIUS       = 0x00000200,
		MM_VERTTEXCOORD     = 0x00000400,
		MM_VERTNUMBER       = 0x00000800,

		MM_FACEVERT         = 0x00001000,
		MM_FACENORMAL       = 0x00002000,
		MM_FACEFLAG	        = 0x00004000,
		MM_FACECOLOR        = 0x00008000,
		MM_FACEQUALITY      = 0x00010000,
		MM_FACEMARK	        = 0x00020000,
		MM_FACEFACETOPO     = 0x00040000,
		MM_FACENUMBER       = 0x00080000,

		MM_WEDGTEXCOORD     = 0x00100000,
		MM_WEDGNORMAL       = 0x00200000,
		MM_WEDGCOLOR        = 0x00400000,

		// 	Selection
		MM_VERTFLAGSELECT   = 0x00800000,
		MM_FACEFLAGSELECT   = 0x01000000,

		// Per Mesh Stuff....
		MM_CAMERA			= 0x08000000,
		MM_TRANSFMATRIX     = 0x10000000,
		MM_COLOR            = 0x20000000,
		MM_POLYGONAL        = 0x40000000,
		MM_UNKNOWN          = 0x80000000,

		MM_ALL				= 0xffffffff
	};

	//MeshDocument *parent;


public:
	/*
	Bitmask denoting what fields are currently used in the mesh
	it is composed by MeshElement enums.
	it should be changed by only mean the following functions:

	updateDataMask(neededStuff)
	clearDataMask(no_needed_stuff)
	hasDataMask(stuff)

	Note that if an element is active means that is also allocated
	Some unactive elements (vertex color) are usually already allocated
	other elements (FFAdj or curvature data) not necessarily.

	*/

private:
	int currentDataMask;
	std::string fullPathFileName;
	std::string _label;
	int _id;
	bool modified;

public:
	void Clear();
	void UpdateBoxAndNormals(); // This is the STANDARD method that you should call after changing coords.
	inline int id() const {return _id;}

	// Some notes about the files and naming.
	// Each mesh when shown in the layer dialog has a label.
	// By default the label is just the name of the file, but the

	// in a future the path should be moved outside the meshmodel into the meshdocument (and assume that all the meshes resides in a common subtree)
	// currently we just fix the interface and make the pathname private for avoiding future hassles.

	std::string label() const { return _label;}

	/// The whole full path name of the mesh
	std::string fullName() const {return fullPathFileName;}

	/// just the name of the file
	std::string shortName() const { return fullPathFileName; }

	/// the full path without the name of the file (e.g. the dir where the mesh and often its textures are)
	std::string pathName() const {return fullPathFileName;}

	/// just the extension.
	std::string suffixName() const {return fullPathFileName;}

	/// the relative path with respect to the current project
	std::string relativePathName() const;

	/// the absolute path of the current project
	std::string documentPathName() const;

	void setFileName(std::string newFileName) {
		//QFileInfo fi(newFileName);
		//if(!fi.isAbsolute()) qWarning("Someone is trying to put a non relative filename");
		fullPathFileName = newFileName;
	}
	void setLabel(std::string newName) {_label=newName;}


public:
	bool visible; // used in rendering; Needed for toggling on and off the meshes

	MeshModel(std::string fullFileName, std::string labelName);
	//bool Render(vcg::GLW::DrawMode _dm, vcg::GLW::ColorMode _cm, vcg::GLW::TextureMode _tm);
	//bool RenderSelectedFace();
	//bool RenderSelectedVert();


	// This function is roughly equivalent to the updateDataMask,
	// but it takes in input a mask coming from a filetype instead of a filter requirement (like topology etc)
	void Enable(int openingFileMask);

	bool hasDataMask(const int maskToBeTested) const;
	void updateDataMask(MeshModel *m);
	void updateDataMask(int neededDataMask);
	void clearDataMask(int unneededDataMask);


	bool& meshModified();
	static int io2mm(int single_iobit);


};// end class MeshModel


class RenderMode
{
public:
	vcg::GLW::DrawMode	drawMode;
	vcg::GLW::ColorMode	colorMode;
	vcg::GLW::TextureMode	textureMode;

	bool lighting;
	bool backFaceCull;
	bool doubleSideLighting;
	bool fancyLighting;
	bool selectedFace;
	bool selectedVert;


	RenderMode()
	{
		drawMode	= vcg::GLW::DMFlat;
		colorMode = vcg::GLW::CMNone;
		textureMode = vcg::GLW::TMNone;

		lighting = true;
		backFaceCull = false;
		doubleSideLighting = false;
		fancyLighting = false;
		selectedFace=false;
		selectedVert=false;
	}

}; // end class RenderMode


class MeshModelState
{
private:
	int changeMask; // a bit mask indicating what have been changed. Composed of MeshModel::MeshElement (e.g. stuff like MeshModel::MM_VERTCOLOR)
	MeshModel *m; // the mesh which the changes refers to.
	std::vector<float> vertQuality;
	std::vector<vcg::Color4b> vertColor;
	std::vector<vcg::Point3f> vertCoord;
	std::vector<vcg::Point3f> vertNormal;
	std::vector<bool> faceSelection;
	std::vector<bool> vertSelection;
	vcg::Matrix44f Tr;
	vcg::Shotf shot;
public:
	// This function save the <mask> portion of a mesh into the private members of the MeshModelState class;
	void create(int _mask, MeshModel* _m);
	bool apply(MeshModel *_m);
	bool isValid(MeshModel *m);
};


#endif
